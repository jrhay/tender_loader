<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 08.08.13
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */

$eMessage = "\n HELP MESSAGE : usage:
     multi-load.php -k x -c y -f z -l l -i i
      where:
      x - number of \"check\" processes
      y- number of \"new\" processes
      z- number of \"parse\" processes
      l- number of \"load\" processes
      i- number of \"index\" processes";



$newNumber = 0;
$parseNumber = 0;
$loadNumber = 0;
$indexNumber = 0;
$prepare = false;
$checkF = false;
$eFlag = false;
unset($argv[0]);
foreach ($argv as $pos => $value) {

    if (!empty($argv[$pos + 1])) {
        $val = (int)$argv[$pos + 1];
    } else {
        $val = null;
    }


    if (in_array($value, array('-c', '-f', '-l', '-p', '-k', '-i'))) {
        switch ($value) {
            case '-k':
                $checkF = true;
                break;
            case '-p':
                $prepare = true;
                break;
            case '-c':
                $newNumber = $val;
                break;
            case '-f':
                $parseNumber = $val;
                break;
            case '-l':
                $loadNumber = $val;
                break;
            case '-i':
                $indexNumber = $val;
                break;
        }
    }

    if (in_array($value, array('-c', '-f', '-l', '-i')) and !is_int($val)) {
        print "\n WARNING: bad parameter $value or value {$argv[$pos + 1]} ";
        $eFlag = true;
    }

}

if ($eFlag === true) {
    print "\n application is terminating...";
    exit(143);
}
if ($argc === 1 || $argc >= 13 || in_array($argv[1], array('--help', '-help', '-h', '-?'))) {
    print ($eMessage);
    exit(143);
} else {
    // TODO поток по умолчанию
    print "\n start...";
    if ($prepare) {
        print "\n preparing project...";
        $prepareDir = __DIR__;
        $prepCmd = " php $prepareDir/prepare.php";
        exec($prepCmd, $output);
        var_dump($output);
    }


    $cwd = __DIR__ . "/../";
$check = "php $cwd/app/console tenders:scheduler -k";
    $new = "php $cwd/app/console tenders:scheduler";
    $parse = "php $cwd/app/console tenders:scheduler";
    $load = "php $cwd/app/console tenders:scheduler";
    $index = "php $cwd/app/console tenders:scheduler";

    if ($checkF) {
        print "\n starting check/update uids....";
        $dir = __DIR__ . '/..';
        exec($check, $output);
        var_dump($output);
    }
    $pullTime = new DateTime();
    print "processing...";

    $ids = array();
    $extra = array();
    $requests = array(
        'new' => array(
            'getId' => "SELECT uid from tenders.Publications where status =3 order by uid desc limit ",
            'setStatus' => "UPDATE tenders.Publications set status = 2 where uid = ",
            'clearBase' => "UPDATE tenders.Publications set status = 3 where status = 2"
        ),
        'parse' => array(
            'getId' => "SELECT id from tenders.Resources where status =1 and name like '%docListPage%' order by uid desc limit ",
            'setStatus' => "UPDATE tenders.Resources set status = 2 where id = ",
            'clearBase' => "UPDATE tenders.Resources set status = 3  where status = 2 and name like '%docListPage%' ",
        ),
        'load' => array(
            'getId' => "SELECT id from tenders.Resources where status =3 order by uid desc limit ",
            'setStatus' => "UPDATE tenders.Resources set status = 2 where id = ",
            'clearBase' => "UPDATE tenders.Resources set status = 3 where status = 2  ",
        ),
        'index' => array(
            'getId' => "SELECT id from tenders.Resources where status =1 and indexed = 0  and name not like '%docListPage%' order by uid desc limit ",
            'setStatus' => "UPDATE tenders.Resources set status  = 2 where id = ",
            'clearBase' => "UPDATE tenders.Resources set status = 3 where status = 2  ",
    )

    );
    //$cmd = "php app/console tenders:scheduler";
    //$cwd = __DIR__ . "/../";

    $dir = __DIR__ . '/logs';
    if (!file_exists($dir)) {
        mkdir($dir);
    }
    $descriptorspec = array(
        0 => array("pipe", "r"), // stdin - канал, из которого дочерний процесс будет читать
        1 => array("pipe", "w"), // stdout - канал, в который дочерний процесс будет записывать
        2 => array("file", $dir . '/output.log', "a") // stderr - файл для записи
    );
	print "\n connecting to DataBase...";
	 $params = getParams();
     $user = $params["user"];
     $pass = $params["pass"];
     $host = $params["host"];
     $dbname = $params["name"];
	 $dbh = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
    print "\n initializing processes...";

    if (!empty($newNumber)) {
        for ($i = 0; $i < $newNumber; $i++) {

            $extra['cmd'][] = $new;
            $extra['flag'][] = 'new';
            $extra['requests'][] = $requests['new'];
            $extra['prefix'][] = '-c';
            $nid = GetId(count($extra['flag']) - 1, 1, true );
            if(!$nid) {continue;} else {$nid = $nid[0];}
            $cm = $new . ' -c ' . $nid;
            $resH[] = proc_open($cm, $descriptorspec, $pipe, $cwd);
            $extra['pipes'][] = $pipe;
        }
    }
    if (!empty($parseNumber)) {
        for ($i = 0; $i < $parseNumber; $i++) {

            $extra['cmd'][] = $parse;
            $extra['flag'][] = 'parse';
            $extra['requests'][] = $requests['parse'];
            $extra['prefix'][] = '-f';
            $pid = GetId(count($extra['flag']) - 1, 1);
            if(!$pid) {continue;} else {$pid = $pid[0];}
            $cm = $parse . ' -f ' . $pid;
            $resH[] = proc_open($cm, $descriptorspec, $pipe, $cwd);
            $extra['pipes'][] = $pipe;
        }
    }
    if (!empty($loadNumber)) {
        for ($i = 0; $i < $loadNumber; $i++) {
            $extra['prefix'][] = '-l';
            $extra['flag'][] = 'load';
            $extra['requests'][] = $requests['load'];
            $extra['cmd'][] = $load;
            $lid = GetId(count($extra['flag']), 1);
            if(!$lid) {continue;} else {$lid = $lid[0];}
            $tt = $load . $lid;
            $cm = $load . ' -l ' . $lid;
            $resH[] = proc_open($cm, $descriptorspec, $pipe, $cwd);
            $extra['pipes'][] = $pipe;

        }
    }

    if (!empty($indexNumber)) {
        for ($i = 0; $i < $indexNumber; $i++) {

            $extra['flag'][] = 'index';
            $extra['requests'][] = $requests['index'];
            $extra['cmd'][] = $index;
            $lid = doGetIds($requests['index'], 1);
            if(!$lid) {continue;} else {$lid = $lid[0];}
            $tt = $index . $lid;
            $extra['prefix'][] = '-i';
            $cm = $index . ' -i ' . $lid;
            $resH[] = proc_open($cm, $descriptorspec, $pipe, $cwd);
            $extra['pipes'][] = $pipe;

        }
    }

    print "\n done";
    print "\n main cycle .... ";
    $a = true;
    if (!empty($resH)) {
        while ($a) {

            foreach ($resH as $ind => $res) {
                sleep(0.25);
                $status = proc_get_status($res);
                if (!empty($status['running'])) {
                    print "\n$ind is running";
                    // $data = fread($pipes[$ind][1], 1000000);
                    //  print $data;
                } else {
                    echo "\n$ind not running";
                    echo "\n info:";
                    $data = fread($extra['pipes'][$ind][1], 1000000);
                    print  $data;
                    unset($pipe);
                    $num = 10;
                    $newIds = getId($ind, $num);
                    if ($newIds) {
                        $options = '';
                        foreach($newIds as $id) {
                            $options .= ' ' . $extra['prefix'][$ind] . ' ' . $id;
                            file_put_contents("./ids.txt", $id."\n", FILE_APPEND);
                            print $id."\n";
                        }
                        $cm = $extra['cmd'][$ind] . $options;
                        $resH[$ind] = proc_open($cm, $descriptorspec, $pipe, $cwd);
                        $extra['pipes'][$ind] = $pipe;
                        echo "\n $ind renewed";
                        sleep(0.5);
                    } else {
                        print "\n {$extra['flag'][$ind]} process number $ind not renewed : no records in the turn or error";
                        continue;
                    }
                }
            }
            if ($checkF) {
                $a = true;
                //sleep(10);
                $now = new DateTime();
                $interval = $pullTime->diff($now);
                $inter = $interval->format("%I");
                if ($inter >= 1) {
                    print "\nchecking for new id's to pull...";
                    exec($check, $output);
                    $pullTime = new DateTime();
                    var_dump($output);
                    print "\ndone, continue load...";
                }
            }
        }
    }
    print "\n  exiting.";

}

$countNest = 0;
function getId($ind, $num){

    global $extra, $ids, $check, $checkF, $countNest;
    $countNest ++;
    if (!empty($ids[$extra['flag'][$ind]]) and is_array($ids[$extra['flag'][$ind]])) {
        $input = &$ids[$extra['flag'][$ind]];
        $count = count($input) - $num;
        if($count <= 0) {$count = 0;}
        $idAr = array_splice($input, $count);
    } else {
        $requests = $extra['requests'][$ind];
        $Ids = doGetIds($requests);
        $ids[$extra['flag'][$ind]] = $Ids;
        if (!empty($ids[$extra['flag'][$ind]]) and is_array($ids[$extra['flag'][$ind]])) {
            $input = &$ids[$extra['flag'][$ind]];
            $count = count($input) - $num;
            if($count <= 0) {$count = 0;}
            $idAr = array_splice($input, $count);
        } else {
            if ($extra['flag'][$ind] === 'new' && $checkF && $countNest < 50) {
                print"\nno uids in Publication turn, launching check process.. ";
                exec($check, $output);
                var_dump($output);
                print"\n done.\n continue..";
                print "\n recursive getting id..";
                $idAr = getId($ind, $num);
                print "\ndone";
            } else {
                $idAr = false;
            }
        }
    }
    $countNest =  0;
    return $idAr;
}

function doGetIds($requests, $num = 100, $new = false)
{
    global $check, $checkF, $dbh;
    if (empty($requests['setStatus']) or empty($requests['getId']) or empty($requests['clearBase'])) {
        print "\nERROR: bad SQL requests were given to getIds()";
        return false;
    }
    try {
//        if($num > 1) {
//        $bCSql = $requests['clearBase'];
//        $cRes = $dbh->query($bCSql);
//        print "\n the base was cleared";
//        }

        $sql = $requests['getId'] . $num;
        $qRes = $dbh->query($sql)->fetchAll();
        if (empty($qRes)) {
            if ($new && $checkF) {
                print"\nno uids in Publication turn, launching check process.. ";
                exec($check, $output);
                var_dump($output);
                print"\n done.\n continue..";
                print "\n recursive getting id..";
                print "\ndone.\$ ";
                return doGetIds($requests, $num);
            }
            return false;
        }

        foreach ($qRes as $row) {
            $sql = $requests['setStatus'] . $row[0];
            $result[] = $row[0];
            $res = $dbh->query($sql);
            if (!$res) {
                print "\n\n ERROR : can't set status 2 to record with id {$row[0]} (request : $sql)";
                return false;
            }
        }
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
    if ($num === 1) {
        $result = $result[0];
    }
    if (!empty($result)) {
        return $result;
    } else {

        return false;
    }
}


function getParams()
{
    $file = __DIR__ . "/../app/config/parameters.yml";
    if (!file_exists($file)) {
        print "\ncan't get file with parameters";
        return false;
    }
    $content = file_get_contents($file);

    $patterns = array(
        'pass' => "/database_password:[\s]*(\w.+)*/",
        'user' => "/database_user:[\s]*(\w.+)*/",
        'host' => "/database_host:[\s]*(\w.+)*/",
        'name' => "/database_name:[\s]*(\w.+)*/"
    );
    foreach ($patterns as $parameter => $pattern) {
        $pass = preg_match($pattern, $content, $matches);
        if (!$pass or empty($matches[1])) {
            print "\n can't get parameter $parameter from file, regexp reterned false";
            return false;
        } else {
            $parameters[$parameter] = $matches[1];
        }
    }

    return $parameters;
}

?>