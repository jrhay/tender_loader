<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 12.08.13
 * Time: 15:56
 * To change this template use File | Settings | File Templates.
 */
//This script prepare project fro testing: deleting logs

print "\n preparing project...";
$logDir = __DIR__ . '/../app/logs';
$outputDir = __DIR__ . '/logs';
if (!file_exists($logDir)) {
    print "\n No directory $logDir";
} else {
    if ($handle = opendir($logDir)) {
        print "\npreparing $logDir....";
        while (($entry = readdir($handle)) !== false) {
            if ($entry == '.' or $entry == '..' or $entry == '.gitkeep') continue;
            $dir = $logDir . '/' . $entry;
            $un = unlink($dir);
            print "\n $dir unlinked.";
            if (!$un) {
                print "\n ERROR: can't unlink $entry, unlink() returned $un";
            }
        }
    } else {
        print "\n Can't open directory $logDir, opendir returned $handle";
    }
    print "\nSymfony Logs deleted...";

    if ($handle = opendir($outputDir)) {
        print "\npreparing $outputDir....";
        while (($entry = readdir($handle)) !== false) {
            if ($entry == '.' or $entry == '..' or $entry == ".gitkeep") continue;
            $dir = $outputDir . '/' . $entry;
            $un = unlink($dir);
            print "\n $dir unlinked.";
            if (!$un) {
                print "\n ERROR: can't unlink $entry, unlink() returned $un";
            }
        }
    } else {
        print "\n Can't open directory $outputDir, opendir returned $handle";
    }
    print "\nMulti-load script's logs deleted";
    print "\nDone,  project prepared";

}