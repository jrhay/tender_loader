<?php
namespace RAFFloader\SchedulerBundle\Command;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class SchedulerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('tenders:scheduler')
            ->setDescription('This command starts scheduler, which plans tenders downloading')
            ->addOption('download-new',    'c', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, "Starting download new lots")
            ->addOption('download-old',    'd', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Starting download old lots')
            ->addOption('check',           'k', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Starting download old lots')
            ->addOption('prepare-project', 'p', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Deleting logs from last launch')
            ->addOption('indexer',         'i', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'indexering')
            ->addOption('parse',           'f', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'parsing documents ( for example docListPage')
            ->addOption('load',            'l', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'start autonomous load process')
            ->addOption('test',            't', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'some command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $scheduler = $container->get('tenders.scheduler');

        $options = $input->getOptions();
        foreach ($options as $option => $valAr) {
            if (!empty($options[$option])) {
                if (!empty($valAr)) {
                    $value = $valAr;
                } else {
                    $value = NULL;
                }
                $markedOptions[$option] = $value;
            }
        }
        if (!empty($markedOptions)) {
            $text = $scheduler->run($markedOptions);
        } else {
            $text = " \n <fg=red> ERROR : You should use options to run sheduler,  try <fg=green> --help </fg=green>  to know possible options </fg=red> ";
        }
        $output->writeln($text);
    }
}