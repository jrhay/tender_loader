<?php
namespace RAFFloader\SchedulerBundle\DependencyInjection\Compiler;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class ScheduleCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('tenders.scheduler')) {
            $definition = $container->getDefinition(
                'tenders.scheduler'
            );

            $taggedServices = $container->findTaggedServiceIds(
                'tenders:scheduler'
            );

            foreach ($taggedServices as $id => $tagAttributes) {

                foreach ($tagAttributes as $attributes) {
                    $definition->addMethodCall(
                        'addSchedule',
                        array($attributes['schedule_tag'], $attributes['schedule_name'], new Reference($id), $attributes['schedule'], $attributes['period'])
                    );
                }

            }
        }
    }
}
