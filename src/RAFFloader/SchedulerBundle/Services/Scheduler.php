<?php
namespace RAFFloader\SchedulerBundle\Services;
use Doctrine\ORM\EntityManager;
use RAFFloader\SchedulerBundle\Entity\Schedule;
use Symfony\Component\HttpFoundation\Response;

/**
 * User: mario
 * Date: 21.06.13
 * Time: 17:10
 */
class Scheduler
{
    /** @var  EntityManager */
    private $em;
    /** @var  \ProjectServiceContainer */
    private $service_container;
    private $eFlag;

    public function  __construct($em, $service_container)
    {
        $this->service_container = $service_container;
        $this->em = $em;
    }

    private $schedules = array();


    public function addSchedule($schedule_tag, $schedName, $ref_id, $schedule, $period)
    {
        $this->schedules[$schedName] = array(
            'schedule_tag' => $schedule_tag,
            'schedule' => $schedule,
            'period' => $period,
            'ref_id' => $ref_id,
        );
    }

    public function run($options)
    {
        $statistics = array(
            'all' => 0,
            'executed' => 0,
            'executed_schedule_names' => array()
        );
        /** @var $repository \Doctrine\ORM\EntityRepository */
        $repository = $this->em->getRepository('SchedulerBundle:Schedule');


        foreach ($options as $option => $value) {
            foreach ($this->schedules as $schedule_name => $schedAttr) {
                if ($schedAttr['schedule_tag'] === $option) {
                    $statistics['all']++;
                    $new_record = false;
                    $schedule_record = $repository->findOneByName($schedule_name);
                    if (is_null($schedule_record)) {
                        $schedule_record = new Schedule();
                        $new_record = true;
                        $this->em->persist($schedule_record);
                        $schedule_record->setName($schedule_name);
                    }
                    $now_datetime = new \DateTime();
                    if ($new_record or $now_datetime->format('U') - $schedule_record->getDate()->format('U') > $schedAttr['period']) {
                        $schedule_record->setDate($now_datetime);
                        $service = $schedAttr['ref_id'];
                        $shedRes = $service->{$schedAttr['schedule']}($value);
                        if (!$shedRes) {
                            print "\n shedule returned false => probably executed with errors\n";
                            $this->eFlag = true;
                        } else {
                            $this->eFlag = false;
                        }
                        $statistics['executed']++;
                        $statistics['executed_schedule_names'][] = $schedule_name;

                    }

                    $this->em->flush();
                }


            }
        }
        $logger = $this->service_container->get('logger');
        if(!$this->eFlag){
        $logger->log(50, 'Scheduler', "schedule complete successfully ", array('schedule statistics' => $statistics));
        } else {
        $logger->log(50, 'Scheduler', "schedule finished with errors ", array('schedule statistics' => $statistics));
        }
        return $statistics;
    }
}
