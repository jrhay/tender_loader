<?php

namespace RAFFloader\SchedulerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use RAFFloader\SchedulerBundle\DependencyInjection\Compiler\ScheduleCompilerPass;

class SchedulerBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ScheduleCompilerPass());
    }
}
