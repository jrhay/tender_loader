<?php

namespace RAFFloader\SchedulerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ShedulerBundle:Default:index.html.twig', array('name' => $name));
    }
}
