<?php

namespace RAFFloader\DataContainerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DataContainerVariable
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class DataContainerVariable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="containerID", type="string", length=255)
     */
    private $containerID;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="blob")
     */
    private $value;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set containerID
     *
     * @param string $containerID
     * @return DataContainerVariable
     */
    public function setContainerID($containerID)
    {
        $this->containerID = $containerID;

        return $this;
    }

    /**
     * Get containerID
     *
     * @return string
     */
    public function getContainerID()
    {
        return $this->containerID;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return DataContainerVariable
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return DataContainerVariable
     */
    public function setValue($value)
    {

        $this->value = serialize($value);

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        if (is_resource($this->value)) {
            fseek($this->value,0,SEEK_SET);
            $val = fread($this->value,1000000);
        } elseif (is_string($this->value)) {
            $val = $this->value;
        }
        else{
            $val = false;
        }

        $value = unserialize($val);
        if(empty($value)){
            print "\n empty value \n";
        }
        return $value;

    }
}