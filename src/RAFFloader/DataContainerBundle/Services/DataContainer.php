<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 25.06.13
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */

namespace RAFFloader\DataContainerBundle\Services;
use RAFFloader\DataContainerBundle\Entity\DataContainerVariable;
use Doctrine\ORM\EntityManager;

class DataContainer
{
    private $ContainerID, $em;

    public function __construct($ContainerID, EntityManager $em)
    {
        $this->ContainerID = $ContainerID;
        $this->em = $em;
    }

    public function set($name, $value)
    {

        $Repository = $this->em->getRepository('DataContainerBundle:DataContainerVariable');
        $record = $Repository->findOneBy(array('containerID' => $this->ContainerID, 'name' => $name));
        if (is_null($record)) {
            $record = new DataContainerVariable();
            $this->em->persist($record);
            $record->setName($name);
            $record->setContainerID($this->ContainerID);
        }
        $record->setValue($value);
        $this->em->flush();
    }

    public function get($name, $default_value = null)
    {
        $Repository = $this->em->getRepository('DataContainerBundle:DataContainerVariable');
        /** @var $record DataContainerVariable */
        $record = $Repository->findOneBy(array('containerID' => $this->ContainerID, 'name' => $name));
        return is_null($record) ? $default_value : $record->getValue();

    }

}
