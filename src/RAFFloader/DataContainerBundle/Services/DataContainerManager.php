<?php
namespace RAFFloader\DataContainerBundle\Services;
use RAFFloader\DataContainerBundle\Entity\DataContainerVariable;
use RAFFloader\DataContainerBundle\Services\DataContainer;

class DataContainerManager
{
    private $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    public function getDataContainer($ContainerID)
    {
        return new DataContainer($ContainerID, $this->em);
    }


}