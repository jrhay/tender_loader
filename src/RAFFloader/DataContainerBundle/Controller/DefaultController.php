<?php

namespace RAFFloader\DataContainerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('DataContainerBundle:Default:index.html.twig', array('name' => $name));
    }
}
