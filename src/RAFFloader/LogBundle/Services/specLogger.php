<?php

/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 01.07.13
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\LogBundle\Services;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use RAFFloader\LogBundle\Entity\Logs;

class specLogger
{    /** @var Logger */
    private $logger;
    /** @var \Doctrine\ORM\EntityManager */
    private $EntityManager;
    private $log_to_db;
    private $logNumber;
    private $log_to_file;
    private $logDir;
    private $nativelogDirs = array(
        'debug' => '/logs/debug.log',
        'info' => '/logs/info.log',
        'error' => '/logs/error.log',
    );
    private $logDirs = array(
        'debug' => '/logs/debug.log',
        'info' => '/logs/info.log',
        'error' => '/logs/error.log',
    );

    //TODO: посомтреть как парсить конфиги

    public function __construct($em, $logToDb = true, $logToFile = true, $logDir = null)
    {
        $this->EntityManager = $em;
        $this->log_to_db = $logToDb;
        $this->log_to_file = $logToFile;
        $this->logNumber = $this->logDirs;
        foreach($this->logNumber as $name =>$number){
            $this->logNumber[$name]= 1 ;
        }
        if (!is_null($logDir)) {
            $this->logDir = $logDir;
        } else {
            $this->logDir = __DIR__ . '/../../../../app';
        }
    }

    public function log($logLevel, $log_sender, $message, $message_data = array())
    {
        $dateTime = new \DateTime();
        $format = 'Y-M-d  H:i:s';
        $formattedDateTime = $dateTime->format($format);
        $message =$log_sender.':'.$message;
        if ($this->log_to_file) {
            $this->logger = new Logger('defaultLogger');
            $this->pushHandlerstoLogger();
            switch ($logLevel) {
                case ($logLevel <= 33) :
                    $this->logger->debug($message, $message_data);
                    break;
                case ($logLevel <= 66) :
                    $this->logger->info($message, $message_data);
                    $this->logger->debug($message, $message_data);
                    print "\n".'['.$formattedDateTime.']          '.$message;
                    break;
                case ($logLevel <= 100) :
                    $this->logger->error($message, $message_data);
                    $this->logger->debug($message, $message_data);
                    print "\n".'['.$formattedDateTime.']          '.$message;
                    break;
                default :
            }
        }
        if ($this->log_to_db and $this->logDir) {
            $log = new Logs();
            $this->EntityManager->persist($log);
            $log->setLevel($logLevel);
            $log->setLogSender($log_sender);
            $log->setMessage($message);
            $log->setLogDatetime(new \DateTime('now'));
            $log->setInformation($message_data);
            $this->EntityManager->flush();
        }
        $this->changeLogFiles();
    }

    private
    function pushHandlerstoLogger()
    {
        $this->logger->pushHandler(new StreamHandler($this->logDir . $this->logDirs['debug'], Logger::DEBUG, false));
        $this->logger->pushHandler(new StreamHandler($this->logDir . $this->logDirs['info'], Logger::INFO, false));
        $this->logger->pushHandler(new StreamHandler($this->logDir . $this->logDirs['error'], Logger::WARNING, false));
    }

    private function changeLogFiles()
    {

        foreach ($this->logDirs as $dirname => $dir) {

            if(!file_exists($this->logDir.$dir)){
                echo "\ncreating new log file: \n".$dir;
                file_put_contents($this->logDir.$dir,"\nNew log file, created at ".date("Y-m-d H:i:s")." \n");
            }

            if (filesize($this->logDir . $dir) > 50000000) {
                    $nativeDir = $this->nativelogDirs[$dirname];
                    $dirArr = explode('/', $nativeDir);
                        $filename = array_pop($dirArr);
                            $nameArr = explode('.', $filename);
                                $extension = array_pop($nameArr);
                                $name = array_pop($nameArr);
                                     ++$this->logNumber[$dirname];
                                $name .= $this->logNumber[$dirname];
                            array_push($nameArr, $name);
                            array_push($nameArr, $extension);
                        $newFilename = implode('.', $nameArr);
                    array_push($dirArr, $newFilename);
                    $newDir = implode('/', $dirArr);
                $this->logDirs[$dirname] = $newDir;
            }
        }

        foreach($this->logDirs as $name =>$dir){
            if(!file_exists($this->logDir.$dir)){
                file_put_contents($this->logDir.$dir,"\nNew log file, created at ".date("Y-m-d H:i:s")." \n");
                echo "\n new log files created : \n" .$dir;
            }

        }
    }
}
