<?php

namespace RAFFloader\LogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Logs
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Logs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="integer")
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="logSender", type="string", length=255)
     */
    private $logSender;

    /**
     * @var string
     *
     * @ORM\Column(name="information", type="blob")
     */
    private $information;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="blob")
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="logDatetime", type="datetime")
     */
    private $logDatetime;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return Logs
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set logSender
     *
     * @param string $logSender
     * @return Logs
     */
    public function setLogSender($logSender)
    {
        $this->logSender = $logSender;

        return $this;
    }

    /**
     * Get logSender
     *
     * @return string 
     */
    public function getLogSender()
    {
        return $this->logSender;
    }

    /**
     * Set information
     *
     * @param string $information
     * @return Logs
     */
    public function setInformation($information)
    {
        $this->information = serialize($information);

        return $this;
    }

    /**
     * Get information
     *
     * @return string 
     */
    public function getInformation()
    {
        if (is_resource($this->information)) {
            $val = fread($this->information,10000000);
        } elseif (is_string($this->information)) {
            $val = $this->information;
        }
        else{
            $val = false;
        }

        return unserialize($val);
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Logs
     */
    public function setMessage($message)
    {
        $this->message = serialize($message);
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        if (is_resource($this->message)) {
            $val = fread($this->message,10000000);
        } elseif (is_string($this->message)) {
            $val = $this->message;
        }
        else{
            $val = false;
        }

        return unserialize($val);

    }

    /**
     * Set logDatetime
     *
     * @param \DateTime $logDatetime
     * @return Logs
     */
    public function setLogDatetime($logDatetime)
    {
        $this->logDatetime = $logDatetime;

        return $this;
    }

    /**
     * Get logDatetime
     *
     * @return \DateTime 
     */
    public function getLogDatetime()
    {
        return $this->logDatetime;
    }
}
