<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 23.07.13
 * Time: 16:33
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\IndexerBundle\Services;
interface  IIndexer
{
    /**
     *index Resource
     *@param integer */
    public function indexResource($id);
}
