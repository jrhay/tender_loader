<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 23.07.13
 * Time: 17:23
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\IndexerBundle\Services;
use Doctrine\ORM\EntityManager;
use RAFFloader\IndexerBundle\Services\IIndexer;
use RAFFloader\IndexerBundle\Services\IndexerFiles\Parser;
use RAFFloader\LogBundle\Services\specLogger;
use RAFFloader\ResourceManagerBundle\Services\ResourceManager;
use RAFFloader\ResourceManagerBundle\Services\Resources\BaseResource;
use RAFFloader\ResourceManagerBundle\Services\Resources\Resource;
use Symfony\Component\Config\Definition\Exception\Exception;


//TODO: решить способ применения бандла Индексатора и в соответствии с этим рефакторить все или удалить
class Indexer implements IIndexer
{
    /** @var  EntityManager */
    protected $entity_manager;
    /** @var  \ProjectServiceContainer */
    protected $service_container;
    protected $parser;
    /** @var   ResourceManager */
    private $resource_manager;
    /** @var  specLogger */
    private $logger;
    protected $IndexList = array(
        'name' => array('name', 'fullpath', 'system_uid'),
    );

    /**
     *index Resource
     * @param integer
     */
    public function indexResource($ids)
    {
        foreach($ids as $id) {
        /** @var BaseResource $resource */
        $resource = $this->resource_manager->getResource($id);
        if (empty($resource)) {
            print "\n ERROR INDEXING: BAD RESOURCE";
            continue;
        }
        $raw = $resource->getRaw();
        $info = $resource->getInfo();
        $uid = $info['uid'];
        $platform = $info['platform'];


        if (!$uid or !$raw) {
            print '\n ERROR,Indexer: no uid or raw';
            continue;
        }
        $name = $info['name'];
        if ($name === 'Main') {
            /** @var /ParserFiles\Business\TenderImportData $data */
            $data = new IndexerFiles\ParserFiles\Business\TenderImportData();
            $data = $this->parser->parseMainRaw($raw, $uid);
            var_dump($data);
            if (empty($data->fields[IndexerFiles\ParserFiles\Enums\ELotFieldName::SYSTEM_UID])) {
                print "\nНе удвлось распарсить(нет SYSTEM_UID, выход.\n";
                continue;
            }
//Отправляем печатную форму публикации:
            $result = array(
                array(
                    'id' => $platform . '_' . $id,
                    'publication_uri' => $data->fields['publication_uri'],
                    'publication_title' => $data->fields['title'],
                    'publication_number' => $data->fields['import_number'],
                    'import_uid' => $data->fields['import_uid'],
                    'budget' => $data->fields['budget_initial'],
                ),

            );
            $cu = curl_init('http://ec2-54-228-0-147.eu-west-1.compute.amazonaws.com/solr/tenders-test2/update?commit=true'); //http://localhost:8983/solr/tenders-test2/update?commit=true
            //$cu = curl_init('http://ec2-54-228-0-147.eu-west-1.compute.amazonaws.com');
            curl_setopt($cu, CURLOPT_HTTPHEADER, array(
                'Content-type: application/json',
            ));
            //  var_dump(json_encode($result));
            curl_setopt($cu, CURLOPT_POSTFIELDS, json_encode($result));
           curl_setopt($cu, CURLOPT_RETURNTRANSFER, true);

            try {
            $res = curl_exec($cu);
                if($res === false) {
                    throw new Exception(curl_error($cu), curl_errno($cu));
                }
            } catch (Exception $e){
                $message = sprintf(
                    'Can\'t send Main form for indexing: Curl failed with error #%d: %s',
                    $e->getCode(), $e->getMessage());
//                trigger_error($message,E_USER_ERROR);
                $this->logger->log(75, 'Indexer', $message);
            }

            if ($res) {
                $res = $this->resource_manager->setIndexed($id, 1);
                if(!$res){
                    $this->logger->log(75, 'Indexer', "ERROR: can't set Indexed = 1 to $id" );
                    continue;
                }
                $resS = $this->resource_manager->setStatus($id, 1);
                if(!$resS){
                    $this->logger->log(75, 'Indexer', "ERROR: can't set Status = 1 to $id" );
                    continue;
                }
            } else {
                $res = $this->resource_manager->setIndexed($id, 4);
                if(!$res){
                    $this->logger->log(75, 'Indexer', "ERROR: can't set Indexed = 4 to $id" );
                    continue;
                }
                $resS = $this->resource_manager->setStatus($id, 1);
                if(!$resS){
                    $this->logger->log(75, 'Indexer', "ERROR: can't set Status = 1 to $id" );
                    continue;
                }

            }
            if (!$res) {
                $this->logger->log(75, 'Indexer', 'Can\'t set Indexed to Main resource');
                continue;
            }
            $this->logger->log(50, 'Indexer', 'Field Indexed was set to record');
            continue;
        } else {

            $tempFile = $this->resource_manager->createTemp($id);
            $docResault = $this->parser->parseDocs($tempFile, $uid);

            var_dump($docResault);

            foreach ($docResault as $uid => $uidArray) {
                foreach ($uidArray as $docName => $fullLocalPath) {
                    $docName = urlencode(str_replace(array('(', ')'), array('|', '|'), $docName));
                    $system_uid = $platform . '_' . $uid;
                    $path = $fullLocalPath;
                    $trashId = urlencode(uniqid('', true));
                    $postDir = __DIR__ . '/';
                    $command = 'java -Durl="http://ec2-54-228-0-147.eu-west-1.compute.amazonaws.com/solr/docs/update?commit=true&literal.name=' . $docName . '&literal.id=' . $trashId . '&literal.system_uid=' . $system_uid . '" -Dauto -jar "' . $postDir . 'post.jar" "' . $path . '"';
                    unset($output);
                    $action = exec($command, $output);
                    if (file_exists($path)) {
                        $res = $this->resource_manager->deleteTempFile($path);
                        //unlink($path);
                    }
                    if (strpos($action, 'error') === false
                        or strpos($action, 'Error') === false
                        or strpos($action, 'Fatal') === false
                        or strpos($action, 'Unable to access jarfile') === false
                        or strpos($action, 'FATAL') === false) {

                        $resI = $this->resource_manager->setIndexed($id, 1);
                        if(!$resI){
                            $this->logger->log(75, 'Indexer', "ERROR: can't set Indexed = 1 to $id" );
                            continue;
                        }
                        $resS = $this->resource_manager->setStatus($id, 1);
                        if(!$resS){
                            $this->logger->log(75, 'Indexer', "ERROR: can't set Status = 1 to $id" );
                            continue;
                        }
                    } else {
                        $resI = $this->resource_manager->setIndexed($id, 4);
                        if(!$resI){
                            $this->logger->log(75, 'Indexer', "ERROR: can't set Indexed = 4 to $id" );
                            continue;
                        }
                        $resS = $this->resource_manager->setStatus($id, 1);
                        if(!$resS){
                            $this->logger->log(75, 'Indexer', "ERROR: can't set Status = 1 to $id" );
                            continue;
                        }
                    }
                    if (!$res) {
                        $this->logger->log(75, 'Indexer', 'Can\'t set Indexed to Main resource');
                        continue;
                    }
                    $this->logger->log(50, 'Indexer', "Field Indexed was set to record\n");;
                }
            }
            $res = $this->resource_manager->deleteTempFile($tempFile);

            if (!$res) {
                $this->logger->log(75, 'INDEXER', 'WARNING; can\' delete temp file');
            }
            continue;
        }
    }
        return true;
    }


    /**
     *index Directory
     * @param string $dir
     */
    public function indexDir($dir)
    {
        foreach (scandir($dir) as $file) {
            if ($file == '.' || $file == '..' || ($file == '*98(' and is_dir($dir . '/' . $file))) continue;
            $full_path = $dir . '/' . $file;
            if (is_dir($full_path)) {
                if ($file == 'Publication') {
                    $this->process_publication($dir);
                } else {
                    $this->indexDir($full_path);
                }
            }
        }
    }



    function __construct($cont, $em)
    {
        $this->service_container = $cont;
        $this->entity_manager = $em;
        $this->resource_manager = $this->service_container->get('resource_manager');
        $this->parser = new Parser();
        $this->logger = $this->service_container->get('logger');
    }

    protected function getIndexList($params)
    {

        //TODO:реализовать получение списка ресурсов для индексации


        return $this->IndexList;
    }

}
