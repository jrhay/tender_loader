<?php

use RAFFloader\IndexerBundle\Services\IndexerFiles\Parser;

include_once(__DIR__ . '/autoload.php');

// Адрес папки в архиве, в которой лежит Publication передавать методу parse()
$parser = new Parser();

$dir = __DIR__ . '/../in';

process_dir($dir);

function process_dir($dir)
{
    foreach (scandir($dir) as $file) {
        if ($file == '.' || $file == '..') continue;

        $full_path = $dir . '/' . $file;
        if (is_dir($full_path)) {
            if ($file == 'Publication') {
                process_publication($dir);
            } else {
                process_dir($full_path);
            }
        }
    }
}

function process_publication($dir)
{
    global $parser;

    echo 'Processing: ' . $dir . PHP_EOL;
//Парсим публикацию
    $result = $parser->parseMainPrintForm($dir);
    var_dump($result);
//Отправляем печатную форму публикации:
    $data = array(
        array(
            'id' => $result->fields['system_uid'],
            'publication_uri' => $result->fields['publication_uri'],
            'publication_title' => $result->fields['title'],
            'publication_number' => $result->fields['import_number'],
            'import_uid' => $result->fields['import_uid'],
            'budget' => $result->fields['budget_initial'],
        ),

    );
    $cu = curl_init('http://localhost:8983/solr/tenders-test2/update?commit=true'); //http://localhost:8983/solr/tenders-test2/update?commit=true
    curl_setopt($cu, CURLOPT_HTTPHEADER, array(
        'Content-type: application/json',
    ));
    //  var_dump(json_encode($data));
    curl_setopt($cu, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($cu, CURLOPT_RETURNTRANSFER, true);

    $res = curl_exec($cu);

    var_dump($res);

    //собираем доки

    $docResault = $parser->parseDocs($dir);

    //отправляем доки
    var_dump($docResault);

    foreach ($docResault as $uid => $uidArray) {
        foreach ($uidArray as $docName => $fullLocalPath) {

            $docName = urlencode(str_replace(array('(', ')'), array('|', '|'), $docName));
            $system_uid = urlencode($result->fields['system_uid']);
            $path = $fullLocalPath;
            $id = urlencode(uniqid('', true));
            $postDir = __DIR__ . '/';
            $command = 'java -Durl="http://localhost:8983/solr/docs/update?commit=true&literal.name='.$docName.'&literal.id='.$id.'&literal.system_uid='.$system_uid.'" -Dauto -jar "'.$postDir.'post.jar" "'.$path.'"';
            unset($output);
            $action = exec($command, $output);
            // var_dump($output);
        }
    }


//	die();
}
//$result = $parser->parse('/data/projects/tendex-test/in/zakupki_gov_ru/6359/771/');
//var_dump($result);
