<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 10.07.13
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\IndexerBundle\Services\IndexerFiles\Interfaces;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\TenderImportData;
use RAFFloader\ResourceManagerBundle\Services\Storages\Interfaces\IResourceCollection;

interface IParser
{
/**
     * @param $uid string
     * @param $raw string
     * @return TenderImportData */
    public function parseMainRaw($raw, $uid);

}
