<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Platforms;

use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\PlatformManager;

/**
 * Абстрактный класс-платформа с базовыми для всех функциями
 */
abstract class APlatform
{
    /**
     * Контейнер хранящий менеджер
     * @var PlatformManager
     */
    private $manager;
    private $collection;

    /**
     * Создает пустой контейнер зависимостей
     */
    function __construct(PlatformManager $manager = null, $collection)
    {
        if (!is_null($manager)) {
            $this->setManager($manager);
        } else {
            $this->setManager(new PlatformManager());
        }
        $this->collection = $collection;
    }

    /**
     * Принудительно назначает необходимый менеджер
     * @param PlatformManager $manager
     */
    public function setManager(PlatformManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Возвращает назначенный платформе менеджер
     * @return PlatformManager
     */
    public function getManager()
    {
        return $this->manager;
    }

    function giveHTTPContent($id, $opts = array())
    {

        //пока не требуется
    }
}