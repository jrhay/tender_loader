<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Platforms;

use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\TenderImportData;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\PlatformSearchParams;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ELotFieldName;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ENotificationPlatformName;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ESearchFields;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ESearchFieldsOperability;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ELotType;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Formatters\Formatters;

class PlatformZakupkiGovRu extends APlatform implements INotificationPlatform, IGetPublicationUrl
{
    //флаг для пояснения ФЗ лота (принимает значения 94 и 223)
    private $flag;

    function import(TenderImportData $data,$xml)
    {

        $import_url = '';
        // Получаем адрес импорта
//        if(isset($data->fields[ELotFieldName::PUBLICATION_URI])
//            and preg_match('/notificationId=(\d+)/', $data->fields[ELotFieldName::PUBLICATION_URI], $id)) {
//
//            $data->fields[ELotFieldName::IMPORT_UID] = $id[1];
//            $import_url = 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=' . $id[1];
//
//        } elseif(isset($data->fields[ELotFieldName::IMPORT_URI])
//            and preg_match('/printForm\?type=NOTIFICATION&id=(\d+)/', $data->fields[ELotFieldName::IMPORT_URI], $id)) {
//
//            $data->fields[ELotFieldName::IMPORT_UID] = $id[1];
//            $import_url = 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=' . $id[1];
//
//        } elseif(isset($data->fields[ELotFieldName::IMPORT_UID])) {
//
//            $import_url = 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=' . $data->fields[ELotFieldName::IMPORT_UID];
//            $data->fields[ELotFieldName::IMPORT_URI] = $import_url;
//
//        } elseif(isset($data->fields[ELotFieldName::IMPORT_NUMBER])) {
//
//            $publ_url = $this->getPublicationUrl($data);
//
//            if(!is_null($publ_url) and preg_match('/notificationId=(\d+)/', $publ_url, $id)){
//
//                $import_url = 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=' . $id[1];
//                $data->fields[ELotFieldName::IMPORT_URI] = $import_url;
//                $data->fields[ELotFieldName::IMPORT_UID] = $id[1];
//
//            }
//        }
        if (!$xml) {
            print "\n ERROR: NO XML ";
            return $data;
        }

        $answer = $xml;
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($xml);
        libxml_use_internal_errors(false);
        if (!$xml) {
            echo "\nПопытка распарсить по 223 ФЗ";
//            foreach(libxml_get_errors() as $error) {
//                echo "\t", $error->message;
            $needle = "<div id=\"tabs-2\" style=\"height:100%; overflow: auto;\">";
            $pos = strpos($answer, $needle);
            $answer = substr($answer, $pos + strlen($needle), -1);
            $needle = "</div>";
            $pos1 = strpos($answer, $needle);
            $answer = substr($answer, 0, $pos1);
            $answer = html_entity_decode($answer);
            $answer = str_replace('ns2:', '', $answer);
            $xml = simplexml_load_string($answer);


            if (!$xml) {
                print "\nНе удалось распарсить страницу.";
                print "\nВыходим из лота..\n ";
                return $data;
            } else {
                print "\n загрузка в xml : удачно.";
                $this->flag = 223;
//                foreach ($xml as $field) {
//                    var_dump($field);
//                }
            }
        } else {
            $this->flag = 94;
        }
        $lotOrdinal = isset($data->fields[ELotFieldName::LOT_ORDINAL]) ? $data->fields[ELotFieldName::LOT_ORDINAL] : null;
        if($name = $xml->lots){
            $purData  = $xml;
        }
        if(empty($purData)){
        $purData = $xml->body->item->purchaseNoticeData;
        }
        if(empty($purData)){
            $purData =  $xml->body->item->purchaseNoticeOAData;
        }
        if(empty($purData)){
            $purData =  $xml->body->item->purchaseNoticeZKData;
        }
        if(empty($purData)){
            $purData =  $xml->body->item->purchaseNoticeAEData;
        }
        if(empty($purData)){
            $purData =  $xml->body->item->purchaseNoticeEPData;
        }
        if(empty($purData)){
            $purData =  $xml->body->item->purchaseNoticeOKData;
        }
        if ($this->flag === 94) {
            $multilot = 'true' == (string)$xml->multiLot;
            $lots = $xml->lots;
        } else {

            $lots = $purData->lots;
            if(empty($lots)){
            $lots = $purData->lots;
             print("\n ERROR: can't get lot from Main resource ,trying to continue");
            }
            $multilot = (bool)($lots->count() - 1);
            var_dump($lotOrdinal);
        }
        if ($multilot && !isset($lotOrdinal)) {
            return $data;
        }

        if ($multilot) {
            $lot = null;
            foreach ($xml->lots->lot as $it) {
                if ($it->ordinalNumber == $lotOrdinal) {
                    $lot = $it;
                    break;
                }
            }
            if (!isset($lot)) {
                return $data;
            }
            $data->fields[ELotFieldName::LOT_ORDINAL] = $lotOrdinal;
            $data->fields[ELotFieldName::LOT_TITLE] = (string)$lot->subject;
        } else {
            $lot = $lots->lot;
        }

        $dateFormat = Formatters::getDateFormatter('d.m.Y G:i:s|');
        $priceFormat = Formatters::getPriceFormatter();

        if ($this->flag === 94) {
            $codeId = $xml->placingWay->name;
        } else {
            $codeId = $purData->purchaseCodeName;
            if(empty($codeId)){
            $codeId = $purData->purchaseCodeName;
            }
        }
        $codeId = (string)$codeId;
        $codeId = strtolower($codeId); //не работает, setlocale(LC_ALL, 'ru_RU.CP1251'); не помогает
        $codeId = preg_replace('/[^\W]/', '', $codeId);
        $codeId = trim($codeId);
        array();
        switch ($codeId) {
            case 'Открытый конкурс': // открытый конкурс
                $type = ELotType::OPEN_CONTEST;
                if ($this->flag === 94) {
                    $data->fields[ELotFieldName::DATE_ENVELOPE_OPENING] = $dateFormat($xml->notificationCommission->p1Date);
                    $data->fields[ELotFieldName::DATE_REQUEST_STUDY] = $dateFormat($xml->notificationCommission->p2Date);
                    $data->fields[ELotFieldName::DATE_DEBRIEFING] = $dateFormat($xml->notificationCommission->p3Date);
                } else {
                    $data->fields[ELotFieldName::DATE_ENVELOPE_OPENING] = $dateFormat($purData->envelopeOpeningTime);
                    $data->fields[ELotFieldName::DATE_REQUEST_STUDY] = $dateFormat($purData->summingupTime);
                    $data->fields[ELotFieldName::DATE_SUBMISSION_END] = $dateFormat($purData->submissionCloseDateTime);
                }
                break;
            case 'Открытый аукцион': // открытый аукцион
                $type = ELotType::OPEN_AUCTION;
                if ($this->flag === 94) {
                    $data->fields[ELotFieldName::DATE_ENVELOPE_OPENING] = $dateFormat($xml->notificationCommission->p1Date);
                    $data->fields[ELotFieldName::DATE_REQUEST_STUDY] = $dateFormat($xml->notificationCommission->p2Date);
                    $data->fields[ELotFieldName::DATE_DEBRIEFING] = $dateFormat($xml->notificationCommission->p3Date);
                } else {
                $data->fields[ELotFieldName::DATE_NOTIFICATION_PUBLIC] = $dateFormat($purData->publicationDateTime);
                $data->fields[ELotFieldName::DATE_SUBMISSION_END] = $dateFormat($purData->submissionCloseDateTime);
                $data->fields[ELotFieldName::DATE_DEBRIEFING] = $dateFormat($purData->examinationDateTime);
                }
                break;
            case 'Запрос котировок': // запрос котировок
                $type = ELotType::REQUEST_FOR_QUOTATION;
                if ($this->flag === 94) {
                    $data->fields[ELotFieldName::DATE_SUBMISSION_BEGIN] = $dateFormat($xml->notificationCommission->p1Date);
                    $data->fields[ELotFieldName::DATE_SUBMISSION_END] = $dateFormat($xml->notificationCommission->p2Date);
                } else {
                    $data->fields[ELotFieldName::DATE_NOTIFICATION_PUBLIC] = $dateFormat($purData->publicationDateTime);
                    $data->fields[ELotFieldName::DATE_SUBMISSION_END] = $dateFormat($purData->submissionCloseDateTime);
                    $data->fields[ELotFieldName::DATE_DEBRIEFING] = $dateFormat($purData->quotationExaminationTime);
                }
                break;
            case 'Открытый запрос предложений в электронной форме': // Открытый запрос предложений в электронной форме
                $type = ELotType::REQUEST_FOR_PROPOSAL;
                if ($this->flag === 94) {

                } else {
                    $data->fields[ELotFieldName::DATE_NOTIFICATION_PUBLIC] = $dateFormat($purData->publicationDateTime);
                    $data->fields[ELotFieldName::DATE_SUBMISSION_END] = $dateFormat($purData->submissionCloseDateTime);
                    $data->fields[ELotFieldName::DATE_DEBRIEFING] = $dateFormat($purData->electronicPlaceInfo->examinationDateTime);

                }
                break;
            case 'Открытый аукцион в электронной форме': //
                $type = ELotType::OPEN_ONLINE_AUCTION;
                if ($this->flag === 94) {
                    $data->fields[ELotFieldName::DATE_SUBMISSION_END] = $dateFormat($xml->notificationCommission->p1Date);
                    $data->fields[ELotFieldName::DATE_FIRST_PART_END] = $dateFormat($xml->notificationCommission->p2Date);
                    $data->fields[ELotFieldName::DATE_ONLINE_AUCTION] = $dateFormat($xml->notificationCommission->p3Date);
                } else {
                    $data->fields[ELotFieldName::DATE_NOTIFICATION_PUBLIC] = $dateFormat($purData->publicationDateTime);
                    $data->fields[ELotFieldName::DATE_SUBMISSION_END] = $dateFormat($purData->submissionCloseDateTime);
                    $data->fields[ELotFieldName::DATE_DEBRIEFING] = $dateFormat($purData->electronicPlaceInfo->examinationDateTime);
                    $data->fields[ELotFieldName::DATE_ONLINE_AUCTION] = $dateFormat($purData->auctionTime);
                    $data->fields[ELotFieldName::DATE_REQUEST_STUDY] = $dateFormat($purData->applExamPeriodTime);
                }
                break;
            case 'Предварительный отбор': // Предварительный отбор
                $type = ELotType::PRELIMINARY_SELECTION;
                    $data->fields[ELotFieldName::DATE_SUBMISSION_BEGIN] = $dateFormat($xml->notificationCommission->p1Date);
                    $data->fields[ELotFieldName::DATE_SUBMISSION_END] = $dateFormat($xml->notificationCommission->p2Date);
                    $data->fields[ELotFieldName::DATE_PRELIMINARY_SELECTION] = $dateFormat($xml->notificationCommission->p3Date);
                break;
            case 'Закупка у единственного поставщика':
            case 'Сообщение о заинтересованности в проведении открытого конкурса':
            case 'Результаты рассмотрения и оценки котировочных заявок': // Результаты рассмотрения и оценки котировочных заявок
            default:
                $type = ELotType::OTHER;
        }


        $data->fields[ELotFieldName::LOT_TYPE] = $type;
        $data->notificationPlatform = $data->fields[ELotFieldName::NOTIFICATION_PLATFORM] =
            $data->fields[ELotFieldName::ORIGIN_NOTIFICATION_PLATFORM] = ENotificationPlatformName::ZAKUPKI_GOV_RU;
        if ($this->flag === 94) {

            $data->fields[ELotFieldName::DATE_NOTIFICATION_PUBLIC] = $dateFormat($xml->publishDate);
            $data->fields[ELotFieldName::IMPORT_NUMBER] = (string)$xml->notificationNumber;
            $data->fields[ELotFieldName::TITLE] = (string)$xml->orderName;
            $data->fields[ELotFieldName::CUSTOMER] = (string)$xml->order->placerOrganization->fullName;
            $data->fields[ELotFieldName::IMPORT_URI] = $import_url;
            $data->fields[ELotFieldName::PUBLICATION_URI] =
                'http://zakupki.gov.ru/pgz/public/action/orders/info/common_info/show?notificationId=' . $data->fields[ELotFieldName::IMPORT_UID];
            $data->fields[ELotFieldName::BUDGET_INITIAL] = $priceFormat($lot->maxPrice . " " . $lot->currency->code);
        } else {
            $data->fields[ELotFieldName::DATE_NOTIFICATION_PUBLIC] = $dateFormat($purData->publicationDateTime);
            $data->fields[ELotFieldName::IMPORT_NUMBER] = (string)$purData->registrationNumber;
            $data->fields[ELotFieldName::TITLE] = (string)$purData->name;
            $data->fields[ELotFieldName::CUSTOMER] = (string)$purData->customer->mainInfo->fullName;
            $data->fields[ELotFieldName::IMPORT_URI] = $import_url;
            $data->fields[ELotFieldName::PUBLICATION_URI] =
                'http://zakupki.gov.ru/223/purchase/public/notification/print-form/show.html?noticeId=' . $data->fields[ELotFieldName::IMPORT_UID];
            $data->fields[ELotFieldName::BUDGET_INITIAL] = $priceFormat($lot->initialSum . " " . $lot->currency->code);

        }
        if (isset($xml->etp)) {
            $data->fields[ELotFieldName::AUCTION_WEBSITE] = (string)$xml->etp->address;
        }
        if (isset($purData->urlVSRZ)){
            $data->fields[ELotFieldName::AUCTION_WEBSITE] = (string)$purData->urlVSRZs;
        }
        $data->fields[ELotFieldName::SYSTEM_UID] = $data->notificationPlatform . '-'
            . $data->fields[ELotFieldName::IMPORT_NUMBER];
        return clone $data;
    }

    function clearSumSpaceComma($x)
    {
//		$x = str_replace(',', ',', $x);
        $x = preg_replace('~[^\d\,]~u', '', $x);
        return (double)$x;
    }

    function clearSumCommaPeriod($x)
    {
        $x = str_replace(array(',', '.'), array('', ','), $x);
        $x = preg_replace('~[^\d\,]~u', '', $x);
        return (double)$x;
    }

    function search(PlatformSearchParams $params)
    {

        $params->platform = & $this;
        return new SearchItZakupkiGovRu($params);

    }

    function getSearchFields()
    {
        return array(
            ESearchFields::KEYWORDS => array(
                'operability' => ESearchFieldsOperability::AVAILABLE,
                'message' => '',
            ),
            ESearchFields::PRICE_INTERVAL => array(
                'operability' => ESearchFieldsOperability::AVAILABLE,
                'message' => '',
            ),
            ESearchFields::DATE_PUBLICATION_INTERVAL => array(
                'operability' => ESearchFieldsOperability::AVAILABLE,
                'message' => '',
            ),
            ESearchFields::CUSTOMER => array(
                'operability' => ESearchFieldsOperability::AVAILABLE,
                'message' => '',
            ),
            ESearchFields::CUSTOMER_INN => array(
                'operability' => ESearchFieldsOperability::WARNING,
                'message' => 'Используется только в том случае, когда не задано поле "Заказчик" ',
            ),
            ESearchFields::LOT_TYPE => array(
                'operability' => ESearchFieldsOperability::AVAILABLE,
                'message' => '',
            ),
            ESearchFields::LOT_NUMBER => array(
                'operability' => ESearchFieldsOperability::WARNING,
                'message' => 'Отдельного параметра нет. Используется как дополнительное ключевое слово.',
            ),
        );
    }

    function getPublicationUrl($data)
    {
        if (isset($data->fields[ELotFieldName::IMPORT_NUMBER])) {
            $code = $data->fields[ELotFieldName::IMPORT_NUMBER];
        } else {
            return null;
        }

        if (!preg_match("/'?\d*'?/", $code)) return null;

        $code = preg_replace('/\D/', '', $code);

        $url = 'http://zakupki.gov.ru/pgz/public/action/search/quick/run?currentSearchString=' . urlencode($code);
        $pattern = '//*[@id="searchResultContainer"]/table//tr[3]/td/table[1]//tr/td[2]/table//tr[1]/td/a';

        $answer = $this->getHTTPContent($url);

        libxml_use_internal_errors(true);
        $domDoc = new \DOMDocument('1.0', 'UTF-8');
        $domDoc->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . $answer);
        libxml_use_internal_errors(false);

        $xpath = new \DOMXPath($domDoc);
        $xResult = $xpath->query($pattern);

        if ($xResult->length == 0) {
            return null;
        } else {
            return 'http://zakupki.gov.ru' . $xResult->item(0)->getAttribute('href');
        }

    }

    function parseUrl($url)
    {

        if (preg_match('/.*zakupki.gov.ru\/pgz\/public\/action\/orders\/info\/common_info\/show\?notificationId=(\d+)/', $url, $id)) {

            $data = new TenderImportData();
            $data->fields[ELotFieldName::IMPORT_UID] = $id[1];

        } elseif (preg_match('/.*zakupki.gov.ru\/pgz\/printForm\?type=NOTIFICATION&id=(\d+)/', $url, $id)) {

            $data = new TenderImportData();
            $data->fields[ELotFieldName::IMPORT_UID] = $id[1];

        } else {
            return null;
        }

        $data->fields[ELotFieldName::IMPORT_URI] =
            'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=' . $data->fields[ELotFieldName::IMPORT_UID];
        return $data;
    }

    /**
     * Возвращает результат запроса по адресу $url с опциями $opts
     * @param $url Адрес запроса
     * @param $opts Дополнительные опции
     * @return string
     */
    function getHTTPContent($url, $opts = array())
    {
        // TODO: Implement getHTTPContent() method.
    }
}
