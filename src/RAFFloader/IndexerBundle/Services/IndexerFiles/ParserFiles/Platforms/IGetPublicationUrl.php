<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Platforms;

use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\TenderImportData;

interface IGetPublicationUrl
{
    /**
     * Пытается определить адрес публикации по ее номеру
     * @param TenderImportData $data Объект, включающий в себя ELotFieldName::IMPORT_NUMBER
     * @return string
     */
    function getPublicationUrl($data);
}