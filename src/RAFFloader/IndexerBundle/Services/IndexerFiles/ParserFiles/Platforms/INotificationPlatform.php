<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Platforms;

use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\TenderImportData;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\PlatformSearchParams;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\PlatformManager;

interface INotificationPlatform
{
    /**
     * Возвращает объект TenderImportData, состоящий из рапарсенных данных тендера.
     * @param TenderImportData $data Объект TenderImportData, включающий в себя уникальный идентификатор тендера.
     * @param $xml string
     * @return TenderImportData
     */
    function import(TenderImportData $data, $xml);

    /**
     * Возвращает объект итератора поиска
     * @param $params PlatformSearchParams
     * @return ASearchIterator
     */
    function search(PlatformSearchParams $params);

    /**
     * Возвращает описание полей, доступных для поиска на данной платформе.
     * @return array
     */
    function getSearchFields();

    /**
     * Пробует распарсить полученный адрес публикации. В случае успеха возвращает TenderImportData с необходимыми для импорта параметрами.
     */
    function parseUrl($url);

    /**
     * Принудительно назначает необходимый менеджер
     * @param PlatformManager $manager
     */
    function setManager(PlatformManager $manager);

    /**
     * Возвращает назначенный платформе менеджер
     * @return PlatformManager
     */
    function getManager();

    /**
     * Возвращает результат запроса по адресу $url с опциями $opts
     * @param $url Адрес запроса
     * @param $opts Дополнительные опции
     * @return string
     */
    function getHTTPContent($url, $opts = array());
}