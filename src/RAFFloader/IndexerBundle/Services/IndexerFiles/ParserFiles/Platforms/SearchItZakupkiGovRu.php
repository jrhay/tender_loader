<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Platforms;

use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\TenderImportData;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Formatters\Formatters;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ESearchFields;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ELotFieldName;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ENotificationPlatformName;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ELotType;

class SearchItZakupkiGovRu extends ASearchIterator
{

    protected function getLot()
    {
        $args = array();
        $result = array();

        // Наименование или номер заказа, лота или извещения
        $args['d'] = $this->params->getFieldValue(ESearchFields::KEYWORDS);
        $number = $this->params->getFieldValue(ESearchFields::LOT_NUMBER);
        if (!empty($number)) {
            $args['d'] .= ' ' . $number;
        }

        // Диапазон начальных цен
        $args['l'] = $this->params->getFieldValue(ESearchFields::PRICE_INTERVAL, 0);
        $args['m'] = $this->params->getFieldValue(ESearchFields::PRICE_INTERVAL, 1);

        // Заказчик
        $args['t'] = $this->params->getFieldValue(ESearchFields::CUSTOMER);
        if (is_null($args['t'])) {
            $args['t'] = $this->params->getFieldValue(ESearchFields::CUSTOMER_INN);
        }

        // Даты размещения заявки
        // От ...
        $args['i'] = $this->params->getFieldValue(ESearchFields::DATE_PUBLICATION_INTERVAL, 0);
        if (!empty($args['i'])) {
            $date = new \DateTime($args['i']);
            if ($date instanceof \DateTime) {
                $args['i'] = $date->format('d.m.Y');
            }
        }
        // По ...
        $args['p'] = $this->params->getFieldValue(ESearchFields::DATE_PUBLICATION_INTERVAL, 1);
        if (!empty($args['p'])) {
            $date = new \DateTime($args['p']);
            if ($date instanceof \DateTime) {
                $args['p'] = $date->format('d.m.Y');
            }
        }

        // Отображать отдельные лоты
        $args['lotView'] = 'true';

        // Учитывать морфологию
        $m = $this->params->getFieldValue(ESearchFields::KEYWORDS, 1);
        if (!empty($m)) {
            $args['e'] = 'true';
        }
        unset($m);

        // Технические параметры
        $args['tabName'] = 'AP';
        $args['descending'] = 'true';
        $args['ext'] = '';
        $args['sortField'] = 'lastEventDate';

        $args['b'] = array();
        if (empty($this->params->fields[ESearchFields::LOT_TYPE]) or !is_array($this->params->fields[ESearchFields::LOT_TYPE])) {
            $args['a'] = 'true'; // true для всех типов
        } else {
            $args['a'] = 'false'; // true для всех типов
            foreach (array(
                         'OK' => ELotType::OPEN_CONTEST,
                         'OA' => ELotType::OPEN_AUCTION,
                         'ZK' => ELotType::REQUEST_FOR_QUOTATION,
                         'ZH' => ELotType::OTHER, // Результаты рассмотрения и оценки котировочных заявок (гл.5 Федерального закона №94)
                         'EF' => ELotType::OPEN_ONLINE_AUCTION,
                         'PO' => ELotType::PRELIMINARY_SELECTION,
                         'SZ' => ELotType::OTHER, // Сообщение о заинтересованности в проведении открытого конкурса
                     ) as $k => $v) {
                if (isset($this->params->fields[ESearchFields::LOT_TYPE][$v])) {
                    $args['b'][] = $k;
                }
            }
        }

        /**
         * Этап размещения
         */
//        $args['c'] = array();
//        foreach (array(
//            PlatformSearchParams::STAGE_SUBMISSION => 'AP',
//            PlatformSearchParams::STAGE_COMMITTEE => 'CW',
//            PlatformSearchParams::STAGE_COMPLETED => 'FO',
//            PlatformSearchParams::STAGE_CANCELED => 'CO',
//        ) as $k => $v) {
//            if (in_array($k, $this->params->stage)) {
//                $args['c'][] = $v;
//            }
//        }

        $args['index'] = $this->curPage;
        $baseUrl = 'http://zakupki.gov.ru/pgz/public/action/search/extended/resultExport';

        $urlArgs = array();
        foreach ($args as $k => $v) {
            foreach (is_array($v) ? $v : array($v) as $vv) {
                $urlArgs[] = $k . '=' . urlencode($vv);
            }
        }

        $url = $baseUrl . '?' . implode('&', $urlArgs);
        $this->dataLogArgs = $args;
        $this->dataLogUrl = $url;
        $result = array_merge($result, $this->subSearch($url));

        if (!$result)
            return $result;
        if (!$this->buffer) {
            $this->buffer = $result;
        }
        return $this->buffer[$this->position];
    }

    private $csvContent = null;

    private function subSearch($url)
    {

        $result = array();

        if (is_null($this->csvContent)) {
            $answer = $this->params->platform->getHTTPContent($url);
            $answer = iconv('windows-1251', 'utf-8', $answer);
            $this->csvContent = explode("\n", $answer, 1000);
        }

        $this->lotCount = count($this->csvContent) - 2; // первая строка - названия столбцов, последняя - пустая
        $this->lotTotal = $this->lotCount;

        if (!$this->totalPages)
            $this->totalPages = 1;
        if ($this->totalPages == $this->curPage)
            $this->lastPage = true;

        for ($i = 1; $i < count($this->csvContent); $i++) {

            $row = explode(';', trim($this->csvContent[$i]));
            $row = array_map('trim', $row);

            if (count($row) < 14) continue;

//            $row = Array
//                (
//                    [0] => 'Реестровый номер заказа',
//                    [1] =>  'Способ размещения заказа',
//                    [2] =>  'Наименование заказа',
//                    [3] =>  'Номер лота',
//                    [4] =>  'Наименование лота',
//                    [5] =>  'Начальная (максимальная) цена контракта (лота)',
//                    [6] =>  'Код валюты',
//                    [7] =>  'Классификация по ОКДП',
//                    [8] =>  'Организация, размещающая заказ',
//                    [9] =>  'Дата публикации',
//                    [10] =>  'Дата последнего события',
//                    [11] =>  'Этап размещения заказа',
//                    [12] =>  'Особенности размещения заказа',
//                    [13] =>  'Дата начала подачи заявок',
//                    [14] =>  'Дата окончания подачи заявок',
//                );

            $data = new TenderImportData();
            $data->notificationPlatform = $data->fields[ELotFieldName::NOTIFICATION_PLATFORM] =
                $data->fields[ELotFieldName::ORIGIN_NOTIFICATION_PLATFORM] = ENotificationPlatformName::ZAKUPKI_GOV_RU;

            // Способ размещения аукциона
            switch ($row[1]) {
                case 'Запрос котировок':
                    $data->fields[ELotFieldName::LOT_TYPE] = ELotType::REQUEST_FOR_QUOTATION;
                    break;
                case 'Открытый конкурс':
                    $data->fields[ELotFieldName::LOT_TYPE] = ELotType::OPEN_CONTEST;
                    break;
                case 'Предварительный отбор':
                    $data->fields[ELotFieldName::LOT_TYPE] = ELotType::PRELIMINARY_SELECTION;
                    break;
                case 'Открытый аукцион в электронной форме':
                default:
                    $data->fields[ELotFieldName::LOT_TYPE] = ELotType::OPEN_ONLINE_AUCTION;
                    break;
            }

            // Номер заказа
            if (!empty($row[0])) {
                $data->fields[ELotFieldName::IMPORT_NUMBER] = str_replace("'", "", $row[0]);
            }

            // Наименование заказа
            if (!empty($row[2])) {
                $data->fields[ELotFieldName::TITLE] = $row[2];
            }

            // Номер лота:
            preg_match('/(\d+)/', $row[3], $_lot);
            if (!empty($_lot[1])) {
                $data->fields[ELotFieldName::LOT_ORDINAL] = $_lot[1];
            }

            // Наименование лота
            if (!empty($row[4])) {
                $data->fields[ELotFieldName::LOT_TITLE] = $row[4];
            }

            // Начальная (максимальная) цена контракта (лота)
            if (!empty($row[5])) {
                $data->fields[ELotFieldName::BUDGET_INITIAL] = Formatters::getPriceFormatter()->format(preg_replace("/[^\d,]/", "", $row[5]) . " " . $row[6]);
            }

            // Организация, размещающая заказ
            if (!empty($row[8])) {
                $data->fields[ELotFieldName::CUSTOMER] = $row[8];
            }

            // Дата публикации
            $d2 = new \DateTime($row[9]);
            if ($d2 instanceof \DateTime) {
                $data->fields[ELotFieldName::DATE_NOTIFICATION_PUBLIC] = $d2->format('Y-m-d G:i e');
            }

            // Дата начала подачи заявок
            $d2 = new \DateTime($row[13]);
            if ($d2 instanceof \DateTime) {
                $data->fields[ELotFieldName::DATE_SUBMISSION_BEGIN] = $d2->format('Y-m-d G:i e');
            }

            // Дата окончания подачи заявок
            $d2 = new \DateTime($row[14]);
            if ($d2 instanceof \DateTime) {
                $data->fields[ELotFieldName::DATE_SUBMISSION_END] = $d2->format('Y-m-d G:i e');
            }

            // Прочее
            $data->fields[ELotFieldName::AUCTION_WEBSITE] = 'http://zakupki.gov.ru';
            $data->fields[ELotFieldName::SYSTEM_UID] = $data->notificationPlatform . '-' . $data->fields[ELotFieldName::IMPORT_NUMBER];

            if (!empty($data->fields[ELotFieldName::LOT_ORDINAL])) {
                $data->fields[ELotFieldName::SYSTEM_UID] .= '_lot' . $data->fields[ELotFieldName::LOT_ORDINAL];
            }

            $result[] = clone $data;
        }

        $this->bufferSize = count($result);
        return $result;
    }

}
