<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Platforms;

use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\PlatformSearchParams;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\TenderImportData;

abstract class ASearchIterator implements \Iterator
{

    /**
     * Всего найденных страниц
     * @var int
     */
    public $totalPages;

    /**
     * Последняя страница
     */
    public $lastPage;

    /**
     * Номер текущей страницы
     * @var int
     */
    public $curPage;

    /**
     * Позиция текущего лота в буфере
     * @var int
     */
    protected $position;

    /**
     * Счетчик лотов на одной странице
     * @var int
     */
    public $lotCount = 0;

    /**
     * Всего найденных лотов
     * @var int
     */
    public $lotTotal = 0;

    public $lotCurrent = 0;

    /**
     * Параметры поиска
     * @var PlatformSearchParams
     */
    public $params;

    protected $currType;
    public $dataLogArgs = null;
    public $dataLogUrl = null;
    public $dataLog = null;

    /**
     * Лот
     * @var TenderImportData
     */
    public $lot = null;
    private $is_first = true;
    private $the_end = false;

    /**
     * Размер буфера
     * @var int
     */
    public $bufferSize = 0;

    /**
     * Буфер лотов (размер равен количеству лотов на 1 странице)
     * @var array of TenderImportData
     */
    public $buffer = array();

    function __construct(PlatformSearchParams $params, $currType = '', $totalPages = 0, $position = 0, $curPage = 1)
    {
        $this->lastPage = false;
        $this->params = $params;
        $this->totalPages = $totalPages;
        $this->position = $position;
        $this->curPage = $curPage;
        $this->currType = $currType;
        $this->dataLog = new TenderImportData();
    }

    abstract protected function getLot();

    public function current()
    {
        return $this->lot;
    }

    public function key()
    {
        return (int)$this->position;
    }

    public function next()
    {
//        if (!$this->valid())
        $this->is_first = false;
        if ($this->buffer) {
            if ($this->position >= $this->bufferSize) {
                if ($this->lastPage) {
                    $this->the_end = true;
                    return false;
                }
                $this->rewind();
                $this->curPage++;
                $this->lot = $this->getLot();
            } else {
                $this->lot = $this->buffer[$this->position];
            }
        } else {
            $this->lot = $this->getLot();
        }
        if (!$this->lot)
            $this->curPage++;
        else {
            $this->position++;
        }
        if ($this->valid()) {
            $this->lotCurrent++;
            return true;
        } else {
            return false;
        }
    }

    public function rewind()
    {
        $this->lotCount = 0;
        $this->lot = null;
        $this->position = 0;
        $this->bufferSize = 0;
        $this->buffer = array();
    }

    public function valid()
    {
        if ($this->is_first) {
            $this->next();
        }
        if ($this->the_end)
            return false;
        if ($this->lastPage && !$this->bufferSize)
            return false;
        if ($this->lotCurrent >= 100)
            return false;
        if ($this->totalPages && ($this->totalPages < $this->curPage))
            return false;
        if (!$this->lot && ($this->lotCount < 1))
            return false;
        return true;
    }

}
