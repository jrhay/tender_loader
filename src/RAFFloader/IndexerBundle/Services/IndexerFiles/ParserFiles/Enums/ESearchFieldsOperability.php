<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums;

/**
 * Список статусов, доступных для обозначения поля в классе платформ
 */
class ESearchFieldsOperability
{
    /**
     * Поле не работает
     */
    const ERROR = 'error';

    /**
     * Поле работает с особенностями
     */
    const WARNING = 'warning';

    /**
     * Поле работает без явных особенностей
     */
    const AVAILABLE = 'available';
}