<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums;

class ESearchFields
{
    /**
     * Ключевые слова
     */
    const KEYWORDS = 'keywords';

    /**
     * Интервал цен
     */
    const PRICE_INTERVAL = 'prices';

    /**
     * Интервал даты оубликования заявки
     */
    const DATE_PUBLICATION_INTERVAL = 'date_publication';

    /**
     * Заказчик
     */
    const CUSTOMER = 'customer';

    /**
     * ИНН заказчика
     */
    const CUSTOMER_INN = 'customerINN';

    /**
     * Тип заявки
     */
    const LOT_TYPE = 'lot_type';

    /**
     * Номер заявки
     */
    const LOT_NUMBER = 'lot_number';

    /**
     * Интервал даты вскрытия конвертов
     */
    const DATE_ENVELOPE_OPENING_INTERVAL = 'date_envelope_opening';

    /**
     * Интервал даты завершения торгов
     */
    const DATE_END_INTERVAL = 'date_end_interval';

    /**
     * Интервал даты окончания подачи заявок
     */
    const DATE_SUBMISSION_END_INTERVAL = 'date_submission_end_interval';
}