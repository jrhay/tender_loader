<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums;

class ELotType
{
    /**
     * Внутренний запрос от клиента
     */

    const CLIENT_REQUEST = 'client_request';

    /**
     * Открытый конкурс
     */
    const OPEN_CONTEST = 'open_contest';

    /**
     * Открытый аукцион
     */
    const OPEN_AUCTION = 'open_auction';

    /**
     * Открытый аукцион в электронной форме
     */
    const OPEN_ONLINE_AUCTION = 'ooa';

    /**
     * Запрос цен (пояснение ниже)
     */
    const REQUEST_FOR_QUOTATION = 'rfq';

    /**
     * Запрос котировок (пояснение ниже)
     */
    const REQUEST_FOR_QUOTATION_SIMPLE = 'rfq-simple';

    /**
     * ======== РАЗЛИЧИЕ МЕЖДУ rfq и rqf-simple ========
     *
     * From:     Василий Ляхов
     *
     * Принципиальное отличие в заказчике и количестве необходимых для участия документов.
     * Запрос котировок проходит на сайте zakupki.gov.ru и не требует особого напряжения для участия.
     * Запрос цен проводят коммерческие заказчики, и пакет документов для участия в нем достаточно большой.
     * И проводится он на сайтах Росатома, АКД и Фабрикант (из тех где мы зарегистрированы).
     */

    /**
     * Запрос предложений
     */
    const REQUEST_FOR_PROPOSAL = 'rfp';

    /**
     * Предварительный отбор
     */
    const PRELIMINARY_SELECTION = 'preliminary_selection';

    /**
     * Прочие
     */
    const OTHER = 'other';

}
