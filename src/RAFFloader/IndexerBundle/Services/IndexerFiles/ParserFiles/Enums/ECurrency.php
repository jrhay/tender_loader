<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Glizhinskiy Dmitry
 * Date: 25.03.13
 * Time: 14:01
 */

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums;

class ECurrency
{
    /**
     * Российский рубль
     */
    const RUB = 'RUB';

    /**
     * Евро
     */
    const EUR = 'EUR';

    /**
     * Американский доллар
     */
    const USD = 'USD';

    /**
     * Швейцарский франк
     */
    const CHF = 'CHF';
}
