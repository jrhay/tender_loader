<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums;

class ELotFieldName
{

    // ---------- Поля ---------- //

    /**
     * Название
     */
    const TITLE = 'title';

    /**
     * Название лота
     */
    const LOT_TITLE = 'lot_title';

    /**
     * Адрес публикации
     */
    const PUBLICATION_URI = 'publication_uri';

    /**
     * Адрес публикации (технический)
     */
    const IMPORT_URI = 'import_uri';

    /**
     * Оригинальный адрес публикации
     */
    const ORIGIN_URI = 'origin_uri';

    /**
     * Заказчик
     */
    const CUSTOMER = 'customer';

    /**
     * Начальный (максимальный) бюджет
     */
    const BUDGET_INITIAL = 'budget_initial';

    /**
     * Окончательный бюджет
     */
    const BUDGET_FINAL = 'budget_final';

    /**
     * Обеспечение заявки
     */
    const REQUEST_DEPOSIT = 'request_deposit';

    /**
     * Обеспечение контракта
     */
    const CONTRACT_DEPOSIT = 'contract_deposit';

    /**
     * Аванс
     */
    const ADVANCE_PAYMENT = 'advance_payment';

    /**
     * Веб-сайт аукциона
     */
    const AUCTION_WEBSITE = 'auction_website';

    /**
     * Номер лота
     */
    const LOT_ORDINAL = 'lot_ordinal';

    /**
     * Номер уведомления на данной площадке
     */
    const IMPORT_NUMBER = 'import_number';

    /**
     * Внешний (кросс-платформенный) номер уведомления или номер на оригинальной платформе
     */
    const ORIGIN_NUMBER = 'origin_number';

    /**
     * UID импортированной публикации
     */
    const IMPORT_UID = 'import_uid';

    /**
     * UID оригинальной публикации
     */
    const ORIGIN_UID = 'origin_uid';

    /**
     * UID системный [platform-number-lot]
     */
    const SYSTEM_UID = 'system_uid';

    /**
     * Код платформы
     */
    const NOTIFICATION_PLATFORM = 'notification_platform';

    /**
     * Код платформы, на которой находится оригинал извещения
     */
    const ORIGIN_NOTIFICATION_PLATFORM = 'origin_notification_platform';

    /**
     * Тип торгов
     */
    const LOT_TYPE = 'lot_type';


    // ---------- Даты ---------- //

    /**
     * Начало подачи заявок
     */
    const DATE_SUBMISSION_BEGIN = 'date_submission_begin';

    /**
     * Окончание подачи заявок
     */
    const DATE_SUBMISSION_END = 'date_submission_end';

    /**
     * Вскрытие конвертов
     */
    const DATE_ENVELOPE_OPENING = 'date_envelope_opening';

    /**
     * Рассмотрение заявок
     */
    const DATE_REQUEST_STUDY = 'date_request_study';

    /**
     * Подведение итогов
     */
    const DATE_DEBRIEFING = 'date_debriefing';

    /**
     * Дата окончания срока рассмотрения первых частей заявок
     */
    const DATE_FIRST_PART_END = 'date_1st_part_end';

    /**
     * Дата проведения открытого аукциона в электронной форме
     */
    const DATE_ONLINE_AUCTION = 'date_online_auction';

    /**
     * Дата и время проведения предварительного отбора
     */
    const DATE_PRELIMINARY_SELECTION = 'date_preliminary_selection';

    /**
     * Дата и время размещения лота на площадке
     */
    const DATE_NOTIFICATION_PUBLIC = 'date_notification_public';

}
