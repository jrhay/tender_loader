<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums;

class ENotificationPlatformName
{
    /**
     * zakupki.gov.ru
     */
    const ZAKUPKI_GOV_RU = 'ZakupkiGovRu';
    const ZAKUPKI_ROSATOM_RU = 'ZakupkiRosatomRu';
    const SBERBANK_AST_RU = 'SberbankAstRu';
    const A_K_D_RU = 'AKDRu';
    const ETP_ROSELTORG_RU = 'EtpRoseltorgRu';
    const ETP_ZAKAZRF_RU = 'EtpZakazrfRu';
    const FABRIKANT_RU = 'FabrikantRu';
    const RTS_TENDER_RU = 'RtsTenderRu';
    const ETP_MICEX_RU = 'EtpMicexRu';
    const OTC_TENDER_RU = 'OtcTenderRu';
    const B2B_CENTER_RU = 'B2bCenterRu';
    const ETPRF_RU = 'EtprfRu';


}
