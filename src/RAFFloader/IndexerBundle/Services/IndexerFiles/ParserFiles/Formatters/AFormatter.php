<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Glizhinskiy Dmitry
 * Date: 18.03.13
 * Time: 17:40
 */

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Formatters;

abstract class AFormatter
{
    abstract public function format($value);

    /**
     * Данный магический метод позволяет обращаться к классу как к функции
     * @param $value
     * @return mixed
     */
    public function __invoke($value)
    {
        return $this->format($value);
    }

}
