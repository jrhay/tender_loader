<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Glizhinskiy Dmitry
 * Date: 18.03.13
 * Time: 17:20
 */

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Formatters;

/**
 * Позволяет инстанциировать нужные классы-форматтеры
 */
class Formatters
{
    /**
     * Возвращает инстанс форматтера даты
     * @param string $inputFormat Шаблон входных данных
     * @param null $outputFormat Шаблон выходных данных (По-умолчанию "Y-m-d H:i e")
     * @return DateFormatter
     */
    static function getDateFormatter($inputFormat = 'd.m.Y G:i e', $outputFormat = "Y-m-d H:i e")
    {
        return new DateFormatter($inputFormat, $outputFormat);
    }

    /**
     * Возвращает инстанс форматтеры цены
     * @return PriceFormatter
     */
    static function getPriceFormatter($currencies = array())
    {
        return new PriceFormatter($currencies);
    }

    /**
     * Возвращает инстанс форматтера, аналог substr
     * @param int $start Начальная позиция
     * @param int|null $length Длина
     * @return CropFormatter
     */
    static function getCropFormatter($start = 0, $length = null)
    {
        return new CropFormatter($start, $length);
    }

    /**
     * Возвращает форматтер-цепочку, состоящую из других форматтеров
     * @param array $formatters Массив форматтеров
     * @return FormatChain
     */
    static function createFormatChain($formatters)
    {
        return new FormatChain($formatters);
    }

    /**
     * Возвращает инстанс форматера, аналог preg_match
     * @param $match_pattern Регулярное выражение для поиска
     * @param $index Номер вхождения
     * @param bool $return_value Возвращать начальное значение в случае неудачи
     * @return MatchFormatter
     */
    static function getMatchFormatter($match_pattern, $index = 1, $return_value = false)
    {
        return new MatchFormatter($match_pattern, $index, $return_value);
    }

    static function getFloatFormatter()
    {
        return new FloatFormatter();
    }

    /**
     * Возвращает форматтер-колбэк, аналог call_user_func
     * @param array|string $callback Функция-колбэк. Смотри call_user_func()
     * @return FormatCallback
     */
    static function getFormatCallback($callback)
    {
        return new FormatCallback($callback);
    }
}
