<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Glizhinskiy Dmitry
 * Date: 18.03.13
 * Time: 17:57
 */

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Formatters;

use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ECurrency;

/**
 * Форматирует дату
 */
class PriceFormatter extends AFormatter
{
    /**
     * Словарь обозначений валюты
     * @var Helpers\ArrayHelper
     */
    public $currencies;

    public function __construct($currencies = array())
    {
        if (!is_array($currencies) or !count($currencies)) {
            /* Оставлено для совместимости */
            $currencies = array(
                'р.' => ECurrency::RUB,
                '(Российский рубль)' => ECurrency::RUB,
                '(Российский рубль )' => ECurrency::RUB,
                'руб.' => ECurrency::RUB,
                'Евро' => ECurrency::EUR,
                'Доллар США' => ECurrency::USD,
                'Швейцарский франк' => ECurrency::CHF,
            );
        }
        $this->currencies = new Helpers\ArrayHelper($currencies);
    }

    public function format($value)
    {
        $value = str_replace("\xC2\xA0", " ", $value);

        if (!preg_match("/([\d ]+([.,]\d+)?)/", $value, $arPrice)) {
            $_price = '';
        } else {
            $_price = $arPrice[1];
        }

        $price = Formatters::getFloatFormatter()->format($_price);

        $curr = trim(str_replace($_price, "", $value));

        $_curr = $this->currencies->getField($curr);
        $curr = !is_null($_curr) ? $_curr : $curr;

        $price = number_format($price, 2, '.', '') . " " . $curr;

        return trim($price);
    }
}
