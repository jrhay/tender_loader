<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Glizhinskiy Dmitry
 * Date: 18.03.13
 * Time: 19:02
 */

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Formatters;

/**
 * Формирует цепочку из других форматтеров
 */
class FormatChain extends AFormatter
{
    public $chain = array();

    public function __construct($chain = array())
    {
        $this->chain = $chain;
    }

    public function format($value)
    {
        /**
         * @var $formatter AFormatter
         */
        foreach ($this->chain as $formatter) {
            $value = $formatter->format($value);
            if (is_null($value)) {
                return $value;
            }
        }

        return $value;
    }

}
