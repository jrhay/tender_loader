<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Glizhinskiy Dmitry
 * Date: 04.04.13
 * Time: 21:07
 */

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Formatters;

class FormatCallback extends AFormatter
{
    private $function;

    public function __construct($function)
    {
        $this->function = $function;
    }

    public function format($value)
    {
        if (is_string($this->function))
            return call_user_func($this->function, $value);
        elseif (is_callable($this->function, true)) {
            if (is_array($this->function)) {
                // an array: 0 - object, 1 - method name
                list($object, $method) = $this->function;
                if (is_string($object)) // static method call
                    return call_user_func($this->function, $value);
                elseif (method_exists($object, $method))
                    return $object->$method($value);
            } else // PHP 5.3: anonymous function
                return call_user_func($this->function, $value);
        }
        return $value;
    }

}
