<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Glizhinskiy Dmitry
 * Date: 25.03.13
 * Time: 14:16
 */

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Formatters;

class FloatFormatter extends AFormatter
{
    public function format($value)
    {
        $value = preg_replace("/[^\d.,]/", "", $value);
        if (preg_match("/(\d*([.,]\d+)?)/", $value, $match)) {
            $value = $match[1];
        }
        $value = str_replace(",", ".", $value);
        $value = floatval($value);
        return $value;
    }
}
