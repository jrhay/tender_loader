<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Glizhinskiy Dmitry
 * Date: 20.03.13
 * Time: 13:38
 */

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Formatters;

/**
 * Пытается найти
 */
class MatchFormatter extends AFormatter
{
    /**
     * Регулярное выражение
     * @var string
     */
    private $match_pattern = '';

    /**
     * Номер совпадения
     * @var int
     */
    private $index;

    /**
     * Возвращать начальное значение в случае неудачи
     * @var bool
     */
    private $return_value = false;

    public function __construct($match_pattern, $index = 1, $return_value = false)
    {
        $this->match_pattern = $match_pattern;
        $this->index = $index;
        $this->return_value = $return_value;
    }

    public function format($value)
    {
        $matches = array();
        if (preg_match($this->match_pattern, $value, $matches) and isset($matches[$this->index])) {
            return $matches[$this->index];
        } else {
            if ($this->return_value) {
                return $value;
            } else {
                return null;
            }
        }
    }

}
