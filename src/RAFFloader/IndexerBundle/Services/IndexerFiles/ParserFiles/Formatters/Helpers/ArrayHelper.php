<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Glizhinskiy Dmitry
 * Date: 25.03.13
 * Time: 14:38
 */

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Formatters\Helpers;

/**
 * Вспомогательный класс для работы с массивами
 */
class ArrayHelper
{
    private $array = array();

    public function __construct($array = array())
    {
        $this->array = $array;
    }

    /**
     * Возвращает элемент массива по его ключу. Null если элемент не найден
     * @param string $key
     * @return mixed
     */
    public function getField($key)
    {
        if (isset($this->array[$key])) {
            return $this->array[$key];
        } else {
            return null;
        }
    }

}
