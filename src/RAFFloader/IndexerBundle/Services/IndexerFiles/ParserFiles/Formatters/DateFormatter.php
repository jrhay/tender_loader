<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Glizhinskiy Dmitry
 * Date: 18.03.13
 * Time: 17:30
 */

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Formatters;

/**
 * Форматирует дату
 */
class DateFormatter extends AFormatter
{
    /**
     * Формат входных данных
     * @link http://www.php.net/manual/ru/function.date.php
     * @var string
     */
    public $inputFormat;

    /**
     * Формат выходных данных
     * @link http://www.php.net/manual/ru/function.date.php
     * @var string
     */
    public $outputFormat;

    public function  __construct($inputFormat = 'd.m.Y G:i e', $outputFormat = 'Y-m-d H:i e')
    {
        $this->inputFormat = $inputFormat;
        $this->outputFormat = $outputFormat;
    }

    public function format($value)
    {

        $value = str_replace("\xC2\xA0", " ", $value);

        $value = trim($value);

        $date = \DateTime::createFromFormat($this->inputFormat, $value);
        if ($date instanceof \DateTime) {
            return $date->format($this->outputFormat);
        } else {
            return $value;
        }

    }
}
