<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Glizhinskiy Dmitry
 * Date: 18.03.13
 * Time: 18:46
 */

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Formatters;

class CropFormatter
{
    /**
     * начальная позиция
     * @var int
     */
    public $start;

    /**
     * Длина подстроки
     * @var int|null
     */
    public $length;

    public function __construct($start = 0, $length = null)
    {
        $this->start = $start;
        $this->length = $length;
    }

    public function format($value)
    {
        return substr($value, $this->start, $this->length);
    }

}
