<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business;

class ImportPosition
{
    /**
     * Название товарной позиции
     * @var string
     */
    public $title = '';

    /**
     * Количество единиц
     * @var int
     */
    public $count = 0;

}
