<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business;

use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ESearchFields;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ELotType;

class SearchFields
{

    /**
     * Массив описывающий поля поиска
     * @var array
     */
    private $fields = array(
        ESearchFields::KEYWORDS => array(
            'title' => 'Ключевые слова',
            'type' => array(
                array('title' => '', 'type' => 'text'),
                array('title' => 'Учитывать морфологию', 'type' => 'checkbox'),
            ),
            'description' => 'Ключевые слова для поиска по площадкам. "Учитывать морфологию" доступно только для Госзакупок',
        ),
        ESearchFields::PRICE_INTERVAL => array(
            'title' => 'Цена',
            'type' => array(
                array('title' => 'От', 'type' => 'text'),
                array('title' => 'До', 'type' => 'text'),
            ),
            'description' => 'Задается обычным числом. По-умолчанию под валютой подразумевается Российский рубль. Не требует обязательного заполнения обоих полей.',
        ),
        ESearchFields::DATE_PUBLICATION_INTERVAL => array(
            'title' => 'Дата публикации',
            'type' => array(
                array('title' => 'С', 'type' => 'date'),
                array('title' => 'По', 'type' => 'date'),
            ),
            'description' => 'Поиск по дате публикации извещения. Задается в формате "dd.mm.yyyy". Не требует обязательного заполнения обоих полей.'
        ),
        ESearchFields::CUSTOMER => array(
            'title' => 'Заказчик',
            'type' => array(
                array('title' => '', 'type' => 'text'),
            ),
        ),
        ESearchFields::CUSTOMER_INN => array(
            'title' => 'ИНН заказчика',
            'type' => array(
                array('title' => '', 'type' => 'text'),
            ),
        ),
        ESearchFields::LOT_TYPE => array(
            'title' => 'Тип заявки',
            'type' => array(
                ELotType::OPEN_ONLINE_AUCTION => array('title' => 'Открытый аукцион в электронной форме', 'type' => 'checkbox'),
                ELotType::REQUEST_FOR_QUOTATION => array('title' => 'Запрос котировок', 'type' => 'checkbox'),
                ELotType::OPEN_CONTEST => array('title' => 'Открытый конкурс', 'type' => 'checkbox'),
                ELotType::OPEN_AUCTION => array('title' => 'Открытый аукцион', 'type' => 'checkbox'),
                ELotType::REQUEST_FOR_PROPOSAL => array('title' => 'Запрос предложений', 'type' => 'checkbox'),
                ELotType::OTHER => array('title' => 'Другой', 'type' => 'checkbox'),

            ),
            'description' => 'Если ничего не выбрано, то подразумевается поиск по всем типам. Под обозначение "Другой" попадают типы, отсутствующие в данном списке.',
        ),
        ESearchFields::LOT_NUMBER => array(
            'title' => 'Номер извещения',
            'type' => array(
                array('title' => '', 'type' => 'text'),
            ),
        ),
        ESearchFields::DATE_ENVELOPE_OPENING_INTERVAL => array(
            'title' => 'Дата вскрытия конвертов',
            'type' => array(
                array('title' => 'С', 'type' => 'date'),
                array('title' => 'По', 'type' => 'date'),
            ),
            'description' => 'Поиск по дате вскрытия конвертов. Задается в формате "dd.mm.yyyy". Не требует обязательного заполнения обоих полей.'
        ),
        ESearchFields::DATE_END_INTERVAL => array(
            'title' => 'Дата завершения торгов',
            'type' => array(
                array('title' => 'С', 'type' => 'date'),
                array('title' => 'По', 'type' => 'date'),
            ),
            'description' => 'Поиск по дате завершения торгов. Задается в формате "dd.mm.yyyy". Не требует обязательного заполнения обоих полей.'
        ),
        ESearchFields::DATE_SUBMISSION_END_INTERVAL => array(
            'title' => 'Дата завершения подачи заявок',
            'type' => array(
                array('title' => 'С', 'type' => 'date'),
                array('title' => 'По', 'type' => 'date'),
            ),
            'description' => 'Поиск по дате завершения подачи заявок. Задается в формате "dd.mm.yyyy". Не требует обязательного заполнения обоих полей.'
        ),
    );

    /**
     * Возвращает список полей для поиска
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Возвращает описание поля
     * @param $fieldName Название поля (из ESearchFields)
     * @return array Массив с описанием поля array('title', array('type', ...))
     */
    public function getField($fieldName)
    {
        if (isset($this->fields[$fieldName])) {
            return $this->fields[$fieldName];
        } else {
            return array();
        }
    }
}