<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Glizhinskiy Dmitry
 * Date: 19.03.13
 * Time: 18:03
 */

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business;

class ContextLogger
{
    const EVENT_TPLATFORMS_SUCCESS = 'event_tplatforms_success';
    const EVENT_TPLATFORMS_ERROR = 'event_tplatforms_error';

    private $context = array();

    /**
     * Менеджер платформ
     * @var PlatformManager
     */
    private $manager;

    public function __construct(PlatformManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Сохраняет переданное значение вместе с доп параметрами
     * @param $value
     * @param $name Имя переменной
     */
    public function logVar($value, $name = null)
    {
        $backtrace = debug_backtrace();

        $context = array(
            'name' => $name,
            'value' => $value,
            'time' => microtime(true),
        );

        if (isset($backtrace[1])) {
            $context = $context + array(
                'file' => $backtrace[1]['file'],
                'line' => $backtrace[1]['line'],
                'class' => $backtrace[1]['class'],
                'function' => $backtrace[1]['function'],
            );
        }

        $this->context[] = $context;
    }

    /**
     * Вызывается при успешном завершении некоего действия
     */
    public function close()
    {
        $this->manager->dispatchEvent(self::EVENT_TPLATFORMS_SUCCESS, $this->context);
        $this->context = array();
    }

    /**
     * Вызывается при ошибке
     */
    public function error()
    {
        $this->manager->dispatchEvent(self::EVENT_TPLATFORMS_ERROR, $this->context);
    }
}
