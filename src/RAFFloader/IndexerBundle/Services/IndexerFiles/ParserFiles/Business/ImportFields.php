<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business;

use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ELotFieldName;

class ImportFields
{

    /**
     * Массив описывающий поля тендера
     * @var array
     */
    private $fields = array(

        // ---------- Поля ---------- //
        ELotFieldName::TITLE => array(
            'title' => 'Название',
            'type' => array('text'),
        ),
        ELotFieldName::LOT_TITLE => array(
            'title' => 'Название лота',
            'type' => array('text'),
        ),
        ELotFieldName::PUBLICATION_URI => array(
            'title' => 'Адрес публикации',
            'type' => array('url'),
        ),
        ELotFieldName::IMPORT_URI => array(
            'title' => 'Адрес публикации (технический)',
            'type' => array('url'),
        ),
        ELotFieldName::ORIGIN_URI => array(
            'title' => 'Оригинальный адрес публикации',
            'type' => array('url'),
        ),
        ELotFieldName::CUSTOMER => array(
            'title' => 'Заказчик',
            'type' => array('text'),
        ),
        ELotFieldName::BUDGET_INITIAL => array(
            'title' => 'Начальный (максимальный) бюджет',
            'type' => array('price'),
        ),
        ELotFieldName::BUDGET_FINAL => array(
            'title' => 'Окончательный бюджет',
            'type' => array('price'),
        ),
        ELotFieldName::REQUEST_DEPOSIT => array(
            'title' => 'Обеспечение заявки',
            'type' => array('price'),
        ),
        ELotFieldName::CONTRACT_DEPOSIT => array(
            'title' => 'Обеспечение контракта',
            'type' => array('price'),
        ),
        ELotFieldName::ADVANCE_PAYMENT => array(
            'title' => 'Аванс',
            'type' => array('price'),
        ),
        ELotFieldName::AUCTION_WEBSITE => array(
            'title' => 'Веб-сайт аукциона',
            'type' => array('url'),
        ),
        ELotFieldName::LOT_ORDINAL => array(
            'title' => 'Номер лота',
            'type' => array('number'),
        ),
        ELotFieldName::IMPORT_NUMBER => array(
            'title' => 'Номер уведомления на данной площадке',
            'type' => array('text'),
        ),
        ELotFieldName::ORIGIN_NUMBER => array(
            'title' => 'Внешний (кросс-платформенный) номер уведомления или номер на оригинальной платформе',
            'type' => array('text'),
        ),
        ELotFieldName::IMPORT_UID => array(
            'title' => 'UID импортированной публикации',
            'type' => array('text'),
        ),
        ELotFieldName::ORIGIN_UID => array(
            'title' => 'UID оригинальной публикации',
            'type' => array('text'),
        ),
        ELotFieldName::SYSTEM_UID => array(
            'title' => 'UID системный [platform-number-lot]',
            'type' => array('text'),
        ),
        ELotFieldName::NOTIFICATION_PLATFORM => array(
            'title' => 'Код платформы',
            'type' => array('text'),
        ),
        ELotFieldName::ORIGIN_NOTIFICATION_PLATFORM => array(
            'title' => 'Код платформы, на которой расположен оригинал извещения',
            'type' => array('text'),
        ),
        ELotFieldName::LOT_TYPE => array(
            'title' => 'Тип торгов',
            'type' => array('text'),
        ),


        // ---------- Даты ---------- //

        ELotFieldName::DATE_SUBMISSION_BEGIN => array(
            'title' => 'Начало подачи заявок',
            'type' => array('date'),
        ),
        ELotFieldName::DATE_SUBMISSION_END => array(
            'title' => 'Окончание подачи заявок',
            'type' => array('date'),
        ),
        ELotFieldName::DATE_ENVELOPE_OPENING => array(
            'title' => 'Вскрытие конвертов',
            'type' => array('date'),
        ),
        ELotFieldName::DATE_REQUEST_STUDY => array(
            'title' => 'Рассмотрение заявок',
            'type' => array('date'),
        ),
        ELotFieldName::DATE_DEBRIEFING => array(
            'title' => 'Подведение итогов',
            'type' => array('date'),
        ),
        ELotFieldName::DATE_FIRST_PART_END => array(
            'title' => 'Дата окончания срока рассмотрения первых частей заявок',
            'type' => array('date'),
        ),
        ELotFieldName::DATE_ONLINE_AUCTION => array(
            'title' => 'Дата проведения открытого аукциона в электронной форме',
            'type' => array('date'),
        ),
        ELotFieldName::DATE_PRELIMINARY_SELECTION => array(
            'title' => 'Дата и время проведения предварительного отбора',
            'type' => array('date'),
        ),
        ELotFieldName::DATE_NOTIFICATION_PUBLIC => array(
            'title' => 'Дата и время размещения лота на площадке',
            'type' => array('date'),
        ),
    );

    /**
     * Возвращает список возможных полей тендера
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Возвращает описание поля
     * @param $fieldName Название поля (из ELotFieldName)
     * @param array $default Возвращаемое значение в случае неудачи
     * @return array Массив с описанием поля array('title' => 'title', 'type' => array('type', ...) )
     */
    public function getField($fieldName, $default = array())
    {
        if (isset($this->fields[$fieldName])) {
            return $this->fields[$fieldName];
        } else {
            return $default;
        }
    }
}
