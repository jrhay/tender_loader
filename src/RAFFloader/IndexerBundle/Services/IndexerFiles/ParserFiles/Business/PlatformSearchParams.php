<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business;

use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Platforms\INotificationPlatform;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Platforms\IGetPublicationUrl;

class PlatformSearchParams
{

    /**
     * Ссылка на соответствующую платформу
     * @var INotificationPlatform|IGetPublicationUrl
     */
    public $platform;

    /**
     * Массив учавствующих в поиске полей и их значений
     * @var array
     */
    public $fields = array();

    /**
     * Возвращает значение нужного поля поиска
     * @param $fieldName Название поля
     * @param int $index Индекс значения
     * @return string|null Значение запрошенного поля, null в случае отсутствия поля
     */
    public function getFieldValue($fieldName, $index = 0)
    {
        if (isset($this->fields[$fieldName][$index])) {
            return $this->fields[$fieldName][$index];
        } else {
            return null;
        }
    }
}