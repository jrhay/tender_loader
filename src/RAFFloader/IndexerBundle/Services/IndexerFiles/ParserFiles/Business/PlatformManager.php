<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business;

use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ENotificationPlatformName;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Platforms as Platforms;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Platforms\INotificationPlatform;

class PlatformManager
{
    //TODO: придумать как парсить коллекцию, не создавая специальных полей классов PlatformManager и APlatform
    /**
     * Объект с описанием полей поиска
     * @var SearchFields
     */
    public $searchFields;

    /**
     * Объект с описанием полей импорта
     * @var ImportFields
     */
    public $importFields;
    private $collection;

    private $platforms = array();

    function __construct()
    {

        $this->platforms = array(
//            ENotificationPlatformName::FABRIKANT_RU => new Platforms\PlatformFabrikantRu($this),
            ENotificationPlatformName::ZAKUPKI_GOV_RU => new Platforms\PlatformZakupkiGovRu($this, $this->collection),
//            ENotificationPlatformName::ETPRF_RU => new Platforms\PlatformEtprfRu($this),
//            ENotificationPlatformName::A_K_D_RU => new Platforms\PlatformAKDRu($this),
//            ENotificationPlatformName::B2B_CENTER_RU => new Platforms\PlatformB2bCenterRu($this),
//            ENotificationPlatformName::ZAKUPKI_ROSATOM_RU => new Platforms\PlatformZakupkiRosatomRu($this),
//            ENotificationPlatformName::SBERBANK_AST_RU => new Platforms\PlatformSberbankAstRu($this),
//            ENotificationPlatformName::RTS_TENDER_RU => new Platforms\PlatformRtsTenderRu($this),
//            ENotificationPlatformName::ETP_ROSELTORG_RU => new Platforms\PlatformEtpRoseltorgRu($this),
//            ENotificationPlatformName::ETP_ZAKAZRF_RU => new Platforms\PlatformEtpZakazrfRu($this),
//            ENotificationPlatformName::ETP_MICEX_RU => new Platforms\PlatformEtpMicexRu($this),
//            ENotificationPlatformName::OTC_TENDER_RU => new Platforms\PlatformOtcTenderRu($this),
        );

        $this->searchFields = new SearchFields();
        $this->importFields = new ImportFields();
    }

    /**
     * Возвращает инстанциированный объект платформы.
     * @param string $name Название платформы. Берется из ENotificationPlatformName
     * @return INotificationPlatform
     */
    public function getPlatform($name)
    {
        if (!isset($this->platforms[$name]) or !($this->platforms[$name] instanceof INotificationPlatform)) {
            return null;
        }

        return $this->platforms[$name];
    }

    public function createCtx()
    {
        return new ContextLogger($this);
    }

    /**
     * @var array Список обработчиков событий
     */
    private $eventListeners = array();

    /**
     * Добавляет обработчик события
     * @param $event Название события
     * @param $listener Callback
     */
    public function addListener($event, $listener)
    {
        $this->eventListeners[$event][] = $listener;
    }

    /**
     * @param $event Название события
     * @param $context Данные для логирования
     */
    public function dispatchEvent($event, $context = null)
    {
        if (isset($this->eventListeners[$event])) {
            foreach ($this->eventListeners[$event] as $handler) {
                if (is_string($handler)) {
                    call_user_func($handler, $context);
                } elseif (is_callable($handler, true)) {
                    if (is_array($handler)) {
                        // an array: 0 - object, 1 - method name
                        list($object, $method) = $handler;
                        if (is_string($object)) // static method call
                            call_user_func($handler, $context);
                        elseif (method_exists($object, $method))
                            $object->$method($context);
                    } else // PHP 5.3: anonymous function
                        call_user_func($handler, $context);
                }
            }
        }
    }
}
