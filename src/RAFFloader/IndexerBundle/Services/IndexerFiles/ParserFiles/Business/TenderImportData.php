<?php

namespace RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business;

class TenderImportData
{

    /**
     * Тип тендера. Берется из ELotType
     * @var string
     */
    public $type;

    /**
     * Код используемой платформы
     * @var string
     */
    public $notificationPlatform;

    /**
     * Список импортируемых распарсенных полей
     * @var array
     */
    public $fields = array();

    /**
     * Список товарных позиций (ImportPosition) в тендере
     * @var array()
     */
    public $positions = array();


    /**
     * Возвращает значение нужного поля импорта
     * @param $fieldName Название поля
     * @param $default Вернет это значение, если поле не найдено
     * @return string|null Значение запрошенного поля, null в случае отсутствия поля
     */
    public function getFieldValue($fieldName, $default = null)
    {
        if (isset($this->fields[$fieldName])) {
            return $this->fields[$fieldName];
        } else {
            return $default;
        }
    }
}
