<?php

function __autoload($class_name)
{
    print "\nПрисоединен autoload из проекта парсер текущая дериктория : ".__DIR__;
    $paths = array(

        __DIR__ . '/',
        __DIR__ . '/ParserFile/',
        __DIR__ . '/Interfaces/',
        __DIR__ . '/ParserFiles/Business/',
        __DIR__ . '/ParserFiles/Enums/',
        __DIR__ . '/ParserFiles/Formatters/',
        __DIR__ . '/ParserFiles/Platforms/',

    );

    foreach ($paths as $path) {
        if (file_exists($path . $class_name . '.php')) {
            require_once($path . $class_name . '.php');
            return;
        }
    }

    $path = explode('\\', $class_name);
    if ($path[0] == 'Parser') {
        unset($path[0]);
        $path = __DIR__ . '/' . implode('/', $path) . '.php';
        if (file_exists($path)) {
            require_once($path);
            return;
        }
    }
    if ($path[0] == 'Parser' && is_null($path[1])) {
        unset($path[0]);
        $path = __DIR__ . '/' . implode('/', $path) . '.php';
        if (file_exists($path)) {
            require_once($path);
            return;
        }
    }
}

spl_autoload_register('__autoload');

//require_once __DIR__ . '/phpunit.phar';
