<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 10.07.13
 * Time: 13:09
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\IndexerBundle\Services\IndexerFiles;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Platforms\PlatformZakupkiGovRu;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\TenderImportData;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Business\PlatformSearchParams;
use RAFFloader\IndexerBundle\Services\IndexerFiles\ParserFiles\Enums\ELotFieldName;

use RAFFloader\IndexerBundle\Services\IndexerFiles\Interfaces\IParser;

//include_once(__DIR__ . '/autoload.php');

class Parser implements IParser
{

    /** @return TenderImportData */
    //Параметр $collection здесь остался от основного проекта, отдельно этот скрипт можно использовать дав ему адрес публикации


    public function parseDocs($dir, $uid)
    {

        $DocsInfoArray[$uid] = $this->parseDoc($dir);

        return $DocsInfoArray;
    }

    private function parseDoc($fileName, $DocsInfoArray = array()){
        if(!file_exists($fileName)){
            print "\nERROR: parsDir: dir or file not exists";
            return false;
        }
        if ( is_dir($fileName)){
            foreach (scandir($fileName) as $element){
                if (in_array($element, array('.','..','Main'))){
                    continue;
                } else {
                    $resAr = $this->parseDoc($fileName.'/'.$element, $DocsInfoArray);
                    $DocsInfoArray = array_merge($DocsInfoArray, $resAr);
                }
            }
        } elseif (in_array(substr($fileName, -4), array('.zip','.rar'))) {
            $path = substr($fileName, 0, -4);
            $command = '7z x \'' . $fileName . '\' -y -oc:\'' . $path . '\'';
            exec($command);

            $result = $this->parseDoc($path, $DocsInfoArray);
            $DocsInfoArray = array_merge($DocsInfoArray, $result);
        } elseif ($this->isImage($fileName)) {
            $extension = substr($fileName, -4);
            $old_name = substr($fileName, 0, -4);
            $path = $old_name . '_parsed';
            $path = $this->isValidName($path, true);
            $command = 'tesseract \'' . $fileName . '\' \'' . $path . '\' -l rus+eng ' . "\n";
            exec($command);
            unlink($fileName);
            echo $command.' ...done';
            $path .= '.txt';
            $result = $this->parseDoc($path, $DocsInfoArray);
            $DocsInfoArray = array_merge($DocsInfoArray, $result);
        } else {
            $newFileName = $this->isValidName($fileName, true);
            if ($fileName !== $newFileName) {
                rename($fileName, $newFileName);
            }
            $nameAr = explode('/', $fileName);
            $name = end($nameAr);
            if (empty($DocsInfoArray[$name])) {
                $DocsInfoArray[$name] = $newFileName;
            }
        }
        return $DocsInfoArray;
    }


    function isValidName($str, $make = null){
        if(!$make){
            return true;
        }
        return $str;
    }
    function doisValidName($str, $mkValidName = null)
    {
        $fullnameAr = explode('/', $str);
        $name = array_pop($fullnameAr);

        if (preg_match('/[^!?@#$%&. A-ZА-ЯЁ0-9]+/ui', $name)) {
            if ($mkValidName !== null) {
                $name = preg_replace('/[^!?@#$%&. A-ZА-ЯЁ0-9]+/ui', '?', $name);
                $nameAr = explode('.', $name);
                if (count($nameAr) >= 2) {
                    $expansion = array_pop($nameAr);
                    array_push($nameAr, uniqid());
                    array_push($nameAr, $expansion);
                    $name = implode('.', $nameAr);
                } else {
                    $name .= '_uniq(' . uniqid() . ')_.xlsx';
                }
                $name = trim($name, ". ");
                $name = urldecode($name);
                $name = str_replace(' ', '_', $name);
            } else {
                return false;
            }
        } else {
            if ($mkValidName !== null) {
            } else {
                return true;
            }
        }
        array_push($fullnameAr, $name);
        $resultName = implode('/', $fullnameAr);
        return $resultName;
    }

    /** Function is_image returns type if file is image and FALSE otherwise*/
    private function isImage($fullImageName)
    {
        if (!function_exists('exif_imagetype')) {
            function exif_imagetype($fullImageName)
            {
                if ((list($width, $height, $type, $attr) = getimagesize($fullImageName)) !== false) {
                    return $type;
                }
                return false;
            }
        }
        return exif_imagetype($fullImageName);
    }

    /**
     * @param $raw string
     * @return TenderImportData
     */
    public function parseMainRaw($raw, $uid)
    {
        if (!$raw or !$uid) {
            return false;
        }

        $platformName = ParserFiles\Enums\ENotificationPlatformName::ZAKUPKI_GOV_RU;

        //Параметры импорта
        $data = new ParserFiles\Business\TenderImportData();
        $data->fields[ELotFieldName::IMPORT_UID] = $uid;
        $platformManager = new ParserFiles\Business\PlatformManager();

        $platform = $platformManager->getPlatform($platformName);
        $import_data = $platform->import(clone $data, $raw);

        return $import_data;
    }
}
