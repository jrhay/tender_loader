<?php

namespace RAFFloader\IndexerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('IndexerBundle:Default:index.html.twig', array('name' => $name));
    }
}
