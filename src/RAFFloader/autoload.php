<?php

function __autoload($class_name)
{

    $paths = array(

        __DIR__ . '/',
        __DIR__ . '/../',
        __DIR__ . '/../',

    );

    foreach ($paths as $path) {
        if (file_exists($path . $class_name . '.php')) {
            require_once($path . $class_name . '.php');
            return;
        }
    }

    $path = explode('\\', $class_name);
    if ($path[0] == 'RAFFloader') {
        unset($path[0]);
        $path = __DIR__ . '/' . implode('/', $path) . '.php';
        if (file_exists($path)) {
            require_once($path);
            return;
        }
    }
    if ($path[0] == 'Symfony') {
        $path = __DIR__ . '/../../vendor/symfony/symfony/src/' . implode('/', $path) . '.php';
        if (file_exists($path)) {
            require_once($path);
            return;
        }
    }
}

spl_autoload_register('__autoload');

require_once __DIR__ . '/phpunit.phar';
