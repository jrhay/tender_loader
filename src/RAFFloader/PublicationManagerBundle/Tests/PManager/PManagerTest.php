<?php

use RAFFloader\PublicationManagerBundle\services\PManager;
use RAFFloader\ZakupkiGovRuBundle\services\Downloader;
use RAFFloader\ResourceManagerBundle\services\ResourceManager;
use Symfony\Component\ClassLoader\ApcClassLoader;

class PManagerTest extends \PHPUnit_Framework_TestCase
{

//
//   public $rr;
//
//    function teststart()
//    {
//
////        $loader = require_once __DIR__.'/../../../../../app/bootstrap.php.cache';
//
//        require_once __DIR__.'/../../../../../app/AppKernel.php';
//
//
//        $kernel = new AppKernel('prod', false);
//        $kernel->loadClassCache();
//        $kernel->boot();
//        $this->rr=$kernel->getContainer();
//
//       // $kernel->terminate($request, $response);
//
//    }
    /**
     * @dataProvider providerNew
     */
    function testFoundNewTender($record)
    {
        $RManager = new ResourceManager;
        $downloader = new Downloader($RManager);
        $Pmanager = new PManager($downloader);
        $Pmanager->FoundNewTender($record);

    }

    function providerNew()
    {
        return array(
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6254030', 'id' => '6254030', 'platform' => 'ZakupkiGovRu', 'finddate' => '2013-06-06 14:08:18')),
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6254031', 'id' => '6254031', 'platform' => 'ZakupkiGovRu', 'finddate' => '2013-06-06 15:08:18')),
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6254032', 'id' => '6254032', 'platform' => 'ZakupkiGovRu', 'finddate' => '2013-06-06 16:08:18')),
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6254034', 'id' => '6254034', 'platform' => 'ZakupkiGovRu', 'finddate' => '2013-06-06 17:08:18')),
        );

    }

}
//-d auto_prepend_file=/home/mario/projects/RAFFloader/src/RAFFloader/autoload.php