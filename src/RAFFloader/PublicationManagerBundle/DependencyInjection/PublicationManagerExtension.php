<?php

namespace RAFFloader\PublicationManagerBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class PublicationManagerExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
        //Пример реализации подачи сервисов, используя теги:

//        if ($container->hasDefinition('publication_manager')) {
//            $definition = $container->getDefinition(
//                'publication_manager'
//            );
//
//            $taggedServices = $container->findTaggedServiceIds(
//                'platform.downloader'
//            );
//            var_dump($taggedServices);
//            foreach ($taggedServices as $id => $tagAttributes) {
//                foreach ($tagAttributes as $attributes) {
//                    $definition->addMethodCall(
//                        'AddDownloader',
//                        array(new Reference($id), $attributes["platform_name"])
//                    );
//                }
//            }
//        }
    }
}
