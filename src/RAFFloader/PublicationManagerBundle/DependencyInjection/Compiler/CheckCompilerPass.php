<?php
namespace RAFFloader\PublicationManagerBundle\DependencyInjection\Compiler;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class CheckCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {

        $taggedServices = $container->findTaggedServiceIds(
            'scheduler.check'
        );
        foreach ($taggedServices as $id => $attributes) {

            if (!$container->hasDefinition($id)) {
                return;
            }

            $definition = $container->getDefinition(
                $id
            );

            $definition->addMethodCall(
                'check_new'
            );
        }
    }
}
