<?php

namespace RAFFloader\PublicationManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('PublicationManagerBundle:Default:index.html.twig', array('name' => $name));
    }

}
