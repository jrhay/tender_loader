<?php
namespace RAFFloader\PublicationManagerBundle\Services;
use RAFFloader\PublicationManagerBundle\Entity\Publications;


class PublicationManager

{
    /** @var \Symfony\Component\DependencyInjection\Container */
    private $service_container;
    /** @var \Doctrine\ORM\EntityManager */
    private $EntityManager;
    /** @var \RAFFloader\LogBundle\Services\specLogger */
    private $logger;
    private $record = array(
        'uid',
        'url',
        'platform',
        'find_datetime',
        'extra'
    );

    public function __construct($service_container, $em)
    {
        $this->service_container = $service_container;
        $this->EntityManager = $em;
        $this->logger = $this->service_container->get('logger');
    }

    public function FoundNewTender(array $record, $callback)
    {
        $this->record = $record;
        $this->putRecord($this->record);

        if (is_string($callback)) {
            call_user_func($callback, $record);
        } elseif (is_callable($callback, true)) {
            if (is_array($callback)) {
                // an array: 0 - object, 1 - method name
                list($object, $method) = $callback;
                if (is_string($object)) // static method call
                    call_user_func($callback, $record);
                elseif (method_exists($object, $method))
                    $object->$method($record);
            } else // PHP 5.3: anonymous function
                call_user_func($callback, $record);
        }
        $this->logger->log(25, 'PublicationManager', 'FoundNewTender function complete', $this->record);
    }

    private function putRecord($record)
    {
        $Repository = $this->EntityManager->getRepository('PublicationManagerBundle:Publications');
        //** $Archive Archive */
        $Publication = $Repository->findOneBy(array('platform' => $record['platform'], 'uid' => $record['uid']));
        if ($Publication == false) {
            $Publication = new Publications();
            $this->EntityManager->persist($Publication);
        }
        $Publication->setPlatform($record['platform']);
        $Publication->setUid($record['uid']);
        $Publication->setFindDateTime($record['find_datetime']);
        $Publication->setUrl($record['url']);
        $Publication->setStatus(0);
        $Publication->setExtra($record['extra']);
        $this->EntityManager->flush();
        return $Publication;
    }

    private function getRecord($Platform, $uid, $extra = null)
    {
        $Repository = $this->EntityManager->getRepository('PublicationManagerBundle:Publications');
        $findArray = array('platform' => $Platform, 'uid' => $uid);
        if(!is_null($extra)){
             array_push($findArray['extra'],$extra);
        }
        if (!is_null($Publication = $Repository->findOneBy($findArray))) {
            $record = array(
                'Platform' => $Publication->getPlatform(),
                'uid' => $Publication->getUid,
                'find_datetime' => $Publication->getFindDateTime(),
                'url' => $Publication->getUrl(),
                'extra' =>$Publication->getExtra()
            );

            return $Publication;
        } else {
            return false;
        }
    }

    /** check Publication status
     *0- deleted
     *1- done
     *2- processing
     *3- waiting for download
     *4- dead id , no such page on zakupki gov ru
     * false - it means no record in Database<can be downloaded
     * @param string $platform
     * @param integer $uid
     * @return integer or boolean
     * */
    public function checkStatus($platform, $uid)
    {
        $Repository = $this->EntityManager->getRepository('PublicationManagerBundle:Publications');
        if (!is_null($Publication = $Repository->findOneBy(array('platform' => $platform, 'uid' => $uid)))) {
            $status = $Publication->getStatus();
        } else {
            return false; // ресурса в базе нет и не было
        }
        return $status;
    }

    /** set status of Publication record
     *0- deleted
     *1- done
     *2- processing
     *3- downloaded with errors or partially
     *4- dead id , no such page on zakupki gov ru
     *
     * return false if record is not exists
     *
     * @param string $platform
     * @param integer $uid
     * @param string $extra
     * @param integer $status
     * @return boolean
     */
    public function setStatus($platform, $uid, $extra, $status)
    {
        $Repository = $this->EntityManager->getRepository('PublicationManagerBundle:Publications');
        /** @var $Publication Publications */
        $Publication = $Repository ->findOneBy(array('platform' => $platform, 'uid' => $uid));

        if (is_null($Publication)) {
            $this->logger->log(75, 'Publication Manager: setStatus', 'ERROR:can\'t search record in Publications
            table');
//            $newRecord = array(
//                'uid' => $uid,
//                'platform' =>$platform,
//                'find_datetime' => new \DateTime(),
//                'url' => null,
//                'extra' => $extra
//            );
//            $Publication = $this->putRecord($newRecord);
            return false;
        }
        $Publication->setStatus($status);
        $this->EntityManager->flush();
        $this->logger->log(50, 'Publication Manager', "new status \" $status\" was  set  for uid $uid in $platform platform");
        return true;
    }

//функция AddDownloader больше не нужна, комментарий лучше оставить для примера получения сервиса через Таги
//    function AddDownloader($downloader,$platform_name)
//    {
//            // функция в процессе
//            $this->platform_downloaders[$platform_name]=$downloader;
//            $this->platform_downloaders['zakupki_gov_ru']=$downloader;
//    }
}