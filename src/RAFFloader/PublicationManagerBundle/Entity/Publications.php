<?php

namespace RAFFloader\PublicationManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Publications
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Publications
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="platform", type="string", length=255)
     */
    private $platform;

    /**
     * @var integer
     *
     * @ORM\Column(name="uid", type="integer")
     */
    private $uid;
    /**
     * @var string
     *
     *@ORM\Column(name="extra", type="string", length=255)
     *
     */
    private $extra;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="findDateTime", type="datetime")
     */
    private $findDateTime;


    /**
     * @var string
     *
     * @ORM\Column(name="url", type="blob")
     */

    private $url;

    /**
     * @var integer
     * @ORM\Column(name="status",type="integer")
     */
    private $status;


    /**
     * Get id
     *
     * @return integer
     */

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set platform
     *
     * @param string $platform
     * @return Publications
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * Get platform
     *
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * Set uid
     *
     * @param integer $uid
     * @return Publications
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set extra
     *
     * @param integer $uid
     * @return Publications
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra
     *
     * @return integer
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Set findDateTime
     *
     * @param \DateTime $findDateTime
     * @return Publications
     */
    public function setFindDateTime($findDateTime)
    {
        $this->findDateTime = $findDateTime;

        return $this;
    }

    /**
     * Get url
     *
     * @return \DateTime
     */
    public function getFindDateTime()
    {
        return $this->findDateTime;
    }

    /**
     * Set url
     *
     * @param string
     * @return Publications
     */
    public function setUrl($url)
    {
        $this->url = serialize($url);

        return $this;
    }

    /**
     * Get url     *
     * @return string
     */
    public function getUrl()
    {
        if (is_resource($this->url)) {
            $val = fread($this->url, 10000000);
        } elseif (is_string($this->url)) {
            $val = $this->url;
        } else {
            $val = false;
        }

        return unserialize($val);
    }

    /**
     * Set Status
     *0- deleted
     *1- done
     *2- processing
     *3- waiting for download
     *4- dead id , no such page on zakupki gov ru
     * @param integer $status
     * @return Publications
     * */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     *get status
     *0- deleted
     *1- done
     *2- processing
     *3- waiting for download
     *4- dead id , no such page on zakupki gov ru
     * @return integer
     *
     */
    public function getStatus()
    {
        return $this->status;
    }
}
