<?php

namespace RAFFloader\ScriptBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ScriptBundle:Default:index.html.twig', array('name' => $name));
    }
}
