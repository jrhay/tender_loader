<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 06.08.13
 * Time: 12:20
 * To change this template use File | Settings | File Templates.
 */
class Collections
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @var integer
     * @ORM\Column(name="uid", type="integer")
     * */
    private $uid;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="meta", type="blob")
     */
    private $meta;
    /**
     * @var string
     *
     * @ORM\Column(name="fullPath", type="blob")
     */
    private $fullPath;

    /** @var integer
     *
     * @ORM\Column(name ="status",type = "integer")
     * */
    private $status;
    /**
     * @var string
     *
     * @ORM\Column(name="indexed", type="boolean")
     */
    private $indexed;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set Uid
     *
     * @param integer $uid
     * @return Resources
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return Resources
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     *Set Condition
     * 0 - not exist ( deleted or not uploaded correctly)
     * 1 - done (uploaded)
     * 2 - processing now
     * @param integer $Condition
     * @return Resources
     */

    public function setStatus($Condition)
    {
        $this->status = $Condition;
        return $this;
    }


    /**
     * Get Condition
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set meta
     *
     * @param string $meta
     * @return Resources
     */
    public function setMeta($meta)
    {
        $this->meta = serialize($meta);

        return $this;
    }

    /**
     * Get meta
     *
     * @return string
     */
    public function getMeta()
    {
        if (is_resource($this->meta)) {
            $val = fread($this->meta, 10000000);
        } elseif (is_string($this->meta)) {
            $val = $this->meta;
        } else {
            $val = false;
        }

        return unserialize($val);
    }

    /**
     * Set fullPath
     *
     * @param string $fullPath
     * @return Resources
     */
    public function setFullPath($fullPath)
    {
        $this->fullPath = serialize($fullPath);

        return $this;
    }

    /**
     * Get fullPath     *
     * @return string
     */
    public function getFullPath()
    {
        if (is_resource($this->fullPath)) {
            $val = fread($this->fullPath, 10000000);
        } elseif (is_string($this->fullPath)) {
            $val = $this->fullPath;
        } else {
            $val = false;
        }

        return unserialize($val);
    }

    /**
     * Set indexed
     *
     * @param boolean $indexed
     * @return Resources
     */
    public function setIndexed($indexed)
    {
        $this->indexed = $indexed;

        return $this;
    }

    /**
     * Get indexed
     *
     * @return bool
     */
    public function getIndexed()
    {
        return $this->indexed;
    }
}
