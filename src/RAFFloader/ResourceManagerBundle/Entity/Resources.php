<?php

namespace RAFFloader\ResourceManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resources
 *
 * @ORM\Table()
 * @ORM\Entity
 */
//TODO: заменить infoinfo на resource_info
class Resources
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @var integer
     * @ORM\Column(name="uid", type="integer")
     * */
    private $uid;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="blob")
     */
    private $info;
    /**
     * @var string
     *
     * @ORM\Column(name="fullPath", type="blob")
     */
    private $fullPath;

    /** @var integer
     *
     * @ORM\Column(name ="status",type = "integer")
     * */
    private $status;
    /**
     * @var string
     *
     * @ORM\Column(name="indexed", type="integer")
     */
    private $indexed;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true )
     */
    private $date;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set Uid
     *
     * @param integer $uid
     * @return Resources
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return Resources
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     *Set Condition
     * 0 - not exist ( deleted or not uploaded correctly)
     * 1 - done (uploaded)
     * 2 - processing now
     * @param integer $Condition
     * @return Resources
     */

    public function setStatus($Condition)
    {
        $this->status = $Condition;
        return $this;
    }


    /**
     * Get Condition
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set info
     *
     * @param string $info
     * @return Resources
     */
    public function setInfo($info)
    {
        $this->info = serialize($info);

        return $this;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        if (is_resource($this->info)) {
            fseek($this->info,SEEK_SET);
            $val = fread($this->info, 1000000);
        } elseif (is_string($this->info)) {
            $val = $this->info;
        } else {
            $val = false;
        }

        return unserialize($val);
    }

    /**
     * Set fullPath
     *
     * @param string $fullPath
     * @return Resources
     */
    public function setFullPath($fullPath)
    {
        $this->fullPath = serialize($fullPath);

        return $this;
    }

    /**
     * Get fullPath     *
     * @return string
     */
    public function getFullPath()
    {
        if (is_resource($this->fullPath)) {
            fseek($this->fullPath,SEEK_SET);
            $val = fread($this->fullPath, 10000000);
        } elseif (is_string($this->fullPath)) {
            $val = $this->fullPath;
        } else {
            $val = false;
        }

        return unserialize($val);
    }

    /**
     * Set indexed
     *
     * @param boolean $indexed
     * @return Resources
     */
    public function setIndexed($indexed)
    {
        $this->indexed = $indexed;

        return $this;
    }

    /**
     * Get indexed
     *
     * @return bool
     */
    public function getIndexed()
    {
        return $this->indexed;
    }
    /**
     *Set date
     *@param /DateTime $date
     *@return Resources
     * */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     * @return \DateTime
     * */
    public function getDate()
    {
        return $this->date;
    }
}
