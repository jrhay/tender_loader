<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 29.07.13
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 *
 */
namespace RAFFloader\ResourceManagerBundle\Services\ArchiveFileSystem\Interfaces;
interface  IArchiveSystem
{
    /** Move file to Archive (if need using temp files)
     * @param $filename string
     * @param $temp_file string
     * @return
     */
    public function moveFile($filename, $temp_file);

    /** load content from file
     * @param $name string
     * @return  $raw string
     */
    public function loadContent($filename);

    /** save content to Archive
     * @param $finalFileName string
     * @param $content string
     * @return boolean
     */
    public function saveContent($filename, $content);

    /** remove  Resource from Archive
     * @param $finalFileName string
     * @return boolean
     */
    public function removeResource($filename);
}
