<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 31.07.13
 * Time: 14:47
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ResourceManagerBundle\Services\ArchiveFileSystem;
use RAFFloader\ResourceManagerBundle\Services\ArchiveFileSystem\Interfaces\IArchiveSystem;
use Gaufrette\Filesystem;
use Gaufrette\Adapter\AmazonS3 as Amazon_Adapter;
use \AmazonS3;

class ArchiveAmazonS3System implements IArchiveSystem
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    private $service_container;
    /** @var \Gaufrette\Filesystem */
    private $filesystem;
    /** @var \RAFFloader\LogBundle\Services\specLogger */
    private $logger;

    public function __construct($service_container)
    {
        $this->service_container = $service_container;
        $amazon = $this->service_container->get('amazonS3');
        /** @var $adapter AmazonS3 */
        $adapter = new Amazon_Adapter($amazon, 'bitender');
        $this->filesystem = new Filesystem($adapter);
        /** @var \RAFFloader\LogBundle\Services\specLogger $this->logger */
        $this->logger = $this->service_container->get('logger');

    }

    /** Move file to Archive (if need using temp files)
     * @param $filename string
     * @param $temp_file string
     * @return
     */
    public function moveFile($filename, $temp_file)
    {
        if (($content = file_get_contents($temp_file)) !== false) {
            if ($this->filesystem->write($filename, $content, true) !== false) {
                $this->logger->log(50, 'AmazoneS3 (Archive)', 'AmazoneS3 successfully moved temp file to it\'s cloud', array('filename' => $filename, 'temp file' => $temp_file));
                if (unlink($temp_file)) {
                    $this->logger->log(50, 'AmazoneS3 (Archive)', 'AmazoneS3 successfully deleted temp file ', array('temp file' => $temp_file));

                } else {
                    $this->logger->log(50, 'AmazoneS3 (Archive)', 'AmazoneS3 can\'t delete temp file: unlink return false', array('temp file' => $temp_file));
                }
                return true;
            }

        } else {
            $this->logger->log(75, 'AmazoneS3 (Archive)', 'AmazoneS3 can\'t move file : file_get_content() returned false, temp resource will be deleted now', array('filename' => $filename, 'temp file' => $temp_file));
            unlink($temp_file);
            return false;
        }

    }

    /** save content to Archive
     * @param $finalFileName string
     * @param $content string
     * @return boolean
     */
    public function saveContent($name, $content)
    {
        //Должен возвращать ключи, почему-то этого не делает
//        $keys = $this->filesystem->listKeys();
//        foreach ($keys['keys'] as $key){
//            $this->filesystem->delete($key);
//        }
        if ($this->filesystem->write($name, $content, true) !== false) {
            $this->logger->log(50, 'AmazoneS3 (Archive)', 'AmazoneS3 successfully saved content to it\'s cloud', array('filename' => $name));
            return true;
        } else {
            $this->logger->log(50, 'AmazoneS3 (Archive)', 'AmazoneS3 can\'t save content', array('filename' => $name));
            return false;
        }
    }

    /** remove  Resource from Archive
     * @param $finalFileName string
     * @return boolean
     */
    public function removeResource($name)
    {
        if ($this->filesystem->delete($name) !== false) {
            $this->logger->log(50, 'AmazoneS3 (Archive)', 'AmazoneS3 successfully deleted content to it\'s cloud', array('filename' => $name));
            return true;
        } else {
            $this->logger->log(50, 'AmazoneS3 (Archive)', 'AmazoneS3 can\'t delete content', array('filename' => $name));
            return false;
        }
    }

    /** load content from file
     * @param $name string
     * @return  $raw string
     */
    public function loadContent($name)
    {
        if (($content = $this->filesystem->read($name)) !== false) {
            $this->logger->log(50, 'AmazoneS3 (Archive)', 'AmazoneS3 successfully read content from it\'s cloud', array('filename' => $name));
            return $content;
        } else {
            $this->logger->log(50, 'AmazoneS3 (Archive)', 'AmazoneS3 can\'t read content', array('filename' => $name));
            return false;
        }
    }
}
