<?php
namespace RAFFloader\ResourceManagerBundle\Services\ArchiveFileSystem;
use RAFFloader\ResourceManagerBundle\Services\Resources\BaseResource;
use RAFFloader\ResourceManagerBundle\Services\ArchiveFileSystem\Interfaces\IArchiveSystem;

class ArchiveFileSystem implements IArchiveSystem
{

    /** @var \Symfony\Component\DependencyInjection\Container */
    private $service_container;
    /** @var \RAFFloader\LogBundle\Services\specLogger */
    private $logger;

    public function __construct($service_container)
    {
        $this->service_container = $service_container;
        $this->logger = $this->service_container->get('logger');
    }

    public function moveFile($finalFileName, $temp_file)
    {
        $this->prepareDir($finalFileName);
        if (copy($temp_file, $finalFileName)) {
            if (unlink($temp_file)) {
                $this->logger->log(50, 'ArchiveFileSystem', 'file saved in file system, temp file was successfully deleted', array('resource name' => $finalFileName,
                    'temp file name' => $temp_file));
                return true;
            } else {
                $this->logger->log(75, 'ArchiveFileSystem', 'Can\'t delete temp file', array('resource name' => $finalFileName,
                    'temp file name' => $temp_file));
                return false;
            }
        } else {
            $this->logger->log(75, 'ArchiveFileSystem', 'can\'t copy temp file to new directory with new name', array('resource name' => $finalFileName,
                'temp file name' => $finalFileName));
                return false;
        }


    }

    public function saveContent($finalFileName, $content)
    {
        $this->prepareDir($finalFileName);
        if (strlen($finalFileName) > 254) {
            $finalFileName = substr($finalFileName, 0, 254);
            $this->logger->log(25, 'ArchiveFileSystem', 'Long name was cut , new name is 254 bytes', array('new name' => $finalFileName,));
        }
        if (file_put_contents($finalFileName, $content)) {
            $this->logger->log(25, 'ArchiveFileSystem', 'content saved in file system', array('resource name' => $finalFileName));
            return true;
        } else {
            $this->logger->log(75, 'ArchiveFileSystem', 'can\'t save file ', array('resource name' => $finalFileName));
            return false;
        }
    }

    public function loadContent($finalFileName)
    {
        if (file_exists($finalFileName) and ($raw = file_get_contents($finalFileName)) !== false) {
            return $raw;
        } else {
            return false;
        }
    }

    public function removeResource($name)
    {
        if (!file_exists($name)) {
            print "\nArchiveFileSystem: 'can\'t remove resource, no such file $name\n";
            $this->logger->log(75, 'ArchiveFileSystem', 'can\'t remove resource, no such file', array('fileName' => $name));
            return false;
        }
        if (unlink($name)) {
            $this->logger->log(25, 'ArchiveFileSystem', 'Resource removed from Archive', array('fileName' => $name));
            return true;
        } else {
            $this->logger->log(75, 'ArchiveFileSystem', 'Can\'t remove resource from Archive', array('fileName' => $name));
            return false;
        }
    }

    public function prepareDir($fileName)
    {
        $dirArray = explode('/', $fileName);
        $name = array_pop($dirArray);
        $dir = '';
        foreach ($dirArray as $dir_part) {
            $dir .= '/' . $dir_part;
            if (!file_exists($dir)) {
                $mkDir = mkdir($dir);
                if (!$mkDir) {
                    $this->logger->log(75, 'ArchiveFileSystem', 'can\'t create directory to save file', array('file' => $fileName));
                    return false;
                }
            }
        }
        return true;
    }


}
