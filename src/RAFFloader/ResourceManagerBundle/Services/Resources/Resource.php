<?php
namespace RAFFloader\ResourceManagerBundle\Services\Resources;

interface Resource
{
    public function getRaw();

    public function getMeta();

    public function getInfo();
}