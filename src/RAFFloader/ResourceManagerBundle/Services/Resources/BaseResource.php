<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 25.06.13
 * Time: 18:56
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ResourceManagerBundle\Services\Resources;
class BaseResource implements Resource
{
    protected $raw;
    protected $metaInfo;
    protected $resource_info;
    private $id;

    public function getId()
    {
        return $this->id ? $this->id : false;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getRaw()
    {
        return $this->raw;
    }

    public function getInfo()
    {
        if (!empty($this->resource_info)) {
            return $this->resource_info;
        }
        return false;
    }

    /**
     *@param array $resource_info
     *@return $this */
    public function setInfo($resource_info)
    {
        $this->resource_info = $resource_info;
        return $this;
    }

    public function getMeta()
    {
        return $this->metaInfo;
    }

    public function setRaw($raw)
    {
        $this->raw = $raw;
    }

    public function setMeta($meta)
    {
        $this->metaInfo = $meta;
    }

    public function __construct($raw, $meta = null)
    {
        $this->setRaw($raw);
        if (!is_null($meta)) {
            $this->setMeta($meta);
        }
    }
}
