<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 25.06.13
 * Time: 19:03
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ResourceManagerBundle\Services\Resources;
use RAFFloader\ResourceManagerBundle\Services\Resources\BaseResource;

class DefaultResource extends BaseResource
{
    /** @var \RAFFloader\ResourceManagerBundle\Services\Transport\HttpTransport */
    private $transport;
    private $url;
    private $headers;
    private $http_opts = array();
    //Уникальный идентификуатор ресурса в наших базах ( не связан с uid на платформе)
    private $id;
    protected $resource_info = array(
        'platform' => '',
        'id' => '',
        'url' => '',
        'name' => '',
        'resource_type' => '',
    );
    protected $illegalCharacters = array(
        '/',
        ':',
        '*',
        '?',
        '"',
        '<',
        '>',
        '|',
        ' '
    );

    public function getInfo()
    {

        if (is_null($this->resource_info)) {
            $this->reload();
        }
        if (!is_null($this->resource_info)) {
            return $this->resource_info;
        } else {
            return false;
        }
    }

    public function getId()
    {
        return $this->id ? $this->id : false;
    }

    public function getRaw()
    {
        if (is_null($this->raw)) {
            $this->reload();
        }
        if(is_null($this->raw)){
            return false;
        }
        return $this->raw;
    }

    public function reload()
    {

        if($response = $this->transport->getResponse($this->url, $this->http_opts))
        {
        $this->raw = $response->getBody();
        $this->headers = $response->getHeaders();
            return true;
        } else {
            print "\nDefaultResource: Bad response. can't reload resource $this->url \n";
            return false;
        }
    }

    public function getHeaders()
    {
        if (is_null($this->headers)) {
            $this->reload();
        }
        if(is_null($this->headers)){
            return false;
        }
        return $this->headers;
    }

    public function setTransport($transport)
    {
        $this->transport = $transport;
    }

    public function rename($newName = null)
    {
        $fileName = '';
        if (!is_null($newName)) {
            $fileName = $newName;

        } else {
            $this->getHeaders();
            if (preg_match_all('/filename="(.*)"/', $this->headers, $fileName)) {
                $fileName = $fileName[1][0];
                $fileName = str_replace($this->illegalCharacters, '_', urldecode($fileName));
            } else {
                return false;
            }
        }

        $nameAr = explode('/', $this->resource_info['name']);
        array_pop($nameAr);
        array_push($nameAr, $fileName);
        $name = implode('/', $nameAr);
        $this->resource_info['name'] = $name;


        return $this->resource_info['name'];
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function __construct($resource_info, $transport, $baseId = null)
    {
        $this->url = $resource_info['url'];
        $this->resource_info = $resource_info;
        $this->setTransport($transport);
        if (!is_null($baseId)) {
            $this->id = $baseId;
        } else {
            $this->baseId = uniqid("res-");
        }
    }
}
