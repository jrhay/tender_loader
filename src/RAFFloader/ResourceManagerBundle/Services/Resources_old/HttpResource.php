<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 25.06.13
 * Time: 19:03
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ResourceManagerBundle\Services\Resources;
use RAFFloader\ResourceManagerBundle\Services\Resources\BaseResource;

class HttpResource extends BaseResource
{
    private $transport;
    private $url;
    private $headers;
    private $http_opts = array();

    public function getRaw()
    {
        if (is_null($this->raw)) {
            $this->reload();
        }

        return $this->raw;
    }

    public function reload()
    {
        $response = $this->transport->getResponse($this->url, $this->http_opts);
        $this->raw = $response->getBody();
        $this->headers = $response->getHeaders();
    }

    public function getHeaders()
    {
        if (is_null($this->headers)) {
            $this->reload();
        }
        return $this->headers;
    }

    public function setTransport($transport)
    {
        $this->transport = $transport;
    }

    public function __construct($url, $transport)
    {
        $this->url = $url;
        $this->setTransport($transport);
        $this->reload();
    }
}
