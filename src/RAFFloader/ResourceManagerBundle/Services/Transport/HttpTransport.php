<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 25.06.13
 * Time: 19:31
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ResourceManagerBundle\Services\Transport;
use RAFFloader\ResourceManagerBundle\Services\Transport\HttpResponse;

class HttpTransport
{
    private $headers = array();
    private $filePointer;
    private $tempPath;
    private $answer;
    private $responceString;
    private $size;
    private $tempFile;
    private $badAnswers = array(
        'Страница не найдена',
        'Превышен лимит времени ожидания',
        'страница временно недоступна',
        'cервер временно недоступен',
        'Внутренняя ошибка сервера',
        'Cервер временно недоступен',
        'Сайт временно недоступен',
        '<title>\u0421\u0430\u0439\u0442 \u0432\u0440\u0435\u043c\u0435\u043d\u043d\u043e \u043d\u0435\u0434\u043e\u0441\u0442\u0443\u043f\u0435\u043d</title>',
        '\u041f\u0440\u0435\u0432\u044b\u0448\u0435\u043d \u043b\u0438\u043c\u0438\u0442 \u0432\u0440\u0435\u043c\u0435\u043d\u0438 \u043e\u0436\u0438\u0434\u0430\u043d\u0438\u044f',
        'Ведутся регламентные работы, приносим извинения за неудобства',
    );

    public function  __construct($temp_path)
    {
        $this->tempPath = $temp_path;
    }

    public function getResponse($url, $opts)
    {
        //        вариант с передачей темпового файла:
        $this->tempPath = __DIR__ . $this->tempPath;
        if (!file_exists($this->tempPath)) {
            mkdir($this->tempPath);
        }
        //при многопоточном запуске приложения ( на боевом сервере ) возможна ситуация, что из разных потоков будет одновременная попытка создать одноименные файлы,
        //чтобы это избежать введена функция rand();
        $temp_file = $this->tempPath . '/HTTP_transport_temp_resource' . uniqid('', true);
        $this->tempFile = $temp_file;

        $opts = $opts + array(
            //CURLOPT_RETURNTRANSFER => true,
            CURLOPT_WRITEFUNCTION => array($this, 'saveFile'),
            CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:13.0) Gecko/20100101 Firefox/13.0.1',
            CURLOPT_HEADERFUNCTION => array($this, 'saveHeader'),
            //  CURLOPT_FILE => $this->filePointer,
        );

        $curl = curl_init($url);
        curl_setopt_array($curl, $opts);
        $count = 0;
        do {
            $this->filePointer = fopen($temp_file, 'w+');
            echo "\n Downloading $url  bu curl starts \n";
            $find_datetime_start = microtime(true);
            curl_exec($curl);
            $find_datetime_end = microtime(true);
            fclose($this->filePointer);
            //TODO: условие правильного ответа
            if ($this->checkAnswer()) {
                $answer = true;
                break;
            }
            echo "\n Server returned bad answer  , try again after 20 seconds\n";
            if(!unlink($temp_file)){
                print "WARNING: can't delete temp file (HTTP transport)";
            }
            sleep(25);
            $answer = false;
            $count++;
        } while ($count < 2);

        $find_time = $find_datetime_end - $find_datetime_start;



        $this->headers = array('http_headers' => $this->headers);
        $this->headers = serialize($this->headers);
        if ($answer !== false) {
            $answer = $temp_file;
            $fileINfo = stat($temp_file);
            if (!empty($fileINfo['size'])) {
                $size = 0.001 * $fileINfo['size'];
            } else {
                $size = '<unknown number>';
//                if(!filesize($temp_file)){
//                    $answer = false;
//                    $size = 0;
//                }
            }
            echo "\n Uploaded $size kilobytes  in  $find_time seconds \n";
            return new HttpResponse($this->headers, $answer);
        } else {
            return false;
        }
    }

    public function getResponseData($url, $opts)
    {

        $curl = curl_init($url);
        $opts = $opts + array(
            //CURLOPT_RETURNTRANSFER => true,
            CURLOPT_WRITEFUNCTION => array($this, 'saveFile'),
            CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:13.0) Gecko/20100101 Firefox/13.0.1',
            CURLOPT_HEADERFUNCTION => array($this, 'saveHeader'),
            //  CURLOPT_FILE => $this->filePointer,
        );
        curl_setopt_array($curl, $opts);

        $count = 0;
        sleep(10);
        do {
            echo "\n Downloading $url  bu curl starts \n";
            $find_datetime_start = microtime(true);
            curl_exec($curl);
            $answer = $this->answer;
            $find_datetime_end = microtime(true);
            if ($this->checkAnswer($answer)) {
                break;
            }

            echo "\n Server returned bad answer  , try again after 20 seconds\n";
            sleep(25);
            $answer = false;
            $count++;
        } while ($count < 2);


        $find_time = $find_datetime_end - $find_datetime_start;
        $size = 0.001 * strlen($answer);
        echo "\n Uploaded $size kilobytes  in  $find_time seconds \n";

        $this->headers = array('http_headers' => $this->headers);
        $this->headers = serialize($this->headers);
        if ($answer !== false) {
            return new HttpResponse($this->headers, $answer);
        } else {
            return false;
        }
    }

    public function saveHeader($curl, $headers)
    {
       // $this->headers = array();
        $this->headers[] = $headers;
        return strlen($headers);
    }

    // Ф-я для передачи в call-back формате curl'y , сохраняет ответ в поле класса
    public function saveData($curl, $data)
    {
        $this->responceString = $data;
        $this->answer .= $this->responceString;
        $length = strlen($this->responceString);
        $this->size += $length;
        return $length;
    }

// Ф-я для передачи в call-back формате curl'y , сохраняет ответ в темповом файле
    public function saveFile($curl, $data)
    {
        $this->responceString = $data;
        $length = fwrite($this->filePointer, $this->responceString);
        $this->size += $length;
        return $length;
    }

    public function checkAnswer()
    {

        $type = mime_content_type($this->tempFile);
        if($type !== 'text/html' and filesize($this->tempFile)){
            return true;
        }
        $content = file_get_contents($this->tempFile);
        foreach($this->badAnswers as $str){
        if(strpos($content, $str) !== false){
            return false;
        }
        }
        if(!filesize($this->tempFile)){
            return false;
        }
        return true;


//        $pattern = '/Content-Type:(.text\/html.);/';
//        $subject = implode($this->headers);
//        if(!preg_match_all($pattern,$subject,$matches)){
//            return true;
//        }
//        $file = file($this->tempFile);
//        if (empty($file)) {
//            return false;
//        }
//        $checkedArray = array_map(array($this,'checkAnswerString'),$file);
//        if(in_array(false,$checkedArray)){
//            return false;
//        }
//        return true;
    }

    public function checkAnswerString($answer)
    {
        foreach ($this->badAnswers as $string) {
            if (strpos($answer, $string) !== false) {
                return false;
            }
        }
        //Проверка на пустой ответ
//        if (is_null($answer)) {
//            return false;
//        }
        return true;
    }
}
