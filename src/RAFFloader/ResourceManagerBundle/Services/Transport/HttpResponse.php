<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 25.06.13
 * Time: 19:35
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ResourceManagerBundle\Services\Transport;
class HttpResponse
{
    private $body;
    private $headers;

    public function getBody()
    {
        return $this->body;
    }

    public function getHeaders()
    {

        return $this->headers;
    }

    public function __construct($headers, $body)
    {
        $this->headers = $headers;
        $this->body = $body;
    }

}
