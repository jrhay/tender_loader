<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 26.06.13
 * Time: 11:58
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ResourceManagerBundle\Services\Transport;
use RAFFloader\ResourceManagerBundle\Services\Transport\FtpResponse;

class FTPTransport
{
    private $filePointer;
    private $tempPath;

    public function __construct($temp_path)
    {
        $this->tempPath = $temp_path;
    }

    public function getResponse($url, $opts)
    {
        $this->tempPath = __DIR__ . $this->tempPath;
        if (!file_exists($this->tempPath)) {
            mkdir($this->tempPath);
        }
        $temp_file = $this->tempPath . '/temp_resource' . uniqid('', true);
        $this->filePointer = fopen($temp_file, 'w+');
        $opts = $opts + array(
            CURLOPT_RETURNTRANSFER => false,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:13.0) Gecko/20100101 Firefox/13.0.1',
            CURLOPT_FTPASCII => true, //данные с фтп возвращаются простым текстом
            CURLOPT_HEADER => true,
            CURLOPT_HEADERFUNCTION => array($this, 'saveHeader'),
            CURLOPT_FILE => $this->filePointer,

        );
        $curl = curl_init($url);
        curl_setopt_array($curl, $opts);
        $answer = curl_exec($curl);
        list($headers, $response) = explode("\r\n\r\n", $answer, 2);
        $headers = explode("\n", $headers);
        return new FTPResponse($headers, $answer);

    }

    public function saveHeader($curl, $headers)
    {
        $this->headers[] = $headers;
        return strlen($headers);
    }

}
