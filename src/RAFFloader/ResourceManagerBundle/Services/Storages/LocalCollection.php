<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 09.07.13
 * Time: 17:58
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ResourceManagerBundle\Services\Storages;
use Doctrine\ORM\EntityManager;
use RAFFloader\ResourceManagerBundle\Entity\Resources;
use RAFFloader\ResourceManagerBundle\Services\Storages\Interfaces\IResourceCollection;
use RAFFloader\ResourceManagerBundle\Services\Resources\BaseResource;
use Symfony\Component\Config\Definition\Exception\Exception;

class LocalCollection implements IResourceCollection
{
//TODO: добавить использование базы данных коллекций
    private $list = array();
    /** @var $this->ArchiveFileSystem RAFFloader\ResourceManagerBundle\Services\ArchiveFileSystem\Interfaces\IArchiveSystem */
    private $ArchiveFileSystem;
    private $archive_path;
    public $uid;
    /** @var  \ProjectServiceContainer */
    private $service_container;


    /**
     * @param $Resource \RAFFloader\ResourceManagerBundle\Services\Resources\DefaultResource
     * @return boolean
     */
    public function addResource($Resource)
    {
        /** @var $Resource \RAFFloader\ResourceManagerBundle\Services\Resources\DefaultResource */
        $resource_info = $Resource->getInfo();
        $content = $Resource->getRaw();
        $metaInfo = $Resource->getHeaders();

        $rawPath = $this->getPath($resource_info, 'raw');
        $metaPath = $this->getPath($resource_info, 'meta');

        if ($this->ArchiveFileSystem->moveFile($rawPath, $content) &&
            $this->ArchiveFileSystem->saveContent($metaPath, $metaInfo)
        ) {
            return true;
        } else {
            return false;
        }
    }

    /** @return boolean */
    public function deleteResource($id)
    {
        $this->pullList();
        /** @var $resource \RAFFloader\ResourceManagerBundle\Services\Resources\DefaultResource */
        if (empty($this->list[$id])) {
            return false;
        }
        if (!empty($this->list[$id]['fullPath']['rawPath'])) {
            $rawPath = $this->list[$id]['fullPath']['rawPath'];
            if (!empty($this->list[$id]['fullPath']['metaPath'])) {
                $metaPath = $this->list[$id]['fullPath']['metaPath'];
            } else {
                $metaPath = 'no_meta_Path';
            }
            if (
                $this->ArchiveFileSystem->removeResource($rawPath) &&
                $this->ArchiveFileSystem->removeResource($metaPath)
            ) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /** @return array */
    public function getResources()
    {
        $this->pullList();
        if ($this->list) {
            return $this->list;
        }
        return false;
    }

    public function __construct($ArchiveFileSystem, $archive_path, $uid, $service_container)
    {
        $this->ArchiveFileSystem = $ArchiveFileSystem;
        $this->archive_path = $archive_path;
        $this->service_container = $service_container;
        $this->uid = $uid;
        $this->pullList();
    }

    public function getPath($resource_info, $subdir)
    {

        $name = $resource_info['name'];
        $uid = strval($resource_info['uid']);
        $dir = array();
        $dir[] = $resource_info['platform'];
        while (strlen($uid) > 4) {
            $dir[] = substr($uid, 0, 4);
            $uid = substr($uid, 4);
        }
        $dir[] = $uid;

        if ($this->archive_path == false) {
            $this->archive_path = 'Archive';
        }
        $fullFilename = $this->archive_path;


        $dir[] = 'Publication';
        $dir[] = $subdir;

        $nameParts = explode('/', $name);
        $lastNamePart = array_pop($nameParts);
        foreach ($nameParts as $part) {
            $dir[] = $part;
        }

        foreach ($dir as $dir_part) {
            $fullFilename .= '/' . $dir_part;
        }


        $fullFilename .= '/' . $lastNamePart;
        return $fullFilename;
    }

    /** Return resource with   given id
    @param $id integer
    @return  \RAFFloader\ResourceManagerBundle\Services\Resources\DefaultResource */
    public function getResource($id)
    {
        $this->pullList();
        if (!empty($this->list[$id]['fullPath']['rawPath'])) {
            $rawPath = $this->list[$id]['fullPath']['rawPath'];
            if (!empty($this->list[$id]['fullPath']['metaPath'])) {
                $metaPath = $this->list[$id]['fullPath']['metaPath'];
            }
            if (!empty($this->list[$id]['info'])) {
                $info = $this->list[$id]['info'];
            } else {
                $info = null;
            }

                $raw = $this->ArchiveFileSystem->loadContent($rawPath);

            if ($raw) {
                $meta = $this->ArchiveFileSystem->loadContent($metaPath);
                $resource = new BaseResource($raw, $meta);
                $resource->setId($id);
                $resource->setInfo($info);
                return $resource;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function pullList()
    {
        $uid = $this->uid;
        $dql = "select r from ResourceManagerBundle:Resources r where r.uid = $uid ";
        /** @var EntityManager $em */
        $em = $data = $this->service_container->get('doctrine.orm.entity_manager');
        $listArray = $em->createQuery($dql)->getResult();
        if (!$listArray or empty($listArray[0])) {
            return false;
        } else {
            /** @var Resources $rec */
            foreach ($listArray as $rec) {
                $id = $rec->getId();
                $fullPath = $rec->getFullPath();
                $info = $rec->getInfo();
                $this->list[$id] = array('fullPath' => $fullPath, 'info' => $info);
            }
        }


    }
}
