<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 09.07.13
 * Time: 18:02
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ResourceManagerBundle\Services\Storages;
use RAFFloader\ResourceManagerBundle\Services\Storages\Interfaces\ICollectionManager;
use RAFFloader\ResourceManagerBundle\Services\Storages\LocalCollection;

class CollectionManager implements ICollectionManager
{
    private $ArchiveFileSystem;
    private $archive_path;
    private $collections = array();
    private $service_container;


    public function __construct($ArchiveFileSystem, $archive_path, $service_container)
    {
        $this->ArchiveFileSystem = $ArchiveFileSystem;
        $this->archive_path = $archive_path;
        $this->service_container = $service_container;

    }

    /** @return  ResourceCollection */
    public function getCollection($uid)
    {
        if(isset($this->collections[$uid])) {
            return $this->collections[$uid];
        }
        return $this->doCreateCollection($uid);
    }

    public function createCollection($uid) {
        return $this->getCollection($uid);
    }

    /** @return  boolean */
    public function doCreateCollection($uid)
    {
        $newCollection =new LocalCollection($this->ArchiveFileSystem, $this->archive_path, $uid, $this->service_container);
        $this->collections[$uid] = $newCollection;
        return $newCollection;

    }

    /** @return  boolean */
    public function dropCollection($uid)
    {
        // TODO: Implement dropCollection() method.
    }
}
