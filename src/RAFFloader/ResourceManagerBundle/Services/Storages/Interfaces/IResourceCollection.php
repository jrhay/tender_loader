<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 09.07.13
 * Time: 16:41
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ResourceManagerBundle\Services\Storages\Interfaces;
interface IResourceCollection
{
    /** @return boolean */
    public function addResource($Resource); //добавляет в текущую коллекцию ресурс, реализующий интерфейс ресурс
    /**delete resource from collection and from Archive
     * @param $id
     * @return boolean
     */
    public function deleteResource($id); //$name - имя ресурса внутри коллекции
    /** Return all resources from   collection
     * @return array
     */
    public function getResources(); //возвращает массив из id ресурсов , хранящихся в этой коллекции
    /** Return resource with   given id
     @param $id integer
     @return  \RAFFloader\ResourceManagerBundle\Services\Resources\DefaultResource */
    public function getResource($id); // возвращает ресурс с id, относящийся к этой коллекции
}
