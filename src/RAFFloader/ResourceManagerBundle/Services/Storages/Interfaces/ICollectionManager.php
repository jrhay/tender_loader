<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 09.07.13
 * Time: 16:33
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ResourceManagerBundle\Services\Storages\Interfaces;
use RAFFloader\ResourceManagerBundle\Services\Storages\Interfaces\ResourceCollection;

interface ICollectionManager
{
    /** @return  ResourceCollection */
    public function getCollection($uid); //возвращает коллекцию(объект типа StorageCollection),где uid - уник.идент. публикации
    /** @return  boolean */
    public function createCollection($uid); //создает коллекцию ( например при нахождении новой публикации), где uid - уник.идент. публикации
    /** @return  boolean */
    public function dropCollection($uid); //удаляет коллекцию ( пока наверно не нужна, но может пригодится)
}
