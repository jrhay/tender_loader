<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 09.07.13
 * Time: 16:33
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ResourceManagerBundle\Services\Storages\Interfaces;
use  RAFFloader\ResourceManagerBundle\Services\Resources\Resource;

interface IResourceStorage
{
    /** @return Resource */
    public function getResource($id); // возвращает ресурс( инициализированный объект со всем содержимым)где $id - уникальный id ресурса ( может содержать что угодно)
    /** @return boolean */
    public function saveResource($Resource, $deleteTempFile = false); // передает ресурс на сохранение файловой системе, $deleteTempFile = false - флаг, поясняющий удалять ли временный файл.
}
