<?php
namespace RAFFloader\ResourceManagerBundle\Services;
use Doctrine\ORM\EntityRepository;
use RAFFloader\ResourceManagerBundle\Services\Resources\DefaultResource;
use RAFFloader\ResourceManagerBundle\Services\Storages\Interfaces\IResourceCollection;
use RAFFloader\ResourceManagerBundle\Services\Transport\HttpTransport;
use RAFFloader\ResourceManagerBundle\Services\Transport\FtpTransport;
use RAFFloader\ResourceManagerBundle\Services\Resources\BaseResource;
use RAFFloader\ResourceManagerBundle\Entity\Resources;

class ResourceManager
{
    /** @var \RAFFloader\ResourceManagerBundle\Services\Storages\CollectionManager */
    private $collectionManager;
    /** @var \Doctrine\ORM\EntityManager */
    private $EntityManager;
    /** @var \Symfony\Component\DependencyInjection\Container */
    private $service_container;
    private $temp_path;
    private $uid;
    /**
     * @var \RAFFloader\LogBundle\Services\specLogger
     */
    private $logger;

    public function getRecord($id){
        $Repository = $this->EntityManager->getRepository('ResourceManagerBundle:Resources');
        if ($id) {
            $resourceRecord = $Repository->findOneBy(array('id' => $id));
            if ($resourceRecord == false) {
                $this->logger->log(75, 'ResourceManager: getRecord', 'can\'t  find a record by ID',
                    array('id' => $id));
                print "\nERROR:ResourceManager: getRecord:can't  find a record by ID = $id";
                return false;
            }
        } else {
            $this->logger->log(75, 'ResourceManager: getRecord', 'bad argument was given to function ($id is false)');
            return false;
        }
        return $resourceRecord;
    }



    public function __construct($collectionManager, $em, $service_container, $temp_path)
    {
        $this->collectionManager = $collectionManager;
        $this->EntityManager = $em;
        $this->service_container = $service_container;
        $this->temp_path = $temp_path;
        $this->logger = $this->service_container->get('logger');
    }

    /** create resource and load in to Archive,return id
     * @param $resource_info array
     * @return  $id integer
     */

    public function getTempPass()
    {
        return $this->temp_path;
    }

    public function deleteTempFile($tempFile)
    {
        if (file_exists($tempFile) && $res = unlink($tempFile)) {

            //$pos = strpos($tempFile, ".");
            //$tempFile = substr($tempFile, 0, $pos);
            $nameAr = explode('/', $tempFile);
            $name = array_pop($nameAr);
            $tempPath = $this->temp_path;
            $tempPathAr = explode('/', $tempPath);
            $dir = array_pop($tempPathAr);

            while ($nameAr) {
                $part = array_pop($nameAr);
                if ($part === $dir) {
                    break;
                } else {
                    $ar = $nameAr;
                    array_push($ar, $part);
                    $dirToDel = implode('/', $ar);
                    if ($is_empty = count(glob($dirToDel . '/*')) ? false : true) {
                        rmdir($dirToDel);
                    }
                }
            }

            return true;
        } elseif (!file_exists($tempFile)) {
            $this->logger->log(50, 'ResourceManager', 'TempFile is not exists');
            return true;
        }

        $this->logger->log(75, 'Indexer', 'can\'t remove temp file');


        return false;
    }


    public function setIndexed($id, $indexed)
    {

        /**
         * @var  EntityRepository $Repository
         * @var Resources $resourceRecord
         */
        $Repository = $this->EntityManager->getRepository('ResourceManagerBundle:Resources');
        if ($id) {
            $resourceRecord = $Repository->findOneBy(array('id' => $id));
            if ($resourceRecord == false) {
                $this->logger->log(75, 'ResourceManager: PutRecordToDatabase', 'can\'t  find a record by ID', array('id' => $id));
                print "\nERROR:ResourceManager: PutRecordToDatabase:can't  find a record by ID = $id";
                return false;
            }
        } else {
            $this->logger->log(75, 'ResourceManager: setIndexed', 'bad argument was given to function ($id is false)');
            return false;
        }
        $res = $resourceRecord->setIndexed($indexed);
        if (!$res) {
            $this->logger->log(75, 'ResourceManager: setIndexed', 'error, while setting indexed value');
            return false;
        }
        $this->EntityManager->flush();
        $newId = $resourceRecord->getId();
        return $newId;
    }

    public function setStatus($id, $status)
    {

        /**
         * @var  EntityRepository $Repository
         * @var Resources $resourceRecord
         */
        $Repository = $this->EntityManager->getRepository('ResourceManagerBundle:Resources');
        if ($id) {
            $resourceRecord = $Repository->findOneBy(array('id' => $id));
            if ($resourceRecord == false) {
                $this->logger->log(75, 'ResourceManager: PutRecordToDatabase', 'can\'t  find a record by ID', array('id' => $id));
                print "\nERROR:ResourceManager: PutRecordToDatabase:can't  find a record by ID = $id";
                return false;
            }
        } else {
            $this->logger->log(75, 'ResourceManager: setStatus', 'bad argument was given to function ($id is false)');
            return false;
        }
        $res = $resourceRecord->setStatus($status);
        if (!$res) {
            return false;
        }
        $this->EntityManager->flush();
        $newId = $resourceRecord->getId();
        return $newId;
    }

    public function createTemp($id)
    {
        /** @var BaseResource $resource */
        $resource = $this->getResource($id);
        if (!$resource) {
            $this->logger->log(75, 'ResourceManager:createTemp()', 'can\'t get resource');
            return false;
        }
        $raw = $resource->getRaw();
        $info = $resource->getInfo();
        $oldName = $info['name'];
        $extansion = $this->getExtansion($oldName);
        $extansion ?
        $name = 'temp_createTemp_' . uniqid() . '.' . $extansion :
            $name = 'temp_createTemp' . uniqid() ;
        $fullName = __DIR__ . $this->temp_path . '/' . $name;
        $newName = $this->isValidName($fullName, true);
        if ($fullName !== $newName) {
            $fullName = $newName;
        }
        $res = file_put_contents($fullName, $raw);
        if (!$res) {
            $this->logger->log(75, 'ResourceManager: createTemp', 'can\'t create temp file');
            return false;
        }


        return $fullName;
    }

    public function getExtansion($name)
    {
        $Ar = explode('.', $name);
        $extension = array_pop($Ar);
        if (strlen($extension) <= 4) {
            return $extension;
        } else {
            return '.txt';
        }
    }

    public function loadMarkedRes($id)
    {
        $dql = "select r from ResourceManagerBundle:Resources r where r.id = :id";
        /** @var Resources $record */
        $record = $this->EntityManager->createQuery($dql)->setParameter(':id', $id)->getResult();
        if (empty($record) or !is_a($record[0], "\\RAFFloader\\ResourceManagerBundle\\Entity\\Resources")) {
            print "ResourceManager:loadMarkedRes: can't load marked resource $id - can't find such record in DB";
            $this->logger->log(75, 'ResourceManager', "can't load marked resource $id - can't find such record in DB", array('id' => $id));
            return false;
        } else {
            $record = $record[0];
        }
        $resource_info = $record->getInfo();
        if (!$resource_info) {
            print "ResourceManager:loadMarkedRes: can't get resource_info from Resource record";
            $this->logger->log(75, 'ResourceManager', "can't get resource_info from Resource record", array('id' => $id));
            return false;
        }
        $this->logger->log(25, 'ResourceManager', 'creating marked Resource', array('resource_info' => $resource_info));
        /** @var $resource DefaultResource */
        switch ($resource_info['resource_type']) {
            case 'http':
                $resource = new DefaultResource($resource_info, new HttpTransport($this->temp_path));
                break;
            case 'ftp':
                $resource = new DefaultResource($resource_info, new FtpTransport($this->temp_path));
                break;
            default:
                $resource = new DefaultResource($resource_info, new HttpTransport($this->temp_path));
        }
        $loadRes = $resource->reload();
        if (!$loadRes) {
            $this->logger->log(25, 'ResourceManager', 'can\'t load marked resource', array('res_info' => $resource_info));
            return false;
        }

        $rnRes = $resource->rename();
        if (!$rnRes) {
            $this->logger->log(25, 'ResourceManager', 'can\'t rename marked resource', array('res_info' => $resource_info));
        } else {
            $this->logger->log(25, 'ResourceManager', 'marked Resource renamed', array('new resource info' => $resource_info));
        }
        $resource_info = $resource->getInfo();
        $info = $resource_info;
        $uid = $resource_info['uid'];

        if (($collection = $this->collectionManager->getCollection($uid)) == false) {
            /** @var  $collection \RAFFloader\ResourceManagerBundle\Services\Storages\LocalCollection */
            $collection = $this->collectionManager->createCollection($uid);
            if ($collection == false) {
                print "\nERROR:ResourceManager: createMarked() resource: can't create collection ";
                $this->logger->log(75, 'ResourceManager: createMarked()', 'can\'t create collection', array('uid' => $uid));
                return false;
            }
            $this->logger->log(25, 'ResourceManager', 'Collection created', array('new collection uid' => $uid));
        }
        $metaPath = $collection->getPath($resource_info, 'meta');
        $rawPath = $collection->getPath($resource_info, 'raw');
        $fullPath = array('rawPath' => $rawPath, 'metaPath' => $metaPath);
        $BaseName = $resource_info['platform'] . '-' . $resource_info['uid'] . '-' . utf8_decode($resource_info['name']);
        $condition = 1;

        $indexed = 0;
        $dt = new \DateTime();
        $newResId = $this->putRecordToDatabase($uid, $BaseName, $info, $fullPath, $condition, $indexed,$dt, $id);
        //$newResId = $this->setStatus($id, $condition);
        $resource->setId($newResId);
        $addResult = $collection->addResource($resource);
        if ($addResult) {
            $this->logger->log(25, 'ResourceManager', 'new resource created', $resource->getInfo());
            $this->logger->log(50, 'ResourceManager', 'new resource created', $resource->getInfo());
            $id = $resource->getId();
            if ($id) {
                return $id;
            } else {
                return false;
            }
        } else {
            $this->logger->log(50, 'ResourceManager', 'can\t add resource to collection', $resource->getInfo());
            return false;
        }

    }

    public function create(array $resource_info)
    {
        $this->logger->log(25, 'ResourceManager', 'creating Resource', array('resource_info' => $resource_info));
        /** @var $resource DefaultResource */
        switch ($resource_info['resource_type']) {
            case 'http':
                $resource = new DefaultResource($resource_info, new HttpTransport($this->temp_path));
                break;
            case 'ftp':
                $resource = new DefaultResource($resource_info, new FtpTransport($this->temp_path));
                break;
            default:
                $resource = new DefaultResource($resource_info, new HttpTransport($this->temp_path));
        }
        $loadRes = $resource->reload();
        if (!$loadRes) {
            $this->logger->log(25, 'ResourceManager', 'can\'t load resource', array('res_info' => $resource_info));
            return false;
        }

        $rnRes = $resource->rename();
        if (!$rnRes) {
            $this->logger->log(25, 'ResourceManager', 'can\'t rename resource', array('res_info' => $resource_info));
        } else {
            $this->logger->log(25, 'ResourceManager', 'Resource renamed', array('new resource info' => $resource_info));
        }
        $resource_info = $resource->getInfo();
        $info = $resource_info;
        $uid = $resource_info['uid'];

        if (($collection = $this->collectionManager->getCollection($uid)) == false) {
            /** @var  $collection \RAFFloader\ResourceManagerBundle\Services\Storages\LocalCollection */
            $collection = $this->collectionManager->createCollection($uid);
            if ($collection == false) {
                print "\nERROR:ResourceManager: createMarked() resource: can't create collection ";
                $this->logger->log(75, 'ResourceManager: create()', 'can\'t create collection', array('uid' => $uid));
                return false;
            }
            $this->logger->log(25, 'ResourceManager', 'Collection created', array('new collection uid' => $uid));
        }
        $metaPath = $collection->getPath($resource_info, 'meta');
        $rawPath = $collection->getPath($resource_info, 'raw');
        $fullPath = array('rawPath' => $rawPath, 'metaPath' => $metaPath);
        $BaseName = $resource_info['platform'] . '-' . $resource_info['uid'] . '-' . utf8_decode($resource_info['name']);
        $condition = 1;

        $indexed = 0;

        $newResId = $this->putRecordToDatabase($uid, $BaseName, $info, $fullPath, $condition, $indexed, false );
        $resource->setId($newResId);
        $addResult = $collection->addResource($resource);
        if ($addResult) {
            $this->logger->log(25, 'ResourceManager', 'new resource created', $resource->getInfo());
            $this->logger->log(50, 'ResourceManager', 'new resource created', $resource->getInfo());
            $id = $resource->getId();
            if ($id) {
                return $id;
            } else {
                return false;
            }
        } else {
            $this->logger->log(50, 'ResourceManager', 'can\t add resource to collection', $resource->getInfo());
            return false;
        }
    }

    public function markForLoad($resource_info)
    {
        $uid = $resource_info['uid'];
        /** @var IResourceCollection $collection */
        $collection = $this->collectionManager->createCollection($uid);
        $info = $resource_info;
        $metaPath = $collection->getPath($resource_info, 'meta');
        $rawPath = $collection->getPath($resource_info, 'raw');
        $fullPath = array('rawPath' => $rawPath, 'metaPath' => $metaPath);
        $BaseName = $resource_info['platform'] . '-' . $resource_info['uid'] . '-' . utf8_decode($resource_info['name']);
        $condition = 3;
        $indexed = 0;
        $dt = new \DateTime();
        $newResId = $this->putRecordToDatabase($uid, $BaseName, $info, $fullPath, $condition, $indexed, $dt);
        return $newResId;
    }

    /** put o  update record in Resources Database
     * @param $id - id in DB Resource (optional parametr)
     * @param $uid - Publication $uid
     * @param $name - ResourceName (don't change name if it 's not necessary)
     * @param $info - resource_info[]
     * @param array $fullPath - array with two keys : metaPath and rawPath
     * @param $condition - status of current resource
     * @param $indexed - indexed status
     * @return integer $id
     */
    public function putRecordToDatabase($uid, $name, $info, $fullPath, $condition, $indexed, $date,  $id = false)
    {
        $Repository = $this->EntityManager->getRepository('ResourceManagerBundle:Resources');
        if ($id) {
            $resourceRecord = $Repository->findOneBy(array('id' => $id));
            if ($resourceRecord == false) {
                $this->logger->log(75, 'ResourceManager: PutRecordToDatabase', 'can\'t  find a record by ID', array('id' => $id));
                print "\nERROR:ResourceManager: PutRecordToDatabase:can't  find a record by ID = $id";
                return false;
            }
        } else {
            if (in_array(false, array($uid, $name, $info, $fullPath, $condition,  $date))) {
                $this->logger->log(75, 'ResoureManager:putRecordToDatabase', 'empty value detected in given parametrs', array('uid' => $uid, 'name' => $name, 'metaInfo' => $info, 'fullPath' => $fullPath, 'condition' => $condition, 'indexed' => $indexed));
                return false;
            }
            $resourceRecord = new Resources;
            $this->EntityManager->persist($resourceRecord);
        }
        if ($uid !== false) {
            $resourceRecord->setUid($uid);
        }
        if ($name !== false) {
            $resourceRecord->setName($name);
        }

        if ($info !== false) {
            $resourceRecord->setInfo($info);
        }

        if ($fullPath !== false) {
            $resourceRecord->setFullPath($fullPath);
        }

        if ($condition !== false) {
            $resourceRecord->setStatus($condition);
        }
        if ($indexed !== false) {
            $resourceRecord->setIndexed($indexed);
        }
        if ($date !== false) {
            $resourceRecord->setDate($date);
        }

        $this->EntityManager->flush();
        $this->logger->log(50, 'ResourceManager', 'record in database created\updated', array($name, $info));
        return $resourceRecord->getId();
    }


    public function deleteRecordFromDatabase($id)
    {
        $Repository = $this->EntityManager->getRepository('ResourceManagerBundle:Resources');
        $resourceRecord = $Repository->findOneBy(array('id' => $id));
        $resourceRecord->setStatus(0);
        $this->EntityManager->flush();
        $this->logger->log(50, 'ResourceManager', 'Record was deleted ($condition = 0 ) from base "Resources"');
        return true;
    }

    /** getting Resource
     * @param $id  - Resource Id ( in Database Resources
     * @param $uid - Publication Id == Collection Id
     * @return $resource BaseResource
     */
    public function getResource($id, $uid = false)
    {
        $Repository = $this->EntityManager->getRepository('ResourceManagerBundle:Resources');
        /** @var $ResourceRecord Resources */
        $ResourceRecord = $Repository->findOneBy(array('id' => $id));
        //$meta = $ResourceRecord->getInfo();
        if (empty($ResourceRecord)) {
            $this->logger->log(75, 'ResourseManager', 'getResourse: can\'t get resourse record from database');
            return false;
        }
        if ($uid) {
            $this->uid = $uid;
        } else {
            $this->uid = $ResourceRecord->getUid();
            // $this->uid = $ResourceRecord->
        }
        /** @var $collection \RAFFloader\ResourceManagerBundle\Services\Storages\Interfaces\IResourceCollection */
        $collection = $this->collectionManager->getCollection($this->uid);
        if (!$collection) {
            print"\nResourceManager:can't get resource: can't get collection";
            $this->logger->log(75, 'ResourceManager', 'can\'t get resource: can\'t get collection', array('new collection uid' => $this->uid));
            return false;
        }
        try {
            $resource = $collection->getResource($id);
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
        if ($resource) {
            return $resource;
        } else {
            return false;
        }
    }

    public function parse($collection)
    {
        $this->parser->pars($collection);
    }

    public function removeResource($id)
    {
        $Repository = $this->EntityManager->getRepository('ResourceManagerBundle:Resources');
        /** @var $ResourceRecord Resources */
        $ResourceRecord = $Repository->findOneBy(array('id' => $id));
        if (is_null($ResourceRecord)) {
            $this->logger->log(75, 'ResourceManager', 'can\'t remove resource : can\'t find such resource in the base', array('id' => $id));
            return false;
        }
        $uid = $ResourceRecord->getUid();
        /** @var $collection \RAFFloader\ResourceManagerBundle\Services\Storages\Interfaces\IResourceCollection */
        $collection = $this->collectionManager->getCollection($uid);
        if ($collection == false) {
            /** @var  \RAFFloader\ResourceManagerBundle\Services\Storages\LocalCollection $collection */
            $this->logger->log(75, 'ResourceManager', 'Trying to remove resource, can\'t find collection', array('new collection uid' => $uid));
            return false;
        }
        if ($collection->deleteResource($id) &&
            $this->deleteRecordFromDatabase($id)
        ) {
            $this->logger->log(25, 'Resource Manager', 'resource deleted successfully', array('id' => $id, 'uid' => $uid));
            return true;
        } else {
            $this->logger->log(75, 'Resource Manager', 'error deleting resource', array('id' => $id, 'uid' => $uid));
            return false;
        }
    }

    function isValidName($str, $mkValidName = null)
    {
        $fullnameAr = explode('/', $str);
        $name = array_pop($fullnameAr);

        if (preg_match('/[^!?@#$%&. A-ZА-ЯЁ0-9]+/ui', $name)) {
            if ($mkValidName !== null) {
                $name = preg_replace('/[^_!?@#$%&. A-ZА-ЯЁ0-9]+/ui', '?', $name);
                $nameAr = explode('.', $name);
                if (count($nameAr) >= 2) {
                    $extension = array_pop($nameAr);
                    array_push($nameAr, uniqid());
                    array_push($nameAr, $extension);
                    $name = implode('.', $nameAr);
                } else {
                    $name .= '_uniq(' . uniqid() . ')_.';
                }
                $name = trim($name, ". ");
                $name = urldecode($name);
                $name = str_replace(' ', '_', $name);
            } else {
                return false;
            }
        } else {
            if ($mkValidName !== null) {
            } else {
                return true;
            }
        }
        array_push($fullnameAr, $name);
        $resultName = implode('/', $fullnameAr);
        return $resultName;
    }
}