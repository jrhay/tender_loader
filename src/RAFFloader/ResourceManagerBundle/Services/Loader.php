<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 14.08.13
 * Time: 12:35
 * To change this template use File | Settings | File Templates.
 */

namespace RAFFloader\ResourceManagerBundle\Services;

use Doctrine\ORM\EntityManager;
use RAFFloader\LogBundle\Services\specLogger;

class Loader
{
    /** @var  specLogger */
    private $logger;
    /** @var  ResourceManager */
    private $resourceManager;
    /** @var  \ProjectServiceContainer */
    private $serviceContainer;
    /** @var  EntityManager */
    private $entityManager;

    function __construct($serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->logger = $this->serviceContainer->get('logger');
        $this->resourceManager = $this->serviceContainer->get('resource_manager');
        $this->entityManager = $this->serviceContainer->get('doctrine.orm.entity_manager');
    }

    public function load($ids)
    {
        foreach($ids as $id)
        {
        $exId = $id;
        $id = (int)$id;
        if (!$id or !is_int($id)) {
            $this->logger->log(75, 'Loader', "can't load resource : bad id was given : $exId");
            continue;
        }
        print "\n starting load resources....";
        $this->logger->log(50, 'Loader', 'starting load resources');
        $a = true;
        while ($a) {
            $a = false;
            $ldRs = $this->resourceManager->loadMarkedRes($id);
            if (!$ldRs) {
                $this->logger->log(75, 'Loader:load', 'can\'t load resource, rmanager->loadMarkedRes() return false');
                $dt = new \DateTime();
                $ptRs = $this->resourceManager->putRecordToDatabase(false, false, false, false, 4, false, $dt, $id);
                if (!$ptRs) {
                    $this->logger->log(75, 'Loader:load', 'can\'t set status 4 for resource $id');
                    continue;
                }
                continue;
            } else {
                print "\n resource \$id= $id is downloaded";
            }
        }
        continue;
    }
        return true;
    }
}