<?php

namespace RAFFloader\ResourceManagerBundle\Tests\RManager;

use RAFFloader\ResourceManagerBundle\services\ResourceManager;
use RAFFloader\AManagerBundle\services\AManager;

class RManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider provideCreate
     */
    function testcreate($resource_list)
    {
        $AManager = new AManager;
        $RManager = new ResourceManager($AManager);
        $RManager->create($resource_list);

    }

    function provideCreate()
    {

        return array(
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6254030', 'id' => '6254030', 'platform' => 'ZakupkiGovRu', 'file_extension' => 'html', 'name' => 'Publication')),
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6254031', 'id' => '6254031', 'platform' => 'ZakupkiGovRu', 'file_extension' => 'html', 'name' => 'Publication')),
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6254032', 'id' => '6254032', 'platform' => 'ZakupkiGovRu', 'file_extension' => 'html', 'name' => 'Publication')),
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6254034', 'id' => '6254034', 'platform' => 'ZakupkiGovRu', 'file_extension' => 'html', 'name' => 'Publication')),
        );
    }

}