<?php

namespace RAFFloader\ZakupkiGovRuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ZakupkiGovRuBundle:Default:index.html.twig', array('name' => $name));
    }
}
