<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 09.08.13
 * Time: 13:47
 * To change this template use File | Settings | File Templates.
 */
/** This class can download page with last added tenders and look for new tenders, then push the newest id to our Base */
namespace RAFFloader\ZakupkiGovRuBundle\Services\Checker;
use RAFFloader\PublicationManagerBundle\Entity\Publications;
use Symfony\Component\Validator\Constraints\DateTime;

class Checker
{
    /** @var \Symfony\Component\DependencyInjection\Tests\ProjectServiceContainer */
    private $service_container;
    /** @var \RAFFloader\LogBundle\Services\specLogger */
    private $logger;
    /** @var \Doctrine\ORM\EntityManager */
    private $entityManager;
    /** @var \RAFFloader\PublicationManagerBundle\Services\PublicationManageri */
    private $publicationManager;
    private $emptySearchRequest_94 = 'http://zakupki.gov.ru/epz/main/public/extendedsearch/search.html?sortDirection=false&sortBy=PUBLISH_DATE&recordsPerPage=_50&pageNo=1&searchPlace=FZ_94&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&okdpWithSubElements=false&orderStages=AF%2CCA&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&contractPriceCurrencyId=-1&okvedWithSubElements=false&changeParameters=true';
    private $emptySearchRequest_223 = 'http://zakupki.gov.ru/epz/main/public/extendedsearch/search.html?sortDirection=false&sortBy=PUBLISH_DATE&recordsPerPage=_50&pageNo=1&searchPlace=FZ_223&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&okdpWithSubElements=false&orderStages=AF%2CCA&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&contractPriceCurrencyId=-1&okvedWithSubElements=false&changeParameters=true';
    private $lastBaseId94;
    private $lastBaseId223;
    private $lastUrlId94;
    private $lastUrlId223;
    private $lastLoadedId94;
    private $lastLoadedId223;
    private $theOldestLoaded94;
    private $theOldestLoaded223;
    private $turnLen = 40000000;
    private $stepLen = 1000;
    private $backlogLen = 100;
    private $backlogLen94 = null;
    private $backlogLen223 = null;
    private $addOld = 1;
    private $platform = 'zakupki_gov_ru';

    function __construct($service_container, $em)
    {
        $this->service_container = $service_container;
        $this->logger = $this->service_container->get('logger');
        $this->entityManager = $em;
        $this->publicationManager = $this->service_container->get('publication_manager');
    }

    public function check()
    {
        $res = $this->getValues();
        if (!$res) {
            $this->logger->log(75, 'Checker', 'ERROR:Can\'t check for new tenders');
            return false;
        }
        $resP = $this->updatePool();
        if (!$resP) {
            $this->logger->log(75, 'Checker', 'ERROR:Can\'t pool new uids to the Publication turn');
            return false;
        }
        $this->logger->log(75, 'Checker', 'exiting...');
        return true;
    }

    private function updatePool($form = false)
    {
        if (!$form) {
            $this->updatePool(94);
            $this->updatePool(223);
            return true;
        }
        $form === 94 ? $lastEtp = $this->lastUrlId94 : $lastEtp = $this->lastUrlId223;
        $form === 94 ? $lastBase = $this->lastBaseId94 : $lastBase = $this->lastBaseId223;
        $form === 94 ? $lastLoaded = $this->lastLoadedId94 : $lastLoaded = $this->lastLoadedId223;
        $form === 94 ? $theOldestLoaded = $this->theOldestLoaded94 : $theOldestLoaded = $this->theOldestLoaded223;

        if(!$theOldestLoaded){
            $theOldestLoaded  = $this->getLastId($form,3);
            if(!$theOldestLoaded and $lastBase){
                $this->logger->log(50,'Checker',"can't get last uid from the base (with any status), set lastLoaded = lastBase (line 104)");
                $theOldestLoaded = $lastBase;
            } elseif(!$theOldestLoaded and !$lastBase){
                $this->logger->log(50,'Checker',"can't get last uid from the base (with any status), set lastLoaded = lastEtp and lastBase = lastEtp (line 104)");
                $theOldestLoaded = $lastEtp;
                $lastBase = $lastEtp;
            }
        }

        if(!$lastBase){
            $lastBase = $lastEtp;
        }

        if ($lastBase - $lastLoaded > $this->turnLen) {
            $this->logger->log(50, 'Checker', " There are more then 100000 lots in our  turn,no need to pull $form FZ lots");
            return true;
        }

        if ($lastEtp - $lastBase > $this->backlogLen) {
            $way = 'forward';
           $this->logger->log(50,'Checker',"is going to add new uids to $form FZ turn ");
        } elseif($lastEtp - $lastBase < 0){
            $this->logger->log(75,'Checker',"ERROR : the newest $form FZ uid on ETP is less then our last uid");
            return false;
        }
        else
         {
            $way = 'recede';
            $this->logger->log(50,'Checker',"is going to add old uids to $form FZ turn ");
        }

        /** @var $repository \Doctrine\ORM\EntityRepository */
        // $repository = $this->entityManager->getRepository("PublicationManagerBundle:Publications");
        /** @var $publications  Publications[] */
        /** @var $now \Datetime $now */
        $now = new \DateTime();

       $way === 'forward'? $uid = $lastBase + 1 : $uid = $theOldestLoaded -1;
        $last_uid = $uid;
        $way === 'forward' ? $this->stepLen = $lastEtp - $lastBase : $this->stepLen = $this ->addOld;
        $start = microtime(true);
        //$this->stepLen = 100;
        $count = 0;
        $this->entityManager->beginTransaction();
        while (abs($uid - $last_uid) <= $this->stepLen) {
            $count++;
            $publication = new Publications();
            $this->entityManager->persist($publication);
            $publication->setExtra($form);
            $publication->setFindDateTime($now);
            $publication->setPlatform($this->platform);
            $publication->setUid($uid);
            $publication->setUrl('');
            $publication->setStatus(3);
            $way === 'forward' ? $uid++ : $uid--;
        }

        $this->entityManager->flush();
         $this->entityManager->commit();
        $stop = microtime(true);
        $time = $stop-$start;
        print "\n count = $count  time = $time \n";

        $this->logger->log(50, "Checker", "Publication  $form FZ turn added $this->stepLen ids");
        $this->logger->log(50, "Checker", "done");
        return true;

    }

    private
    function getValues($form = false)
    {
        if (!$form) {
            $this->getValues(94);
            $this->getValues(223);
            return true;
        }
        print "\n starting getValues() \n";

        // получаем последний uid из базы ( необработанный)
        $lastIdFromBase = $this->getLastId($form);
        if (!$lastIdFromBase) {
            $this->logger->log(75, "Checker : check $form FZ", " ERROR:Checker: Last Id form base is false line 146");
            $lastIdFromBase = 0;
        }
        ($form === 94)
            ? $this->lastBaseId94 = $lastIdFromBase
            : $this->lastBaseId223 = $lastIdFromBase;

        $theOldestLoaded = $this->getLastId($form,2);
        if (!$theOldestLoaded) {
            $this->logger->log(75, "Checker : check $form FZ", " ERROR:Checker: Last Id form base is false line 146");
            $theOldestLoaded = 0;
        }
        ($form === 94)
            ? $this->theOldestLoaded94 = $theOldestLoaded
            : $this->theOldestLoaded223 = $theOldestLoaded;


        // получаем последний uid  с ООС
        $form === 94 ? $emptySearchRequest = $this->emptySearchRequest_94 : $emptySearchRequest = $this->emptySearchRequest_223;
        $answer = $this->getHTTPContent($emptySearchRequest);
        if (!$answer) {
            $this->logger->log(75, 'Checker', "ERROR can't check  new $form FZ: bad answer");
            return false;
        }
        $searcher = $this->service_container->get('id.searcher');
        $lastIdFromUrl = $searcher->getLastId($answer);
        if (!$lastIdFromUrl) {
            $this->logger->log(75, "Checker check $form FZ", 'Can\'t get last id from url');
            return false;
        }
        $form === 94 ? $this->lastUrlId94 = $lastIdFromUrl : $this->lastUrlId223 = $lastIdFromUrl;
        $this->logger->log(50, 'Checker', "new $form FZ tenders are aviable", array('last uid from base' => $lastIdFromBase, 'last uid from URL' => $lastIdFromUrl));

        // получаем последний обработанный(завершенный) uid из базы
        $lastLoadedId = $this->getLastId($form, true);
        if (!$lastLoadedId) {
            $this->logger->log(75, 'Checker', "Last loaded Uid from base is zero: no loaded Pubs or Can't get last loaded id from  DB for $form FZ");
            $lastLoadedId = 0;
        }

        if ($form === 94) {
            $this->lastLoadedId94 = $lastLoadedId;
            $backlog = $this->backlogLen94 = $lastIdFromUrl - $lastLoadedId;
        } else {
            $this->lastLoadedId223 = $lastLoadedId;
            $backlog = $this->backlogLen223 = $lastIdFromUrl - $lastLoadedId;
        }

        $this->logger->log(50, 'Checker', "values : DataBase: the oldest - $theOldestLoaded last - $lastIdFromBase loaded - $lastLoadedId  ZGR: $lastIdFromUrl backlog: $backlog .");
        return true;

    }


    private
    function getLastId($form, $complete = false)
    {
        if ($complete) {
            if ($complete == 1) {
                $dql = "Select max(p.uid) from PublicationManagerBundle:Publications p where p.extra = $form and p.status IN(1,4,0) ";
            } elseif ($complete === 2) {
                $dql = "Select min(p.uid) from PublicationManagerBundle:Publications p where p.extra = $form and p.status IN(1,4,0) ";
            } elseif($complete ===3){
                $dql = "Select min(p.uid) from PublicationManagerBundle:Publications p where p.extra =$form and p.status IN(3,1,0)";
            } else {
                $this->logger - log(75, 'Checker', 'Bad arguments are given to getLastId');
                return false;
            }
        } else {
            $dql = "Select max(p.uid) from PublicationManagerBundle:Publications p where p.extra = $form ";
        }
        if ($form == 94) {
            $last_id_94 = $this->entityManager->createQuery($dql)->getResult();
            if (!empty($last_id_94[0][1])) {
                $last_id_94 = $last_id_94[0][1];
            } else {
                $this->logger->log(75, 'Checker', "\nCan't t get last 94FZ id from the Base, set \$last_id_94 = 0\n");
                $last_id_94 = 0;
                $r=0;
            }
            if ($last_id_94 !== false) {
                if (!$complete) {
                    $this->lastBaseId94 = $last_id_94;
                }
                return $last_id_94;
            } else {
                $this->logger->log(75, 'Checker', 'ERROR : can\'t get lastId :can\'t get result  with querry', array('query' => $dql));
                return false;
            }

        } else if ($form == 223) {
            $last_id_223 = $this->entityManager->createQuery($dql)->getResult();
            if (!empty($last_id_223[0][1])) {
                $last_id_223 = $last_id_223[0][1];
            } else {

                $this->logger->log(75, 'Checker', "\nCan't t get last 223FZ id from the Base, set \$last_id_223 = 0 \n");
                $last_id_223 = 0;
                //return false;
            }
            if ($last_id_223 !== false) {
                if (!$complete) {
                    $this->lastBaseId223 = $last_id_223;
                }
                return $last_id_223;
            } else {
                $this->logger->log(75, 'Checker', 'ERROR : can\'t get lastId :can\'t get result  with querry', array('query' => $dql));
                return false;
            }
        } else {
            $this->logger->log(75, 'Checker', 'ERROR NO $form in getLastID in checker', array('form' => $form));
            return false;
        }

    }


    private
    function getHTTPContent($url, $opts = array())
    {

        /** @var $rmanager \RAFFloader\ResourceManagerBundle\Services\ResourceManager */
        $rmanager = $this->service_container->get('resource_manager');
        /** @var $resource \RAFFloader\ResourceManagerBundle\Services\Resources\DefaultResource */
        $resource_info = array(
            'platform' => 'zakupki_gov_ru',
            'uid' => 1,
            'url' => $url,
            'name' => "checker_temp_resource_" . uniqid(),
            'resource_type' => 'html',
        );

        $id = $rmanager->create($resource_info);
        if (!$id) {
            $Content = false;
        } else {
            $resource = $rmanager->getResource($id);
            if (!$resource) {
                print "\n Checker:getHTTPContent:getResource returned false";
            }
            $Content = $resource->getRaw();
            $rmanager->removeResource($id);
        }


        if ($Content) {
            return $Content;
        } else {
            $this->logger->log(75, 'Checker', 'can \'t get answer from temp resource', array('url' => $url, 'resource_info' => $resource_info));
            return false;
        }
    }
}
