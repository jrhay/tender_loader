<?php
namespace RAFFloader\ZakupkiGovRuBundle\services;

class LookForNewTender
{
    /** @var \RAFFloader\PublicationManagerBundle\Services\PublicationManager */
    private $publication_manager;
    /** @var \Symfony\Component\DependencyInjection\Container */
    private $service_container;
    /** @var \Doctrine\ORM\EntityManager */
    private $entityManager;
    private $containerID = 'zakupki_gov_ru';
    /** @var \RAFFloader\DataContainerBundle\Services\DataContainerManager */
    private $data_container_manager;
    private $resource_manager;
    private $lastId_94;
    private $lastId_223;
    private $currentId;
    /** @var \RAFFloader\LogBundle\Services\specLogger */
    private $logger;
    private $base_url_94 = 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=';
    private $base_url_223 = 'http://zakupki.gov.ru/223/purchase/public/notification/print-form/show.html?noticeId=';
    private $platform = 'zakupki_gov_ru';
    private $record_94 = array(
        'url',
        'uid',
        'platform',
        'find_datetime',
        'extra'
    );
    private $record_223 = array(
        'url',
        'uid',
        'platform',
        'find_datetime',
        'extra'
    );

    public function __construct($publication_manager, $data_container_manager, $service_container)
    {
        $this->publication_manager = $publication_manager;
        $this->data_container_manager = $data_container_manager;
        $this->service_container = $service_container;
        $this->logger = $this->service_container->get('logger');
        $this->entityManager = $this->service_container->get('doctrine.orm.entity_manager');
        $this->resource_manager = $this->service_container->get('resource_manager');
    }

    public function check_new($new_uids)
    {
        foreach ($new_uids as $new_uid) {
      //  file_put_contents("./ids",$new_uid."\n",FILE_APPEND);
         //   $list = file("./ids");
//            foreach($list as $str){
//                $intList[] = (int) $str;
//            }
            //array_multisort($intList);
        //      array_multisort($list);
//            foreach ($intList as $i) {
//                $str2[] = (string) $i;
//            }

          //  file_put_contents("./ids", $list);

            $exId = $new_uid;
            $new_uid = (int)$new_uid;
            if (!$new_uid or !is_int($new_uid)) {
                $this->logger->log(75, 'LoohForNewTender', "Bad argument  was given", array('new_uid' => $exId,
                    'uid' => $new_uid));
                continue;
            }

            $platform = $this->platform;
            $startTime = microtime(true);
            $a = true;
            while ($a) {
                $a = false;
                $flag = $this->getFlag($new_uid);
                if ($flag !== '94' and $flag !== '223') {
                    $this->logger->log(75, 'LoohForNewTender', "Bad flag was given", array('flag' => $flag,
                        'uid' => $new_uid));
                    continue;
                }

                //Для публикаций по форме 223 ФЗ

                if ($flag === '223') {

                    $new_uid_223 = $new_uid;

                    $this->currentId = $new_uid_223;
                    $url_223 = $this->base_url_223 . $new_uid_223;
                    $find_datetime_223 = new \DateTime('now');

                    $this->logger->log(50, 'check for new tenders (ZakupkiGovRu)', "Lot with uid = $new_uid_223  processing ");
                    $this->record_223['uid'] = $new_uid_223;
                    $this->record_223['url'] = $url_223;
                    $this->record_223['platform'] = $platform;
                    $this->record_223['find_datetime'] = $find_datetime_223;
                    $this->record_223['extra'] = '223';

                    $downloader_metod = $this->getDownloader();
                    if ($downloader_metod === false) {
                        $this->logger->log(75, 'Look for new tender', ' ERROR: can\'t get downloader fro 223FZ (Look for new tender str:99)');
                        continue;
                    }
                    $this->publication_manager->FoundNewTender($this->record_223, $downloader_metod);
                    $stRes = $this->publication_manager->setStatus($this->platform, $new_uid_223, '223', 1);
                    if (!$stRes) {
                        $this->logger->log(75, 'LookFor New tender', 'ERROR: can\'t set new status \'1\' for 223 FZ tender line 107');
                    }
                    $this->logger->log(50, 'check for new tenders (ZakupkiGovRu)', "lot with uid = $new_uid_223 is send to download turn", array('new tender' => $this->record_223,
                        'last id_223 ' => $this->lastId_223));
                }

                //для публикаций по форме 94ФЗ
                if ($flag === '94') {

                    $new_uid_94 = $new_uid;

                    $this->currentId = $new_uid_94;
                    $url_94 = $this->base_url_94 . $new_uid_94;
                    $find_datetime_94 = new \DateTime('now');

                    $this->logger->log(50, 'check for new tenders (ZakupkiGovRu)', "Lot with uid = $new_uid_94  processing ");

                    $this->record_94['uid'] = $new_uid_94;
                    $this->record_94['url'] = $url_94;
                    $this->record_94['platform'] = $platform;
                    $this->record_94['find_datetime'] = $find_datetime_94;
                    $this->record_94['extra'] = '94';

                    $downloader_metod = $this->getDownloader();
                    if ($downloader_metod === false) {
                        $this->logger->log(75, 'Look for new tender', ' ERROR: can\'t get downloader fro 223FZ (Look for new tender str:99)');
                        continue;
                    }
                    $this->publication_manager->FoundNewTender($this->record_94, $downloader_metod);
                    $stRes = $this->publication_manager->setStatus($this->platform, $new_uid_94, '94', 1);
                    if (!$stRes) {
                        $this->logger->log(75, 'LookFor New tender', 'ERROR: can\'t set new status \'1\' for 94FZ tender line 156');
                    }
                    $this->logger->log(50, 'check for new tenders (ZakupkiGovRu)', "lot with uid $new_uid_94 is send to download turn", array('new tender' => $this->record_94,
                        'last id_94 ' => $this->lastId_94));
                }
            }

            $endTime = microtime(true);
            $timeInterval = $endTime - $startTime;
            echo "\n Время поиска :" . $timeInterval . "\n";
        }
        return true;
    }

    /** Check last unused id, be careful : this function can change status in Publication DataBase, it can be called only once in public
     * finction check_new()
     */

    function getFlag($uid)
    {
        $dql = "SELECT min(p.id) from PublicationManagerBundle:Publications p where p.uid = $uid";
        $id = $this->entityManager->createQuery($dql)->getResult();
        if (empty($id[0][1])) {
            $this->logger->log(75, 'LookForNewTenders:Checker', 'can,t get id from Publication table (getFlag)');
            return false;
        }
        $flag = $this->doGetFlag($id[0][1]);
        return $flag;
    }

    function doGetFlag($id)
    {
        $dql = "SELECT p.extra from PublicationManagerBundle:Publications p where p.id = $id";
        $extra = $this->entityManager->createQuery($dql)->getResult();
        if (empty($extra[0]['extra'])) {
            $this->logger->log(75, 'Look for new tender : getFlag', "can't get flag (extra) for $id");
            return false;
        }
        $flag = $extra[0]['extra'];
        return $flag;
    }

    function getDownloader()
    {
        $service_container = $this->service_container;
        $downloader = $service_container->get('zakupki_gov_ru.downloader');
        if (is_null($downloader)) {
            $this->logger->log(75, 'check for new tenders (ZakupkiGovRu)', 'ERROR can\'t get downloader', array('94' => $this->record_94,
                '223' => $this->record_223, 'last_id' => $this->currentId));
            return false;
        }
        return array($downloader, 'load');
    }


}