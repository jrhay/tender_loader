<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 13.08.13
 * Time: 18:42
 * To change this template use File | Settings | File Templates.
 */

namespace RAFFloader\ZakupkiGovRuBundle\Services;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use RAFFloader\LogBundle\Services\specLogger;
use RAFFloader\ResourceManagerBundle\Entity\Resources;
use RAFFloader\ResourceManagerBundle\Services\ResourceManager;
use RAFFloader\ZakupkiGovRuBundle\Services\Searchers\DocSearcher94;
use RAFFloader\ZakupkiGovRuBundle\Services\Searchers\DocSearcher223;

class Parse
{
    private $resource_info = array(
        'platform' => 'zakupki_gov_ru',
        'uid' => '',
        'url' => '',
        'name' => '',
        'resource_type' => '',
    );
    private $record = array(
        'uid' => '',
        'url',
        'platform',
        'finddate',
    );
    /** @var  \ProjectServiceContainer */
    private $service_container;
    /** @var specLogger */
    private $logger;
    /** @var EntityManager */
    private $entityManager;
    private $fzFlag;

    function __construct($service_container)
    {
        $this->service_container = $service_container;
        $this->logger = $this->service_container->get('logger');
        $this->entityManager = $this->service_container->get('doctrine.orm.entity_manager');
    }

    public function parse($ids)
    {
        foreach ($ids as $id) {
            $exId = $id;
            $id = (int)$id;
            if (!$id or !is_int($id)) {
                print "\nERROR: Bad argument given to parse : $exId";
                continue;
            }

            $dql = "select r from ResourceManagerBundle:Resources r where r.id = $id ";
            $a = true;
            while ($a) {
                $a = false;
                $docListResourceAr = $this->entityManager->createQuery($dql)->getResult();
                /**@var $docListResourceAr Resources[] */
                if (empty($docListResourceAr[0]) or !is_a($docListResourceAr[0], "\\RAFFloader\\ResourceManagerBundle\\Entity\\Resources")) {
                    $this->logger->log(75, "Parser: can't get record with $id = $id", array('record' => $this->record));
                    $this->logger->log(50, 'Parser', 'exiting parsing...');
                    continue;

                } else {
                    /** @var ResourceManager $rm */
                    $rm = $this->service_container->get('resource_manager');

                    $docListResourceId = $docListResourceAr[0]->getId();
                    $rm->putRecordToDatabase(false, false, false, false, 2, false, false, $docListResourceId);
                    $info = $docListResourceAr[0]->getInfo();
                    if (!empty($info)) {
                        $this->resource_info = $info;
                    } else {
                        $this->logger->log(75, 'Parser', 'Resource Info is empty, can\'t set FZflag', array('resource_info' => $this->resource_info));
                    }
                    if (!empty($info['url'])) {
                        $url = $info['url'];
                        if (strpos($url, '/223/') !== false) {
                            $this->fzFlag = 223;
                        } else {
                            $this->fzFlag = 94;
                        }
                    }
                }

                /** @var $rmanager \RAFFloader\ResourceManagerBundle\Services\ResourceManager */
                $rmanager = $this->service_container->get('resource_manager');
                /** @var $docListResource \RAFFloader\ResourceManagerBundle\Services\Resources\DefaultResource */
                $docListResource = $rmanager->getResource($docListResourceId);
                if ($docListResource !== false) {
                    $docListPage = $docListResource->getRaw();
                    $dlr_info = $docListResource->getInfo();
                    $uid = $dlr_info['uid'];
                    if (!$docListPage) {
                        print "\nParser Zakupki Gov Ru: can't get raw from doc List Resource\n";
                        $this->logger->log(75, 'Parser Zakupki Gov Ru: can\'t get raw from doc List Resource', array('record' => $this->record));
                        continue;
                    }
                } else {
                    print "\nParser Zakupki Gov Ru: can't get doc List Resource\n";
                    $this->logger->log(75, 'Parser Zakupki Gov Ru', 'can\'t get doc List Resource', array('record' => $this->record));
                    continue;
                }
                $docResourceArray = $this->loadDocs($docListPage, $uid);
                if ($docResourceArray !== false) {
                    $this->logger->log(50, 'Parser ZakupkiGovRu', 'Docs successfully marked for download', array('resource list' => $docListResource));
                    $rm->putRecordToDatabase(false, false, false, false, -1, false, false, $docListResourceId);
                } else {
                    $rm->putRecordToDatabase(false, false, false, false, 4, false, false, $docListResourceId);
                    print "\nwarning: docs are not downloaded\n";
                    continue;
                }
            }
        }
        return true;
    }

    private function loadDocs($docListPage, $uid)
    {
        $logger = $this->service_container->get('logger');
        $uid = $this->resource_info['uid'];
        $searcher = null;
        $memory = memory_get_usage(true);
        print "\n mem usage before getting searcher : $memory";
        if ($this->fzFlag == '223') {
            /** @var $searcher DocSearcher223 */
            $searcher = $this->service_container->get('doc.searcher223');
        } else if ($this->fzFlag == '94') {
            /** @var $searcher DocSearcher94 */
            $searcher = $this->service_container->get('doc.searcher94');
        } else {
            echo "\nError: bad fzFlag (Parser ZakupkiGovRu)";
            // $this->logger(75, 'Parser', 'Error: bad fzFlag (Parser ZakupkiGovRu)', array('info' => $this->record, 'record' => $this->resource_info));
            return false;
        }
        print "\n Docs marked for load:\n";
        $docUrlArray = $searcher->searchDocs($docListPage, $uid);
        if (!empty($docUrlArray)) {
            $resourceArray = array();
            $logger->log(50, 'Parser (ZakupkiGovRu)', 'downloading docs processing...', array('resource info' => $this->resource_info));
            $answerArray = array();
            foreach ($docUrlArray as $name => $docUrl) {
                $this->resource_info['url'] = $docUrl;
                $this->resource_info['resource_type'] = 'http';
                $this->resource_info['name'] = $name;
                $this->resource_info['uid'] = $uid;
                /** @var ResourceManager $resourceManager */
                $resourceManager = $this->service_container->get('resource_manager');
                $answerArray[] = $resourceManager->markForLoad($this->resource_info);
            }
//            print "\n Docs marked for load:\n";
//            var_dump($docUrlArray);
            $logger->log(50, 'Parser (ZakupkiGovRu)', 'Docs marked to load', array('ocUrlArray' => $docUrlArray));
            return $answerArray;
        } else {
            $logger->log(75, 'Parser (ZakupkiGovRu)', 'Problem Publication detected: can\'t parse page with a list of documents', array('resource info' => $this->resource_info));
            return false;
        }
    }
}