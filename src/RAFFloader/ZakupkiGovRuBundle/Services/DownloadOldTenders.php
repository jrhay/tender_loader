<?php
namespace RAFFloader\ZakupkiGovRuBundle\services;

class DownloadOldTenders
{
    /** @var \RAFFloader\PublicationManagerBundle\Services\PublicationManager */
    private $publication_manager;
    /** @var \Symfony\Component\DependencyInjection\Container */
    private $service_container;
    private $containerID = 'zakupki_gov_ru';
    /** @var \RAFFloader\DataContainerBundle\Services\DataContainerManager */
    private $data_container_manager;
    private $lastId_94;
    private $lastId_223;
    /** @var \RAFFloader\LogBundle\Services\specLogger */
    private $logger;
    private $platform = 'zakupki_gov_ru';
    private $base_url_94 = 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=';
    private $base_url_223 = 'http://zakupki.gov.ru/223/purchase/public/notification/print-form/show.html?noticeId=';
    private $record_94 = array(
        'url',
        'uid',
        'platform',
        'find_datetime',
        'extra' => '94'
    );
    private $record_223 = array(
        'url',
        'uid',
        'platform',
        'find_datetime',
        'extra' => '223'
    );

    public function __construct($publication_manager, $data_container_manager, $service_container)
    {
        $this->publication_manager = $publication_manager;
        $this->data_container_manager = $data_container_manager;
        $this->service_container = $service_container;
        $this->logger = $this->service_container->get('logger');
    }

    public function download_old($value)
    {
        // $this->platform = uniqid();
        $max_all_count = $value;
        $max_all_count_94 = $value;
        $max_all_count_223 = $value;
        $max_last_count_94 = $value;
        $max_last_count_223 = $value;


        $platform = $this->platform;
        $count_94 = 0;
        $count_223 = 0;
        $count_all_94 = 0;
        $count_all_223 = 0;
        $startTime = microtime(true);
        do {
            //Для публикаций по форме 223 ФЗ
            if ($count_all_223 <= $max_all_count_223) {
                $count_all_223++;
                $new_id_223 = $this->getLastId(223);
                if (!$new_id_223) {
                    print "\nDownload old: getLastId(223) returned FALSE, go to 94 FZ\n";
                    $this->logger->log(75, 'Download old tenders', 'ERROR: loading 223: Download old: getLastId(223) returned FALSE, go to 94 FZ');
                    goto NEXT_STAGE;
                }
                $url_223 = $this->base_url_223 . $new_id_223;
                $find_datetime_223 = new \DateTime('now');
                print "\n checking lot with uid = $new_id_223\n";
                $answer_223 = $this->getHTTPContent($url_223);
                if ($answer_223 !== false) {

                    print "\n  Lot with uid = $new_id_223 is aviable , processing \n";
                    $this->record_223['uid'] = $new_id_223;
                    $this->record_223['url'] = $url_223;
                    $this->record_223['platform'] = $platform;
                    $this->record_223['find_datetime'] = $find_datetime_223;

                    $downloader_metod = $this->getDownloader();
                    if (!$downloader_metod) {
                        print "\nDown olad tenders: Can\'t get downloader 223FZ";
                        $this->logger->log(75, 'Download old tenders: downloading 223 FZ', 'Can\'t get downloader', array('uid' => $new_id_223));
                        goto NEXT_STAGE;
                    }
                    $this->publication_manager->FoundNewTender($this->record_223, $downloader_metod);
                    $stRes = $this->publication_manager->setStatus($this->platform, $new_id_223, '223', 1);
                    if (!$stRes) {
                        print "\n ERROR: can't set new status";
                        $this->logger->log(75, 'Download old Tenders', 'ERROR: can\'t set new status \'1\' for 223 FZ tender line 89', array('uid' => $new_id_223));
                    }
                    $count_223 = 0;

                    $logger = $this->service_container->get('logger');
                    $logger->log(50, 'check for new tenders (ZakupkiGovRu)', 'checking for new _223FZ tender is completed', array('new tender' => $this->record_223,
                        'last id_223 ' => $this->lastId_223));

                } else {
                    print "\n  Lot with uid = $new_id_223 is not aviable , going to next uid \n";
                    $this->PutDeadId($new_id_223);
                    $stRes = $this->publication_manager->setStatus($this->platform, $new_id_223, '223', 4);
                    if (!$stRes) {
                        print "\nDownload old tenders: ERROR: can\'t set  new status \'4\' for 223 FZ tender line 102";
                        $this->logger->log(75, 'Download old tenders', 'ERROR: can\'t set  new status \'4\' for 223 FZ tender line 103', array('uid' => $new_id_223));
                    }
                    $count_223++;
                }
                $this->SetLastId($new_id_223, 223);
            }
            NEXT_STAGE:
            //для публикаций по форме 94ФЗ
            if ($count_all_94 <= $max_all_count_94) {
                $count_all_94++;
                $new_id_94 = $this->getLastId(94);
                if (!$new_id_94) {
                    print "\nDownload old tenders: ERROR getLastId(94) returned FALSE \n";
                    $this->logger->log(75, 'Download old tenders', 'Download old tenders: ERROR getLastId(94) returned FALSE ');
                    goto NEXT;
                }
                $url_94 = $this->base_url_94 . $new_id_94;
                $find_datetime_94 = new \DateTime('now');
                print "\n checking lot with uid = $new_id_94\n";
                $answer_94 = $this->getHTTPContent($url_94);
                if ($answer_94 !== false) {

                    print "\n  Lot with uid = $new_id_94 is aviable , processing \n";
                    $this->record_94['uid'] = $new_id_94;
                    $this->record_94['url'] = $url_94;
                    $this->record_94['platform'] = $platform;
                    $this->record_94['find_datetime'] = $find_datetime_94;

                    $downloader_metod = $this->getDownloader();
                    if (!$downloader_metod) {
                        print "\nDown olad tenders: Can't get downloader 94FZ";
                        $this->logger->log(75, 'Download old tenders: downloading 94 FZ', 'Can\'t get downloader', array('uid' => $new_id_94));
                        goto NEXT_STAGE;
                    }
                    $this->publication_manager->FoundNewTender($this->record_94, $downloader_metod);
                    $stRes = $this->publication_manager->setStatus($this->platform, $new_id_94, '94', 1);
                    if (!$stRes) {
                        print "\nDownload old tenders: ERROR: can't set  new status '4' for 94 FZ tender line 102";
                        $this->logger->log(75, 'Download old tenders', 'ERROR: can\'t set  new status \'4\' for 94 FZ tender line 141', array('uid' => $new_id_94));
                    }
                    $count_94 = 0;
                    $this->logger->log(50, 'check for new tenders (ZakupkiGovRu)', 'checking for new _94FZ tender is completed', array('new tender' => $this->record_94,
                        'last id_94 ' => $this->lastId_94));

                } else {
                    print "\n  Lot with uid = $new_id_94 is not aviable , going to next uid \n";
                    $this->PutDeadId($new_id_94);
                    $stRes = $this->publication_manager->setStatus($this->platform, $new_id_94, '223', 4);
                    if (!$stRes) {
                        print "\nDownload old tenders: ERROR: can't set  new status '4' for 94 FZ tender line 152";
                        $this->logger->log(75, 'Download old tenders', 'ERROR: can\'t set  new status \'4\' for 94 FZ tender line 153', array('uid' => $new_id_94));
                    }

                }
                $this->SetLastId($new_id_94, 94);
            }
            NEXT:
        } while ((($count_223 <= $max_last_count_223)or($count_94 <= $max_last_count_94)) and ($count_all_94 + $count_all_223) <= $max_all_count
            and ($count_all_223 <= $max_all_count_223 or $count_all_94 <= $max_all_count_94));

        $endTime = microtime(true);
        $timeInterval = $endTime - $startTime;
        echo "\n Время поиска :" . $timeInterval . "\n";
        return true;
    }

    private function getLastId($form)
    {
        if ($form == 94) {
            /** @DataContainer DataContainer */
            $DataContainer = $this->data_container_manager->getDataContainer($this->containerID);
            $last_id_94 = $DataContainer->get('old_last_id_94', 6482706);
            do {
                $status = $this->publication_manager->checkStatus($this->platform, $last_id_94);
                if ($status === false) {
                    $stRes = $this->publication_manager->setStatus($this->platform, $last_id_94, '94', 2);
                    if (!$stRes) {
                        print "\nDownload old tenders: getLastId: ERROR: can't set  new status '2' for 94 FZ tender line 180";
                        $this->logger->log(75, 'Download old tenders:getLastId', 'ERROR: can\'t set  new status \'2\' for 94 FZ tender line 141', array('uid' => $last_id_94));
                    }
                    break;
                } else {
                    ++$last_id_94;
                }
            } while (true);
            $this->lastId_94 = $last_id_94;
            return $last_id_94;
        } else if ($form == 223) {
            /** @DataContainer DataContainer */
            $DataContainer = $this->data_container_manager->getDataContainer($this->containerID);
            $last_id_223 = $DataContainer->get('old_last_id_223', 420934);
            do {
                $status = $this->publication_manager->checkStatus($this->platform, $last_id_223);
                if ($status === false) {
                    $stRes = $this->publication_manager->setStatus($this->platform, $last_id_223, '223', 2);
                    if (!$stRes) {
                        print "\nDownload old tenders: getLastId: ERROR: can't set  new status '2' for 223 FZ tender line 199";
                        $this->logger->log(75, 'Download old tenders:getLastId', 'ERROR: can\'t set  new status \'2\' for 223 FZ tender line 200', array('uid' => $last_id_223));
                    }
                    break;
                } else {
                    ++$last_id_223;
                }
            } while (true);
            $this->lastId_223 = $last_id_223;
            return $last_id_223;
        } else {

            $this->logger->log(75, 'download old tenders (ZakupkiGovRu)', 'ERROR NO $form in getLastID in LookForNewTender', array('94' => $this->record_94,
                '223' => $this->record_223, 'last_id_94' => $this->lastId_94, 'last_id_223' => $this->lastId_223));
            return false;
        }

    }

    private function SetLastId($new_id, $form)
    {
        if ($form == 94) {
            /** @DataContainer DataContainer */
            $DataContainer = $this->data_container_manager->getDataContainer($this->containerID);
            $DataContainer->set('old_last_id_94', $new_id);
        } else if ($form == 223) {
            /** @DataContainer DataContainer */
            $DataContainer = $this->data_container_manager->getDataContainer($this->containerID);
            $DataContainer->set('old_last_id_223', $new_id);
        }

    }

    function PutDeadId($Dead_Id)
    {
        /** @DataContainer DataContainer */
        $DataContainer = $this->data_container_manager->getDataContainer($this->containerID);
        $deadIds = $DataContainer->get('old_dead_id');
        if (!is_array($deadIds)) {
            $deadIds = array();
        }
        $deadIds[] = $Dead_Id;
        $DataContainer->set('old_dead_id', $deadIds);
    }

    function getHTTPContent($url, $opts = array())
    {
        /** @var $rmanager \RAFFloader\ResourceManagerBundle\Services\ResourceManager */
        $rmanager = $this->service_container->get('resource_manager');
        /** @var $resource \RAFFloader\ResourceManagerBundle\Services\Resources\DefaultResource */
        $resource_info = array(
            'platform' => 'zakupki_gov_ru',
            'uid' => '',
            'url' => $url,
            'name' => "temp_resource_" . uniqid(),
            'resource_type' => 'html',
        );

        $id = $rmanager->create($resource_info);
        if ($id !== false) {
            $resource = $rmanager->getResource($id);
        } else {
            return false;
        }
        $Content = $resource->getRaw();
        $rmanager->removeResource($resource->getId());

        if ($Content) {
            return $Content;
        } else {
            // $this->logger->log(75, 'Download old Tenders', 'can \'t get answer from temp resource', array('url' => $url, 'resource id' => $resource->getId()));
            return false;
        }
    }

    function getDownloader()
    {
        $service_container = $this->service_container;
        $downloader = $service_container->get('zakupki_gov_ru.downloader');
        if (is_null($downloader)) {
            $this->logger->log(75, 'download all tenders (ZakupkiGovRu)', 'ERROR can\'t get downloader', array('94' => $this->record_94,
                '223' => $this->record_223, 'last_id_94' => $this->lastId_94, 'last_id_223' => $this->lastId_223));
            return false;
        }

        return array($downloader, 'load');
    }

}