<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 24.07.13
 * Time: 17:32
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ZakupkiGovRuBundle\Services\Searchers;

class DocSearcher223
{
    private $uid;
    private $content;
    private $baseDocsPublicationUrl = 'http://zakupki.gov.ru/223/purchase/public/purchase/info/documents.html?noticeId=';
    private $baseDocUrl = 'http://zakupki.gov.ru';
    private $headers;
    private $service_container;
    private $logger;

    public function searchDocs($content, $uid)
    {
        $this->content = $content;
        $this->uid = $uid;

        echo "\n\n  Publication $this->baseDocsPublicationUrl" . $this->uid . "\n";

        $idDocArray = array();
        $urlDocArray = array();
        $countAllDocs = 0;

        $resultUrlArray = array();
        $answer = $this->getContent();

        libxml_use_internal_errors(true);
        $domDoc = new \DOMDocument('1.0', 'UTF-8');
        $domDoc->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . $answer);
        libxml_use_internal_errors(false);

        $xpath = new \DOMXPath($domDoc);

        // получаем центральный блок со всеми доками
        $pattern = "//*[@id='tabtable']";
        $centralBlock = $xpath->query($pattern);
        // поучаем блоки
        if ($centralBlock->length) {
            $pattern = "div[@class='tabpad']/table[@class='documentTablet maintable3']";
            $blocks = $xpath->query($pattern, $centralBlock->Item(0));

            $pattern = 'div[@class=\'tabpad\']/table[@class=\'documentTablet maintable3\']/@id';
            $ids = $xpath->query($pattern, $centralBlock->Item(0));

            for ($i = 0; $i < $blocks->length; $i++) {
                $pattern = 'tbody/tr/td[2]/a/@href';
                $hrefs = $xpath->query($pattern, $blocks->item($i));
                if ($hrefs->length) {
                    for ($j = 0; $j < $hrefs->length; $j++) {
                        if ($ids->length > $i) {
                            $id = $ids->item($i)->nodeValue;
                        } else {
                            $id = uniqid();
                        }
                        $name = $id . '/' . $id . '_uniqid(' . uniqid() . ')_';
                        $url = $this->baseDocUrl . $hrefs->item($j)->nodeValue;
                        $resultUrlArray[$name] = $url;
                    }
                }
            }
        } else {
            $this->logger->log('75', 'DocSearcher223', 'Can\'t get central block from $content', array('$content' => $this->content, 'uid' => $this->uid, 'url' => $this->baseDocsPublicationUrl . $this->uid));
        }
        echo" \n Result url array (DocSearcher223): \n";
        var_dump($resultUrlArray);
        return $resultUrlArray;
    }

    public function __construct($serv_cont)
    {
        $this->service_container = $serv_cont;
        $this->logger = $this->service_container->get('logger');
    }

    public function getContent()
    {
        return $this->content;
    }

    public function saveHeader($curl, $headers)
    {
        $this->headers[] = $headers;
        return strlen($headers);
    }

}
