<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 25.07.13
 * Time: 18:21
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ZakupkiGovRuBundle\Services\Searchers;

class IdSearcher
{
    private $answer;
    private $service_container;
    private $logger;

    function __construct($service_container)
    {
        $this->service_container = $service_container;
        $this->logger = $this->service_container->get('logger');
    }

    public function getLastId($answer)
    {
        $this->answer = $answer;
        if (empty($this->answer)) {
            $this->logger->log('75', 'IdSearcher', 'IDSearcher->getLastId was given empty $answer', array('$answer' => $this->answer));
            return false;
        }


        libxml_use_internal_errors(true);
        $domDoc = new \DOMDocument('1.0', 'UTF-8');
        $domDoc->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . $this->answer);
        libxml_use_internal_errors(false);

        $xpath = new \DOMXPath($domDoc);

        // получаем центральный блок c лотами
        $pattern = "//*[@id=\"exceedSphinxPageSizeDiv\"]";
        $centralBlock = $xpath->query($pattern);
        if ($centralBlock->length) {
            $pattern = 'div[@class ="registerBox"][1]/table/tr[1]/td[2]/dl/dt/a/@href';
            $idText = $xpath->query($pattern, $centralBlock->item(0));
            if ($idText->length) {
                $ids_94 = array();
                $ids_223 = array();
                preg_match_all("/notificationId=(\\d*)/", $idText->item(0)->nodeValue, $ids_94);
                preg_match_all("/noticeId=(\\d*)/", $idText->item(0)->nodeValue, $ids_223);
                if (!empty($ids_94[1][0])) {
                    $lastId = $ids_94[1][0];
                    echo "The newest 94FZ id,found on zakupki.gov.ru is " . $lastId . "\n";
                    $this->logger->log(50, 'IdSearcher', "The newest 94FZ id,found on zakupki.gov.ru is $lastId", array('newest id' => $lastId));
                    return $lastId;
                } else if (!empty($ids_223[1][0])) {
                    $lastId = $ids_223[1][0];
                    echo "The newest 223FZ id,found on zakupki.gov.ru is " . $lastId . "\n";
                    $this->logger->log(50, 'IdSearcher', "The newest 223FZ id,found on zakupki.gov.ru is $lastId ", array('newest id' => $lastId));
                    return $lastId;
                } else {
                    $this->logger->log('75', 'IdSearcher', 'Can\'t search new id\'s on zakupki.gov.ru (can\'t parse id)', array('$answer' => $this->answer));
                    return false;
                }
            } else {
                $this->logger->log('75', 'IdSearcher', 'Can\'t search new id\'s on zakupki.gov.ru (can\'t pars nodevalue with id text )', array('$answer' => $this->answer));
                return false;
            }
        } else {
            $this->logger->log('75', 'IdSearcher', 'Can\'t search new id\'s on zakupki.gov.ru(can\'t find central block)', array('$answer' => $this->answer));
            return false;
        }
    }
}
