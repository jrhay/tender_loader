<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mario
 * Date: 10.07.13
 * Time: 19:33
 * To change this template use File | Settings | File Templates.
 */
namespace RAFFloader\ZakupkiGovRuBundle\Services\Searchers;

class DocSearcher94
{
    private $uid;
    private $content;
    private $baseDocsPublicationUrl = 'http://zakupki.gov.ru/pgz/public/action/orders/info/order_document_list_info/show?notificationId=';
    private $baseDocUrl = 'http://zakupki.gov.ru/pgz/documentdownload?documentId=';
    private $baseProtocolUrl = 'http://zakupki.gov.ru/pgz/printForm?type=PROTOCOL&id=';
    private $headers;
    private $service_container;
    private $logger;


    function __construct($serv_cont)
    {
        $this->service_container = $serv_cont;
        $this->logger = $this->service_container->get('logger');
    }

    public function searchDocs($content, $uid)
    {

        $this->content = $content;
        $this->uid = $uid;

        //TODO: рефакторить и оптимизировать механизм парсига :


        echo "\n\n  Publication $this->baseDocsPublicationUrl" . $this->uid . "\n";

        $idDocArray = array();
        $urlDocArray = array();
        $countAllDocs = 0;

        $resultUrlArray = array();
        $answer = $this->getContent();

        libxml_use_internal_errors(true);
        $domDoc = new \DOMDocument('1.0', 'UTF-8');
        $domDoc->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . $answer);
        libxml_use_internal_errors(false);

        $xpath = new \DOMXPath($domDoc);

        // получаем блок с документацией к лоту
        $pattern = "//*[@id='white']/table/tbody/tr/td[3]/table/tbody/tr/td/div/form/table/tbody/tr/td/table/tbody/tr[2]/td/div/table[3]/tbody/tr[2]/td/div/div[1]/table/tbody/tr/td[1]/div";
        $lotDocBlock = $xpath->query($pattern);
        $pattern = 'table';
        $lotDocRecords = $lotDocBlock = $xpath->query($pattern, $lotDocBlock->Item(0));
        // для отмененных заказов
        //Получаем блок с документами об отказе ( для отмененных тендеров)
        $pattern = "//*[@id='white']/table/tbody/tr/td[3]/table/tbody/tr/td/div/form/table/tbody/tr/td/table/tbody/tr[2]/td/div/table[3]/tbody/tr[2]/td/div/div[1]/table/tbody/tr/td[1]/div";
        $lotRejectionDocBlock = $xpath->query($pattern);
        $pattern = 'div/table/tbody/tr/td';

        $lotRejectionDocRecords = $lotDocBlock = $xpath->query($pattern, $lotRejectionDocBlock->Item(0));

        //Получаем блок с конкурсной документацией
        $pattern = "//*[@id='white']/table/tbody/tr/td[3]/table/tbody/tr/td/div/form/table/tbody/tr/td/table/tbody/tr[2]/td/div/table[4]/tbody/tr[2]/td/div";
        $RejectionContDocBlock = $xpath->query($pattern);
        $pattern = 'div/table/tbody/tr/td[1]/div/table/tbody/tr/td';
        $RejectionContDocBlockRecords = $xpath->query($pattern, $RejectionContDocBlock->Item(0));
        //Получаем блок с разъяснениями для отмененных заказов

        $pattern = "//*[@id='white']/table/tbody/tr/td[3]/table/tbody/tr/td/div/form/table/tbody/tr/td/table/tbody/tr[2]/td/div/table[5]/tbody/tr[2]/td/div"; ///div/table/tbody/tr/td[1]";
        $RejectedLotExplanationBlock = $xpath->query($pattern);

        $pattern = 'div/table/tbody/tr/td[1]/div/div/table/tbody/tr/td';
        $RejectedLotExplanationRecords = $lotProtocolBlock = $xpath->query($pattern, $RejectedLotExplanationBlock->Item(0));


        //для НЕ отмененных заказов
        // получаем блок с Дополнительной инфой
        $pattern = "//*[@id='white']/table/tbody/tr/td[3]/table/tbody/tr/td/div/form/table/tbody/tr/td/table/tbody/tr[2]/td/div/table[4]/tbody/tr[2]/td/div/div[1]/table/tbody/tr/td[1]/div/div";
        $lotDopInfoBlock = $xpath->query($pattern);
        $pattern = 'table';
        $lotDopInfoRecords = $xpath->query($pattern, $lotDopInfoBlock->Item(0));

        // получаем блок с доками для Протоколов
        $pattern = "//*[@id='white']/table/tbody/tr/td[3]/table/tbody/tr/td/div/form/table/tbody/tr/td/table/tbody/tr[2]/td/div/table[5]/tbody/tr[2]/td/div"; ///div/table/tbody/tr/td[1]";
        $lotProtocolBlock = $xpath->query($pattern);
        $pattern = 'div/table/tbody/tr/td[1]/table';
        $lotProtocolRecords = $lotProtocolBlock = $xpath->query($pattern, $lotProtocolBlock->Item(0));

        //Вытягиваем id документов к лоту
        if ($lotDocRecords->length) {
            for ($i = 0; $i < $lotDocRecords->length; $i++) {
                $docRecord = $lotDocRecords->item($i);
                $pattern = 'tbody/tr/td[3]/a/@onclick';
                $attrebute = $xpath->query($pattern, $docRecord);
                $pattern = '/downloadDocument((.*));return false;/';
                $countDocs = preg_match_all($pattern, $attrebute->item(0)->nodeValue, $idDoc);
                if ($countDocs) {
                    $countAllDocs += $countDocs;
                    $name = 'lotDocs/lotDoc-' . (count($idDocArray));
                    $idDocArray[$name] = $idDoc[1][0];
                }
                //echo "\n Docs for lot   " . $idDoc[1][0];
            }
        }

        //для отмененных лотов
        //Вытягиваем ID документов об отказе (для отмененных лотов)
        if ($lotRejectionDocRecords->length) {
            for ($i = 0; $i < $lotRejectionDocRecords->length; $i++) {
                $docRecord = $lotRejectionDocRecords->item($i);
                $pattern = 'a/@onclick';
                $attrebute = $xpath->query($pattern, $docRecord);

                if ($attrebute->length) {
                    $pattern = '/showNotificationPrintForm((.*));return false;/';
                    $countDocs = preg_match_all($pattern, $attrebute->item(0)->nodeValue, $urlDoc);
                    if ($countDocs) {
                        $countAllDocs += $countDocs;
                        $name = 'RejectionDocs/Redirected/lotDoc-' . (count($urlDocArray));
                        $urlDocArray[$name] = 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=' . preg_replace('([^0-9])', '', $urlDoc[1][0]);
                    }

                    $pattern = '/downloadDocument((.*));return false;/';
                    $countDocs = preg_match_all($pattern, $attrebute->item(0)->nodeValue, $idDoc);
                    if ($countDocs) {
                        $countAllDocs += $countDocs;
                        $name = 'RejectionDocs/Native/lotDoc-' . (count($idDocArray));
                        $idDocArray[$name] = $idDoc[1][0];
                    }
                }
            }
        }

        //ВЫтягиваем ID конкурсной документации для документов об отказе:


        if ($RejectionContDocBlockRecords->length) {
            for ($i = 0; $i < $RejectionContDocBlockRecords->length; $i++) {

                $docRecord = $RejectionContDocBlockRecords->item($i);
                $pattern = 'a/@onclick';

                $attrebute = $xpath->query($pattern, $docRecord);
                if ($attrebute->length) {
                    $pattern = '/downloadDocument((.*));return false;/';
                    $countDocs = preg_match_all($pattern, $attrebute->item(0)->nodeValue, $idDoc);
                    if ($countDocs) {
                        $countAllDocs += $countDocs;
                        $name = 'RejectedContestDocs/InfoDoc-' . (count($idDocArray));
                        $idDocArray[$name] = $idDoc[1][0];
                    }
                    // echo "\n Content documentation for canceled lot   " . $idDoc[1][0];
                }
            }
        }

        //Вытягиваем id документов с доп. инфой /разъяснениями для отмененных заказов

        if ($RejectedLotExplanationRecords->length) {
            for ($i = 0; $i < $RejectedLotExplanationRecords->length; $i++) {
                $docRecord = $RejectedLotExplanationRecords->item($i);
                $pattern = 'a/@onclick';
                $attrebute = $xpath->query($pattern, $docRecord);

                if ($attrebute->length) {
                    $pattern = '/showNotificationPrintForm((.*));return false;/';
                    $countDocs = preg_match_all($pattern, $attrebute->item(0)->nodeValue, $urlDoc);
                    if ($countDocs) {
                        $countAllDocs += $countDocs;
                        $name = 'RejectionExplanationDocs/Redirected/lotDoc-' . (count($urlDocArray));
                        $urlDocArray[$name] = 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=' . preg_replace('([^0-9])', '', $urlDoc[1][0]);
                    }

                    $pattern = '/downloadDocument((.*));return false;/';
                    $countDocs = preg_match_all($pattern, $attrebute->item(0)->nodeValue, $idDoc);
                    if ($countDocs) {
                        $countAllDocs += $countDocs;
                        $name = 'RejectionExplanationDocs/Native/lotDoc-' . (count($idDocArray));
                        $idDocArray[$name] = $idDoc[1][0];
                    }
                }
            }
        }


        //TODO: для неотмененных заказов:     ( переделать структуру кода), поместить все для отмененных заказов отдельно.
        //Вытягиваем id документов с доп. инфой /разъяснениями
        if ($lotDopInfoRecords->length) {
            for ($i = 0; $i < $lotDopInfoRecords->length; $i++) {

                $docRecord = $lotDopInfoRecords->item($i);
                $pattern = 'tbody/tr/td[3]/a/@onclick';
                $attrebute = $xpath->query($pattern, $docRecord);
                $pattern = '/downloadDocument((.*));return false;/';
                $countDocs = preg_match_all($pattern, $attrebute->item(0)->nodeValue, $idDoc);
                if ($countDocs) {
                    $countAllDocs += $countDocs;
                    $name = 'Info/InfoDoc-' . (count($idDocArray));
                    $idDocArray[$name] = $idDoc[1][0];
                }
            }
        }

        //Вытягиваем id документов с протоколами
        if ($lotProtocolRecords->length) {
            for ($i = 0; $i < $lotProtocolRecords->length; $i++) {

                $docRecord = $lotProtocolRecords->item($i);
                $pattern = 'tbody/tr/td[2]/a/@onclick';
                $attrebute = $xpath->query($pattern, $docRecord);

                // Вытягиваем ID шники из функций showProtocolPrintForm()
                $pattern = '/showProtocolPrintForm((.*));/';
                $countDocsPrintForm = preg_match_all($pattern, $attrebute->item(0)->nodeValue, $idDoc);
                if ($countDocsPrintForm) {
                    $countAllDocs += $countDocsPrintForm;
                    $namePrintForm = 'protocols/nativeServer/protocolDoc-' . (count($idDocArray));
                    $idDocArray['protocols'][$namePrintForm] = $idDoc[1][0];
                    // echo "\n Protocol  printForm " . $idDoc[1][0];
                }

                // Вытягиваем URLки из функций redirectToAE
                $pattern2 = '/redirectToAE(.*);.return.false;/';
                $countDocsRedirected = preg_match_all($pattern2, $attrebute->item(0)->nodeValue, $urlDoc);
                if ($countDocsRedirected) {
                    $countAllDocs += $countDocsRedirected;
                    $nameRedirected = 'protocols/Redirected/protocolDoc-' . (count($urlDocArray) + 1);
                    $urlDoc = str_replace(array("('", "')"), '', $urlDoc[1][0]);
                    $urlDocArray[$nameRedirected] = $urlDoc;
                    //echo "\n Protocol  redirectToAE " . $urlDoc;
                }
            }
        }

        if (is_null($countAllDocs)) {
            return false;
        }
        if (!empty($idDocArray)) {
            foreach ($idDocArray as $name => $id) {
                if ($name !== 'protocols') {
                    $resultUrlArray[$name] = $this->baseDocUrl . preg_replace('([^0-9])', '', $id);
                } else {
                    foreach ($id as $IDname => $protId) {
                        $resultUrlArray[$IDname] = $this->baseProtocolUrl . preg_replace('([^0-9])', '', $protId);
                    }
                }
            }
        }

        if (!empty($urlDocArray)) {
            foreach ($urlDocArray as $name => $url) {
                $resultUrlArray[$name] = $url;
            }
        }
        if (empty($resultUrlArray)) {
            $this->logger->log(70, 'DocSearcher94', 'result Url Array is empty, it can be a bug. Check this publication', array('uid' => $this->uid, 'url' => $this->baseDocsPublicationUrl . $this->uid));
        } else {
            $this->logger->log(50, 'DocSearcher94', 'The page with list of documents was parsed ', array('result array with refs on all docs' => $resultUrlArray, 'uid' => $this->uid, 'url' => $this->baseDocsPublicationUrl . $this->uid));
        }
        echo" \n Result url array (DocSearcher94): \n";
        var_dump($resultUrlArray);
        return $resultUrlArray;
    }


    public function getContent()
    {
        if (empty($this->content)) {
            $this->logger->log(75, 'Docsearcher94', 'DocSearcher94 was given an empty $content', array('content' => $this->content, 'uid' => $this->uid, 'url' => $this->baseDocsPublicationUrl . $this->uid));
            return false;
        }
        return $this->content;
    }

    public function saveHeader($curl, $headers)
    {
        $this->headers[] = $headers;
        return strlen($headers);
    }

}
