<?php
namespace RAFFloader\ZakupkiGovRuBundle\Services;
use RAFFloader\ZakupkiGovRuBundle\Services\Searchers\DocSearcher94;
use RAFFloader\ZakupkiGovRuBundle\Services\Searchers\DocSearcher223;

class Downloader
{
    private $baseDocsPublicationUrl_94 = 'http://zakupki.gov.ru/pgz/public/action/orders/info/order_document_list_info/show?notificationId=';
    private $baseDocsPublicationUrl_223 = 'http://zakupki.gov.ru/223/purchase/public/purchase/info/documents.html?noticeId=';
    private $baseDocsPublicationUrl = '';
    private $fzFlag = ''; // $fzFlag  принимает значения либо '94' либо '223' (строки) .
    private $resource_info = array(
        'platform' => 'zakupki_gov_ru',
        'uid' => '',
        'url' => '',
        'name' => '',
        'resource_type' => '',
    );
    /** @var \RAFFloader\ResourceManagerBundle\Services\ResourceManager */
    private $ResourceManager;
    /** @var \Symfony\Component\DependencyInjection\Container */
    private $service_container;
    /** @var \RAFFloader\LogBundle\Services\specLogger */
    private $logger;
    //TODO: для отладки:
    private $ProblemPublications;
    private $record = array(
        'uid' => '',
        'url',
        'platform',
        'finddate',
    );

    function __construct($RManager, $service_container)
    {
        $this->ResourceManager = $RManager;
        $this->service_container = $service_container;
        $this->logger = $this->service_container->get('logger');
    }

    public function load($record)
    {
        $memory = memory_get_usage(true);
        print "\n mem usage in load begin : $memory";
        if (strpos($record['url'], '223') !== false) {
            $this->baseDocsPublicationUrl = $this->baseDocsPublicationUrl_223;
            $this->fzFlag = '223';
        } else if (strpos($record['url'], 'pgz') !== false) {
            $this->baseDocsPublicationUrl = $this->baseDocsPublicationUrl_94;
            $this->fzFlag = '94';
        } else {
            echo "\n Error ! unknown url , can't detect FZ (Downloader ZakupkiGovRu)\n";
        }
        $this->record = $record;
        foreach ($record as $key => $value) {
            if (isset($this->resource_info[$key])) {
                $this->resource_info[$key] = $value;
            }
        }

        $mainResourceId = $this->loadMain();
        if ($mainResourceId === false) {
            print "\nERROR :Downloader (ZakupkiGovRu) can't load main resource";
            $this->logger->log(75,"Downloader(ZakupkiGovRu","ERROR :Downloader (ZakupkiGovRu) can't load main resource",array('record'=>$record));
            return false;
        }
        //TODO: переделать механизм обновления ресурсов
        $docListResourceId = $this->loadDocList(); //TODO : исправить на передачу ресурса
        if ($docListResourceId === false) {
            print "\n ERROR: DocListResource is not created";
            $this->logger->log(75, 'Downloader Zakupki Gov Ru', 'DocListResource is not created', array('record' => $record));
            return false;
        }
        return true;
    }

    /** Load Main page ( Notification print form), creates resource,using this Main page and reterns $id of created resource
     * @return $id (integer)
     */
    private function loadMain()
    {
        $this->resource_info['resource_type'] = 'http';
        $this->resource_info['name'] = 'Main';
        $this->logger->log(50, 'Downloader (ZakupkiGovRu)', 'downloading Main resource...', array('resource info' => $this->resource_info));
        $id = $this->ResourceManager->markForLoad($this->resource_info);
        if ($id) {
            return $id;
        } else {
            return false;
        }

    }

    /** Load html with the list of docs for current tender< creates resource< returns id of created resource
     * @return $id (integer)
     */
    private function loadDocList()
    {
        $this->resource_info['resource_type'] = 'http';
        $this->resource_info['name'] = 'docListPage/docListPage';
        $this->resource_info['url'] = $this->baseDocsPublicationUrl . $this->resource_info['uid'];

        $this->logger->log(50, 'Downloader (ZakupkiGovRu)', 'started downloading docList resource', array('resource info' => $this->resource_info));

        $resourceId = $this->ResourceManager->markForLoad($this->resource_info);
        if ($resourceId === false) {
            return false;
        }
        return $resourceId;
    }

    private function loadDocs($docListPage)
    {
        $logger = $this->service_container->get('logger');
        $uid = $this->resource_info['uid'];
        $searcher = null;
        $memory = memory_get_usage(true);
        print "\n mem usage before getting searcher : $memory";
        if ($this->fzFlag == '223') {
            /** @var $searcher DocSearcher223 */
            $searcher = $this->service_container->get('doc.searcher223');
        } else if ($this->fzFlag == '94') {
            /** @var $searcher DocSearcher94 */
            $searcher = $this->service_container->get('doc.searcher94');
        } else {
            echo "\nError: bad fzFlag (Downloader ZakupkiGovRu)";
            $this->logger(75, 'Downloader', 'Error: bad fzFlag (Downloader ZakupkiGovRu)', array('info' => $this->record, 'record' => $this->resource_info));
        }
        $memory = memory_get_usage(true);
        print "\n mem usage after getting searcher service : $memory";
        $docUrlArray = $searcher->searchDocs($docListPage, $uid);
        $memory = memory_get_usage(true);
        print "\n mem usage after parsing : $memory";
        if (!empty($docUrlArray)) {
            $resourceArray = array();
            $logger->log(50, 'Downloader (ZakupkiGovRu)', 'downloading docs processing...', array('resource info' => $this->resource_info));
            foreach ($docUrlArray as $name => $docUrl) {
                $this->resource_info['url'] = $docUrl;
                $this->resource_info['resource_type'] = 'http';
                $this->resource_info['name'] = $name;
                $resourceArray[] = $this->ResourceManager->create($this->resource_info);
            }
            print "\n Downloading docs complete\n";
            $logger->log(50, 'Downloader (ZakupkiGovRu)', 'downloading docs complete', array('resource info' => $this->resource_info));
            return $resourceArray;
        } else {
            $this->ProblemPublications[] = $this->baseDocsPublicationUrl . $this->resource_info['uid'];
            $logger->log(75, 'Downloader (ZakupkiGovRu)', 'Problem Publication detected: can\'t parse page with a list of documents', array('Bad Page adress' => end($this->ProblemPublications), 'resource info' => $this->resource_info));
            return false;
        }
    }

}