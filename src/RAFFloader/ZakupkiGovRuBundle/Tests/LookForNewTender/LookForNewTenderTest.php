<?php


use RAFFloader\ZakupkiGovRuBundle\services\LookForNewTender;
use RAFFloader\PublicationManagerBundle\services\PManager;
use RAFFloader\ZakupkiGovRuBundle\services\Downloader;

include_once('/home/mario/projects/RAFFloader/src/RAFFloader/ForTests/autoload.php');

class LookForNewTenderTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider providerGetCont
     */
    function testcheck_new()
    {
        $p_loader = new Downloader;
        $p_manager = new PManager($p_loader);
        $LFNT = new LookForNewTender($p_manager);
        $last_checked_id = $LFNT->check_new();
        echo "\ncheck_new()->getlast_id()=$last_checked_id \n\n";

    }


    public static function providerGetCont()
    {
        return array(
            array('http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6259773'),
            array('google.com'),
            array('http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=1'),
            array('монитор'),
        );
    }
}