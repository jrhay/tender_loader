<?php

namespace RAFFloader\ZakupkiGovRuBundle\Tests\Downloader;

use RAFFloader\ZakupkiGovRuBundle\services\Downloader;
use RAFFloader\ResourceManagerBundle\services\ResourceManager;

class DownloaderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider provideLoad
     */
    function testload($record)
    {
        $RManager = new ResourceManager;
        $Downloader = new Downloader($RManager);
        $Downloader->load($record);

    }

    function provideLoad()
    {

        return array(
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6306886', 'id' => '6306886', 'platform' => 'ZakupkiGovRu', 'finddate' => '2013-06-06 14:08:18')),
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6254030', 'id' => '6254030', 'platform' => 'ZakupkiGovRu', 'finddate' => '2013-06-06 14:08:18')),
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6254031', 'id' => '6254031', 'platform' => 'ZakupkiGovRu', 'finddate' => '2013-06-06 15:08:18')),
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6254032', 'id' => '6254032', 'platform' => 'ZakupkiGovRu', 'finddate' => '2013-06-06 16:08:18')),
            array(array('url' => 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=6254034', 'id' => '6254034', 'platform' => 'ZakupkiGovRu', 'finddate' => '2013-06-06 17:08:18')),
        );
    }

}